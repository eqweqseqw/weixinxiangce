<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ZeptoAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web'; 
    public $js = [
//         'js/zepto.min.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
