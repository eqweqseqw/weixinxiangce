<?php
namespace common\wechat;

use Yii;
use yii\log\Logger;
use common\wechat\interfaces\Ireply; 
use common\tools\WeChatTool;
use common\tools\YWeChatPrint;
use common\models\User;

class ReplySubscribeEvent implements Ireply
{
    protected $_postObject;
    protected $_fromUserName;
    protected $_toUserName;
    protected $_event;
    protected $_time;
    protected $_content;
    
    public function __construct($postObj) {
        // 获取参数   
        $this->_postObject = $postObj;  
        $this->_time = time();      
        $this->_fromUserName = $this->_postObject->FromUserName;      
        $this->_toUserName =  $this->_postObject->ToUserName;   
        $this->_event = $this->_postObject->Event;  
        
        $this->eventHandle();
    }
    private function eventHandle(){
        $user = User::findByOpenid($this->_fromUserName);
        if($user){
            $user->subscribe=1;
            $user->save();
            return;
        }
        User::addUserByOpenid($this->_fromUserName);           
    }

    public function createReMsg(){
    	switch ($this->_event)
    	{
            case "subscribe":   //关注事件
                $this->_content = \Yii::$app->params["weixinConf"]["guanzhuyu"];
                $result = YWeChatPrint::print_text($this->_fromUserName,$this->_toUserName,$this->_content);                           
                break;                                					
            default:
                $content = "你好！";
                //需要回显
                $result = YWeChatPrint::print_text($this->_fromUserName,$this->_toUserName,$content);
                break;
        }
    	return $result;
    }                                                                      
}

