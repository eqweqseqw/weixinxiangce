<?php

namespace app\modules\album\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\helpers\ArrayHelper;

use common\models\AlbumAppModel;
use common\models\AlbumTemplateModel;

/**
 * Default controller for the `album` module
 */
   
class TemplateController extends Controller
{
    public $title;
    public $keywords;
    public $description;
    public $layout = "show";//布局
    public $data;//数据
    public $items;//图片包
    public $editFlag;//编辑
    public $previewFlag;//预览
    public $template;//模板dir
    public $templateid;//模板id
    public $aid;//相册id、
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function actionIndex($aid)
    {
        if(empty($aid)){
            throw new NotFoundHttpException("相册不存在");
        }
        $model = new AlbumAppModel();
        $albumApp = $model->findOneById($aid); 
        if(empty($albumApp)){
            throw new NotFoundHttpException("相册不存在");
        } 
//        if($albumApp->status==0&&$albumApp->uid != yii::$app->user->identity->id){
//            throw new ForbiddenHttpException("你没有权限查看该相册！");
//        }
            
        $items = ArrayHelper::toArray($albumApp->items, [
                'app\modules\album\models\AlbumAppItem' => [
                    'id',
                    'pic',
                    'show'=>function($item){
                        return $item->show==1?TRUE:FALSE;
                    },
                    'des',
                    'appId' =>'app_id',
                    'createTime' => 'created_at',
                    'updateTime' => 'updated_at',     
                ],
            ]);         
        if(isset(\yii::$app->user->identity)&&$albumApp->uid === \yii::$app->user->identity->id){
            $this->editFlag = true;
        }else{
            $this->editFlag = false;
        }
        $this->title = $albumApp->title;
        $this->keywords = $albumApp->title;
        $this->description = $albumApp->des;
        
        $albumApp->incrementViews();
        $this->data = $albumApp;
        $this->items = array_map(function($item){
            return $item['pic'];
        }, $items);
        return $this->render($albumApp->template.'/index');
    }  
     /**
     * 编辑相册
     * @return type
     */
    public function actionEdit($aid){
        
        if(empty($aid)){
            throw new NotFoundHttpException("相册不存在");
        }    
        $model = new AlbumAppModel();
        $albumApp = $model->findOneById($aid); 
         if(empty($albumApp)){
            throw new NotFoundHttpException("相册不存在");
        }
        if($albumApp->uid != \yii::$app->user->identity->id){
            throw new ForbiddenHttpException("你没有权限编辑该相册！");
        }
        $items = ArrayHelper::toArray($albumApp->items, [
                'app\modules\album\models\AlbumAppItem' => [
                    'id',
                    'pic',
                    'show'=>function($item){
                        return $item->show==1?TRUE:FALSE;
                    },
                    'des',
                    'appId' =>'app_id',
                    'createTime' => 'created_at',
                    'updateTime' => 'updated_at',     
                ],
            ]);
        $this->data = $albumApp;
        $this->items = array_map(function($item){
            return $item['pic'];
        }, $items);
        $this->editFlag = true;
        return $this->render($albumApp->template.'/index');
        
    }   
     /**
     * 编辑预览
     * @return type
     */
    public function actionPreview($aid,$template){  
        if(empty($aid)||empty($template)){
            throw new NotFoundHttpException("参数异常！");
        }    
        
        $albumApp = AlbumAppModel::findOneById($aid); 
         if(empty($albumApp)){
            throw new NotFoundHttpException("相册不存在");
        }
        $templateObj = AlbumTemplateModel::findOneById($template); 
         if(empty($templateObj)){
            throw new NotFoundHttpException("模板不存在");
        }
        $albumApp->music = $templateObj->music;
        if($albumApp->uid != \yii::$app->user->identity->id){
            throw new ForbiddenHttpException("你没有权限编辑该相册！");
        }
        $items = ArrayHelper::toArray($albumApp->items, [
                'app\modules\album\models\AlbumAppItem' => [
                    'id',
                    'pic',
                    'show'=>function($item){
                        return $item->show==1?TRUE:FALSE;
                    },
                    'des',
                    'appId' =>'app_id',
                    'createTime' => 'created_at',
                    'updateTime' => 'updated_at',     
                ],
            ]);
        $this->data        = $albumApp;
        $this->title       = "预览".$albumApp->title;
        $this->keywords    = "预览".$albumApp->title;
        $this->description = $albumApp->des;
        
        $this->items = array_map(function($item){
            return $item['pic'];
        }, $items);
        $this->previewFlag = true;
        $this->aid         = $aid;
        $this->template    = $templateObj->dir;
        $this->templateid    = $templateObj->id;
        return $this->render($this->template.'/index');       
    }   
}
