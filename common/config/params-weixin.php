<?php

/*
 * 微信相关配置
 * qqmusic:
 * https://auth-external.music.qq.com/open/fcgi-bin/fcg_weixin_music_search.fcg?remoteplace=txt.weixin.officialaccount&w=关键字&platform=weixin&jsonCallback=MusicJsonCallback&perpage=分页大小&curpage=当前页
 */
$weixinConf = [
    "siteName"        =>'yyxch',
    "weixinPath"      =>'yyxch',
    "domainName"      =>''
    "webTitle"        =>'音乐相册制作号',
    'appid'           => '',
    'appsecret'       => '',
    'token'           => '',
    'template'        => 'dielianhua',
    'music'           => 'http://ws.stream.qqmusic.qq.com/C1000009dYfw42vUzC.m4a?fromtag=46',//默认音乐（终于等到你）
    'guanzhuUrl'      => 'https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzIwOTU4NjQ5OA==&scene=116#wechat_redirect', //关注链接
    'qqMusicUrl'      => 'https://auth-external.music.qq.com/open/fcgi-bin/fcg_weixin_music_search.fcg?remoteplace=txt.weixin.officialaccount&platform=weixin&jsonCallback=MusicJsonCallback&curpage=1&perpage=', //音乐接口
    'qqMusicPageSize' => '100', //音乐接口
    'defaultPassword' => '相册号', //微信新用户默认密码
    'guanzhuyu' => '谢谢你关注我们！', //微信关注时回复
    'kefuQQ' => '', //微信关注时回复
];

$menu = '{
        "button": [
            {
                "type": "click",
                "name": "登录",
                "key": "login"
            },
            {       
                "name":"相册",
                "sub_button":[
                    {
                        "type":"pic_weixin",
                        "name":"创建相册",
                        "key": "CREATE"
                    },
                    {
                        "type":"view",
                        "name":"我的相册",
                        "url": "'.$weixinConf["domainName"].'/album"
                    },
                    {
                        "type":"view",
                        "name":"相册广场",
                        "url":"'.$weixinConf["domainName"].'/album/app/gallery" 
                    }
                ]
            },         
            {
                "name":"关于我们",
                "sub_button":[
                    {
                        "type":"view",
                        "name":"关于我们",
                        "url":"'.$weixinConf["domainName"].'/site/about"
                    },
                    {
                        "type":"click",
                        "name":"在线客服",
                        "key":"customerService"
                    }
                ]
            }
        ]
    }';

return [
    "menu"=>$menu,
    "weixinConf"=>$weixinConf, 
];