<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AlbumPlugAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web'; 
    public $js = [
         'js/common.min.js',
         'js/eva_album.min.js',
    ];
    public $css = [
        'css/eva_album.css'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
