<?php
use yii\helpers\Url;
use app\modules\album\assets\AlbumUpdateAsset;

AlbumUpdateAsset::register($this);
?>
<div class="row">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">增加图片</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">编辑图片</a></li>
        <li><button type="button" class="btn btn-success"><a class="white" href="<?= Url::to(['/album/'.$aid])?>">回到相册</a></button></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">         
            <div id="uploader">
              <div class="queueList">
                  <div id="dndArea" class="placeholder">
                      <div id="filePicker"></div>
                      <p>或将照片拖到这里，单次最多可选300张</p>
                  </div>
              </div>
              <div class="statusBar" style="display:none;">
                  <div class="progress">
                      <span class="text">0%</span>
                      <span class="percentage"></span>
                  </div><div class="info"></div>
                  <div class="btns">
                      <div id="filePicker2"></div>
                      <div class="uploadBtn">上传</div>
                  </div>
              </div>
            </div>      
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($pics as $key=>$pic):?>
                    <div class="swiper-slide pid<?=$pic->id?>">  
                        <label class="pic-index"><?=$key?></label>
                        <img src="<?=$pic->pic?>" class="img-thumbnail img-responsive">  
                        <div class="tools" data-id="<?=$pic->id?>" data-aid="<?=$aid?>">
                            <span onclick="del(this)"><i class="glyphicon glyphicon-trash"></i><br>删除</span>
                            <?php if($album->pic!==$pic->pic):?>
                            <span onclick="setTpic(this)"><i class="glyphicon glyphicon-flag"></i><br><span class="text">设封面</span></span>
                            <?php else:?>
                            <span onclick="setTpic(this)"><i class="glyphicon glyphicon-flag text-red"></i><br><span class="text">封面</span></span>
                            <?php endif;?>
                        </div>
                    </div>                   
                    <?php endforeach;?>
                </div>               
            </div>             
        </div>         
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
</div>
<script>

function uploadFinishCallBack(){
     $.ajax({
        async: true,
        type : "post",  //提交方式  
        url : "doupdate",//路径  
        data : {  
            pics : uploader.options.pics,
            aid:<?=$aid?>
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.code!=1) {
               alert( result.message );
            } else {  
                window.location.href="/album/"+result.aid; 
            }  
        },
        error:function(){
            alert( '哎呀，系统开小差了！' );
        }
    });  
}
function del(obj){
        var _this = $(obj);
        var id    = _this.parent(".tools").data("id");
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "delpic",//路径  
            data : {  
                id:id
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理               
                if(result.code!=1) {
                   alert( result.message );                
                } else {  
                    $(".pid"+id).remove();
                    mySwiper.init();
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
    });  
    }
function setTpic(obj){
        var _this = $(obj);
        var id    = _this.parent(".tools").data("id");
        var aid    = _this.parent(".tools").data("aid");
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "settpic",//路径  
            data : {  
                aid:aid,
                id:id,
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理               
                if(result.code!=1) {
                   alert( result.message );
                } else {                   
                    var $active = _this.parents(".swiper-wrapper").find(".text-red");
                    $active.removeClass("text-red").find(".text").text("设封面");
                    _this.addClass("text-red").find(".text").text("封面");
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
    });  
    }
</script>