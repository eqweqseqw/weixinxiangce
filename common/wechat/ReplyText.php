<?php
namespace common\wechat;

use common\wechat\interfaces\Ireply;
use common\tools\YWeChatPrint;

class ReplyText implements Ireply
{
    protected $_postObject;
    protected $_fromUserName;
    protected $_toUserName;
    protected $_content;
    protected $_time;
    protected $_data;
    
    public function __construct($postObj) {
        // 获取参数   
        $this->_postObject = $postObj;
        $this->_time = time();      
        $this->_fromUserName = $this->_postObject->FromUserName;      
        $this->_toUserName =  $this->_postObject->ToUserName;      
        $this->_content = $this->_postObject->Content;            
    }
    
    public function createReMsg(){	             
        $content = "敬请期待，谢谢...";     
        return YWeChatPrint::print_text($this->_fromUserName,$this->_toUserName,$content);       
    }
}
