<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppCreateAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web'; 
    public $js = [
         'js/appcreate.js',
    ];
    public $depends = [
        'common\assets\WebUploadAsset',
    ];  
}
