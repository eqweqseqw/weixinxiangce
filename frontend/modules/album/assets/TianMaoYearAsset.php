<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class TianMaoYearAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web';
    public $css = [
        'css/tianmaoyearmain.css'
    ];
    public $js = [
        'js/three.min.js',
        'js/tianmaoyearmain.js',
    ];
    public $depends = [
    ];
}
