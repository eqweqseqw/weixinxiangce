<?php

/* 
 * 自定义输出工具类(针对框架)
 */
namespace common\tools;


class WeiChatPrint{
    //回复文本消息
    static public function print_text($fromusername,$tousername,$content,$time=null){
        if($time==null){
           $time = time(); 
        }
	$textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[text]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    </xml>";
        $result = sprintf($textTpl,$fromusername,$tousername,$time,$content);
        return $result;
    }
    //回复图片消息
    static public function print_image($fromusername,$tousername,$imageArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        $itemTpl = "<Image><MediaId><![CDATA[%s]]></MediaId></Image>";
        $item_str = sprintf($itemTpl, $imageArray['MediaId']);
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[image]]></MsgType>
                    $item_str
                    </xml>";
        $result = sprintf($textTpl,$fromusername,$tousername, $time);
        return $result;
    }
    //回复声音消息
    static public function print_voice($fromusername,$tousername,$voiceArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        $itemTpl = "<Voice><MediaId><![CDATA[%s]]></MediaId></Voice>";
        $item_str = sprintf($itemTpl, $voiceArray['MediaId']);
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[voice]]></MsgType>
                    $item_str
                    </xml>";
        $result = sprintf($textTpl,$fromusername,$tousername,$time);
        return $result;
    }
    //回复视频消息
    static public function print_video($fromusername,$tousername,$videoArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        $itemTpl = "<Video>
                    <MediaId><![CDATA[%s]]></MediaId>
                    <ThumbMediaId><![CDATA[%s]]></ThumbMediaId>
                    <Title><![CDATA[%s]]></Title>
                    <Description><![CDATA[%s]]></Description>
                    </Video>";
        $item_str = sprintf($itemTpl, $videoArray['MediaId'], $videoArray['ThumbMediaId'], $videoArray['Title'], $videoArray['Description']);
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[video]]></MsgType>
                    $item_str
                    </xml>";
        $result = sprintf($textTpl,$fromusername,$tousername,$time);
        return $result;
    }
    //回复图文消息
    static public function print_news($fromusername,$tousername,$arr_item,$time=null){
        if(!is_array($arr_item))
            return;
        if($time==null){
           $time = time(); 
        }
        $itemTpl = "<item>
               <Title><![CDATA[%s]]></Title>
               <Description><![CDATA[%s]]></Description>
               <PicUrl><![CDATA[%s]]></PicUrl>
               <Url><![CDATA[%s]]></Url>  
               </item>";        
        $item_str = "";
        $sum = count($arr_item);
        foreach ($arr_item as $item)
            $item_str .= sprintf($itemTpl, $item['Title'], $item['Description'], $item['PicUrl'], $item['Url']);
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[news]]></MsgType>
                    <Content><![CDATA[]]></Content>
                    <ArticleCount>$sum</ArticleCount>
                    <Articles>$item_str</Articles>
                    </xml>";
        $result = sprintf($textTpl,$fromusername,$tousername,$time);
        return $result;
    }
    //回复音乐消息
    static public function print_music($fromusername,$tousername,$musicArray,$time=null){
        if($time==null){
           $time = time(); 
        }
       $itemTpl = "<Music>
		    <Title><![CDATA[%s]]></Title>
		    <Description><![CDATA[%s]]></Description>
		    <MusicUrl><![CDATA[%s]]></MusicUrl>
		    <HQMusicUrl><![CDATA[%s]]></HQMusicUrl></Music>";

        $item_str = sprintf($itemTpl, $musicArray['Title'], $musicArray['Description'], $musicArray['MusicUrl'], $musicArray['HQMusicUrl']);

        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[music]]></MsgType>
                    $item_str
                    </xml>";
        $result = sprintf($textTpl,$fromusername,$tousername,$time);
        return $result;
    }   
}
