<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\tools\CommonTools;
use common\assets\WebUploadAsset;
use common\assets\WebUploadCssAsset;
?>
<?php if(CommonTools::is_wechat()&&CommonTools::is_android()):?>
<?php  WebUploadCssAsset::register($this);?>
<div class="album-upload">
     <div id="uploader">
        <div class="queueList">
            <div id="dndArea" class="placeholder">
                <div id="filePicker" class="webuploader-container">
                    <div class="webuploader-pick" >点击选择图片</div>
                    <div style="position: absolute; top: 20px; left: 38px; width: 168px; height: 44px; overflow: hidden; bottom: auto; right: auto;">
                        <label class="addImage" style="opacity: 0; width: 100%; height: 100%; display: block; cursor: pointer; background: rgb(255, 255, 255);"></label>
                    </div>
                </div>             
                <p>或将照片拖到这里，单次最多可选9张</p>
            </div>
        <ul class="filelist"></ul></div>
        <div class="statusBar" style="display:none;">        
            <div class="btns">
                <div id="filePicker2" class="webuploader-container"><div class="webuploader-pick">继续添加</div>
                    <div id="rt_rt_1b5pnnctnvjtj6g1n70ocmh5a6" style="position: absolute; top: 0px; left: 10px; width: 94px; height: 42px; overflow: hidden; bottom: auto; right: auto;">                  
                        <label class="addImage" style="opacity: 0; width: 100%; height: 100%; display: block; cursor: pointer; background: rgb(255, 255, 255);"></label>
                    </div>
                </div>
                <div class="uploadBtn state-ready">开始制作</div>
            </div>
        </div>     
    </div>
</div>
<script>
    $(function(){
        var pics = [];
        function delPic(){
            var $li = $(this).parents("li");
            var index =  $li.data("index");
            $(this).unbind("click");
            $li.remove();
            pics.splice(index,1);
        }
        function addFile(file,index){
             var $li = $( '<li data-index="'+index+'">' +
                    '<p class="imgWrap"><img src="' + file + '"></p></li>' ),

                $btns = $('<div class="file-panel" style="height:30px">' +
                    '<span class="cancel">删除</span></div>').appendTo( $li );
            $li.appendTo(".filelist");
            $(".filelist").find(".cancel:last").bind("click",delPic);
        }
        $(".addImage").bind("click",function(){       
            wx.chooseImage({
               count: 9, // 默认9
               sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
               sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
               success: function (res) {
                    $("#dndArea").addClass("element-invisible");
                    $(".statusBar").removeAttr("style");
                    var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                    for(var i in localIds){
                        var pic = localIds[i];
                        if($.inArray(pic,pics)==-1){
                            pics.push(pic);
                            addFile(pic,pics.length-1);
                        }                    
                    }                                
               }
           });
        })
        $(".uploadBtn").bind("click",function(){
            var serverIds = [];
            for(var i in pics){
                wx.uploadImage({
                    localId: pics[i], // 需要上传的图片的本地ID，由chooseImage接口获得
                    isShowProgressTips: 1, // 默认为1，显示进度提示
                    success: function (res) {
                        var serverId = res.serverId; // 返回图片的服务器端ID
                        serverIds.push(serverId);
                        if(pics.length==serverIds.length){
                            upImage();
                        }
                    }
                });
            }
            //上传服务器
            var upImage = function(){
                $.ajax({
                async: false,
                type : "post",  //提交方式  
                url : "dowechatcreate",//路径  
                data : {  
                    pics : serverIds  
                },//数据，这里使用的是Json格式进行传输  
                success : function(result) {//返回数据根据结果进行相应的处理  
                    if(result.code!=1) {
                       alert( result.message );
                    } else {  
                        window.location.href="/album/edit/"+result.aid; 
                    }  
                },
                error:function(){
                    alert( '哎呀，系统开小差了11s！' );
                }
            });  
            }           
        })
    })   
</script>
<?php else:?>
<?php  WebUploadAsset::register($this);?>
<div class="album-upload">
     <div id="uploader">
        <div class="queueList">
            <div id="dndArea" class="placeholder">
                <div id="filePicker"></div>             
                <p>或将照片拖到这里，单次最多可选300张</p>
            </div>
        </div>
        <div class="statusBar" style="display:none;">
            <div class="progress">
                <span class="text">0%</span>
                <span class="percentage"></span>
            </div><div class="info"></div>
            <div class="btns">
                <div id="filePicker2"></div>
                <div class="uploadBtn">开始制作</div>
            </div>
        </div>
    </div>
</div>
<script>
function uploadFinishCallBack(){
     $.ajax({
        async: true,
        type : "post",  //提交方式  
        url : "docreate",//路径  
        data : {  
            pics : uploader.options.pics  
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.code!=1) {
               alert( result.message );
            } else {  
                window.location.href="/album/edit/"+result.aid; 
            }  
        },
        error:function(){
            alert( '哎呀，系统开小差了！' );
        }
    });  
}
</script>
<?php endif;?>
