<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class WebUploadCssAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/dist/webuploader';
    public $baseUrl = '@web';
    public $css = [
        'webuploader.css',
        'style.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
