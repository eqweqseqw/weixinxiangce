<?php
use yii\helpers\Html;
use yii\helpers\Url;

use app\modules\album\assets\AlbumAsset;
use common\tools\WeChatTool;

AlbumAsset::register($this);
$albumAssetsDir = Yii::$app->assetManager->getPublishedUrl('@app/modules/album/assets/dist');

$data       = $this->context->data;
$items      = $this->context->items;
$weixinConf = \Yii::$app->params["weixinConf"];
//$actionID   = Yii::$app->controller->action->id;

$this->beginPage();
?>
<!DOCTYPE html>
<html style="overflow: hidden">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="format-detection" content="telephone=no,email=no">
<meta name="ML-Config" content="fullscreen=yes,preventMove=no">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="keywords" content="<?= Html::encode($this->context->keywords) ?>">
<meta name="description" content="<?= Html::encode($this->context->description) ?>">
<title><?= Html::encode($this->context->title) ?></title>
<?= Html::csrfMetaTags() ?>
<?php $this->head() ?>
<script src='http://res.wx.qq.com/open/js/jweixin-1.0.0.js'></script>
</head>
<body>
<?php 
$this->beginBody();
$time = time();   
$nonceStr = WeChatTool::createNonceStr();
?>
<script>      
wx.config({
    debug: false,
    appId: '<?php echo $weixinConf["appid"];?>',
    timestamp: <?php echo $time;?>,
    nonceStr: '<?php echo $nonceStr;?>',
    signature: '<?php echo WeChatTool::getSignature($time,$nonceStr);?>',
    jsApiList: [
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'hideMenuItems',
        'showMenuItems',
        'hideAllNonBaseMenuItem'
    ]
});
var slider_images_url = <?= json_encode($items) ?>;
var aid = '<?=$data["id"]?>';
var desc = '<?= empty($data["des"])?$data["title"]:$data["des"]?>';
var title = '<?=$data["title"]?>'
var openid = "<?=$data["openid"]?>";
var scene = '<?=$data["template"]?>';
var music_url = '<?=$data["music"]?>';
var pic_url = "http://"+window.location.host + '<?=$data["pic"]?>';
var albumAssetsDir = '<?=$albumAssetsDir?>';
var module_inits = [];
var words = {};
var music_player = new Audio();
 //微信相关
    wx.ready(function () {
        wx.hideAllNonBaseMenuItem();			
        wx.showMenuItems({
            menuList: ["menuItem:share:appMessage","menuItem:share:timeline", "menuItem:favorite"]
        });
//        wx.hideMenuItems({
//            menuList: ["menuItem:copyUrl","menuItem:profile","menuItem:refresh","menuItem:share:facebook","menuItem:openWithQQBrowser","menuItem:openWithSafari","menuItem:addContact","menuItem:share:qq","menuItem:share:QZone"] 
//        });
        wx.onMenuShareTimeline({//分享到朋友圈
            title: title, // 分享标题
            link: window.location.href, // 分享链接
            imgUrl: pic_url, // 分享图标
            success: function () { 
                // 用户确认分享后执行的回调函数
            },
            cancel: function () { 
                // 用户取消分享后执行的回调函数
            }
        });
        wx.onMenuShareAppMessage({//分享给朋友
            title: title, // 分享标题
            desc: desc, // 分享描述
            link: window.location.href, // 分享链接
            imgUrl: pic_url, // 分享图标
            type: 'link', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            success: function () { 
                // 用户确认分享后执行的回调函数
            },
            cancel: function () { 
                // 用户取消分享后执行的回调函数
            }
        });       
//        wx.onMenuShareQQ({
//            title : title,
//            desc : desc,
//            link: window.location.href, // 分享链接
//            imgUrl: pic_url, // 分享图标
//            success: function () { 
//               // 用户确认分享后执行的回调函数
//            },
//            cancel: function () { 
//               // 用户取消分享后执行的回调函数
//            }
//        });
//        wx.onMenuShareWeibo({
//            title  : title,
//            desc   : desc,
//            link: window.location.href, // 分享链接
//            imgUrl: pic_url, // 分享图标
//            success: function () { 
//               // 用户确认分享后执行的回调函数
//            },
//            cancel: function () { 
//                // 用户取消分享后执行的回调函数
//            }
//        });
//        wx.onMenuShareQZone({
//            title   : title,
//            desc    : desc,
//            link    : window.location.href,
//            imgUrl  : pic_url,
//            success: function () { 
//               // 用户确认分享后执行的回调函数
//            },
//            cancel: function () { 
//                // 用户取消分享后执行的回调函数
//            }
//        }); 
    })
function call_me(fun){
    module_inits.push(fun);
}
function load_init_modules(){
    for(var i=0; i<module_inits.length; i++){
        module_inits[i]();
    }
}
var footerClose = true;
</script>

<div class="main-body">
    <?= $content ?>
</div>
<div class="footer-option">  
    <?php if(isset($this->context->editFlag)&&!$this->context->editFlag):?>               
    <div class="row" data-aid="<?=Yii::$app->request->get("aid")?>">
        <div class="col-xs-3"><a class="white" href="<?= $weixinConf['guanzhuUrl']?>"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon glyphicon-heart"></i><br>关注我们</button></a></div>             
        <div class="col-xs-3"><a class="white" href="<?=  Url::to(['app/create'])?>"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-th-large"></i><br>我要制作</button></a></div>   
        <?php if(isset(\yii::$app->user->identity)):?>
        <div class="col-xs-3" id="collect-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-star"></i><br>收藏</button></div>      
        <div class="col-xs-3" id="zan-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-thumbs-up"></i><br>点赞</button></div>      
        <?php endif;?>
    </div>   
    <?php elseif(isset($this->context->editFlag)&&$this->context->editFlag):?>
    <div class="row" data-aid="<?=Yii::$app->request->get("aid")?>">
        <div class="col-xs-3" id="template-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-th-large"></i><br>模板</button></div>
        <div class="col-xs-3" id="music-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-music"></i><br>音乐</button></div>
        <div class="col-xs-3" id="title-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-pencil"></i><br>标题</button></div>
        <div class="col-xs-3" id="pic-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-picture"></i><br>图片</button></div>
    </div>
    <?php elseif(isset($this->context->previewFlag)&&$this->context->previewFlag):?>
    <div class="row" data-aid="<?=$this->context->aid?>" data-template="<?=$this->context->template?>" data-templateid="<?=$this->context->templateid?>">
        <div class="col-xs-3" id="template-ok"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-ok"></i><br>保存</button></div>
        <div class="col-xs-3" id="template-ko"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-check"></i><br>只换模板</button></div>
        <div class="col-xs-3" id="cancel-b"><button type="button" class="btn btn-transparence"><i class="bottom glyphicon glyphicon-remove"></i><br>取消</button></div>
    </div>
    <script>
        footerClose = false;
    </script>
    <?php endif;?>   
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>