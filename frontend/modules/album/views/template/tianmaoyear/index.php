<?php
use app\modules\album\assets\TianMaoYearAsset;
TianMaoYearAsset::register($this);

$p = \Yii::$app->controller->module->templateAsset."/tianmaoyear/";
?>
<style>
    html,body,.main-body{
        height: 100%;
    }
    ul, li {
    list-style: none;
    line-height: 0px;
}

body, div, ul, li, img, p, a, h1, h2, h3 {
    margin: 0px;
    padding: 0px;
    border: 0px;
}

html, body {
    background: #f4f4cd;
    color: #49494d;
    font-family: "微软雅黑", "黑体", Helvetica, Verdana, sans-serif;
    font-size: 14px;
}

body {
    width: 100%;
    height: 100%;
    overflow: hidden;
}

div {
    position: absolute;
    background-size: contain;
    background-position: center center;
    background-repeat: no-repeat;
}

#three{
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    overflow: hidden;
}

#main {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    overflow: hidden;
    /*background-image: url('images/bg.jpg');*/
    /*background-size: 100% 100%;*/
}

#three, #nav, #menu, #share, #preload, #tip2 {
    position: absolute;
}

#three, #menu, #share, #preload, #tip2 {
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
}

#menu, #share {
    opacity: 0;
    display: none;
}

.btnShare {
    position: absolute;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    display: none;
}

#nav {
    top: 0px;
    right: 0px;
}

#nav div {
    position: absolute;
    width: 80px;
    height: 80px;
    top: 0px;
}

#preload .group {
    left: 50%;
    top: 50%;
    position: absolute;
    margin: -70px 0 0 0;
}

#preload .group div {
    position: absolute;
}

#preload .n1 {
    width: 20px;
    height: 44px;
    margin: 40px 0 0 -55px;
    line-height: 44px;
    font-size: 30px;
    text-align: center;
    color: #fff;
}

#preload .n2 {
    width: 20px;
    height: 44px;
    margin: 40px 0 0 -10px;
    line-height: 44px;
    font-size: 30px;
    text-align: center;
    color: #fff;
}

#preload .n3 {
    width: 20px;
    height: 44px;
    margin: 40px 0 0 35px;
    line-height: 44px;
    font-size: 30px;
    text-align: center;
    color: #fff;
}

#shareImg {
    width: 1px;
    height: 1px;
    opacity: 0;
}

#tip {
    position: absolute;
    width: 438px;
    height: 157px;
    background: url('<?=$p?>images/tip.png');
    left: 50%;
    bottom: 20px;
    margin: 0 0 0 -219px;
    opacity: 0;
}

#tip2 {
    background-image: url('<?=$p?>images/tip2.png');
    background-color: #e10000;
}


</style>
<script>
    function parseUA() {
        var u = navigator.userAgent;
        var u2 = navigator.userAgent.toLowerCase();
        return { //移动终端浏览器版本信息
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            iosv: u.substr(u.indexOf('iPhone OS') + 9, 3),
            weixin: u2.match(/MicroMessenger/i) == "micromessenger",
            ali: u.indexOf('AliApp') > -1,
        };
    }
    var exPage = [];
    var ua = parseUA();
    for(var i in slider_images_url){
        if(i==0){
            exPage.push.apply(exPage,[
                {
                    basic: "b"+(i%8+1)
                },
                {
                    basic: "b"+(i%8+1),
                    img: "../.."+slider_images_url[i]+"_700x1024"
                }
            ])
        }else{
            exPage.push.apply(exPage,[
                {
                    basic: "b"+(i%8+1)
                }, {
                    basic: "b"+(i%8+1)
                }, {
                    basic: "b"+(i%8+1)
                }, {
                    basic: "b"+(i%8+1)
                },
                {
                    basic: "b"+(i%8+1),
                    img: "../.."+slider_images_url[i]+"_700x1024"
                }
            ])
        }                  
    }
</script>
<div id="three">
</div>
<div id="main">  
    <div id="tip"></div>
    <div id="tip2"></div>
</div>
<!--<audio id="bgm" preload="auto" src="static/bgm.mp3" loop></audio>-->
<script>
    
    wx.ready(function () {
        document.getElementById('bgm').load();
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-79368027-9', 'auto');
    ga('send', 'pageview');

</script>