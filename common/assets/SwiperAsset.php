<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SwiperAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/dist/swiper';
    public $baseUrl = '@web';
    public $css = [
        'css/swiper.min.css',
    ];
    public $js = [
        'js/swiper.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
