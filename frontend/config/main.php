<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'album' => [
            'class' => 'app\modules\album\Album',
            'defaultRoute' => 'app/index',
            'layout'=>'main'
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],      
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false, 
            'showScriptName' => false,
            'suffix' => '', 
            'rules' => [
                "album/<aid:\d+>"=>"album/template/index",
                "album/edit/<aid:\d+>"=>"album/template/edit",
//                "<controller:\w+>/<id:\d+>"=>"<controller>/view", 
//                "<controller:\w+>/<action:\w+>"=>"<controller>/<action>" 
            ],
        ],  
        'assetManager' => [ 
            'linkAssets' => true, 
        ], 
        'i18n' => [
            'translations' => [
               'app*' => [
                  'class' => 'yii\i18n\PhpMessageSource',
                  'fileMap' => [
                     'app' => 'app.php'
                  ],
               ],
            ],
         ],
        'language' => 'zh-CN',
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                $code     = Yii::$app->request->get('suppress_response_code');
                if ($response->data !== null && !empty($code)) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                    $response->statusCode = 200;
                }
            },
        ],          
    ],
    'params' => $params,
];
