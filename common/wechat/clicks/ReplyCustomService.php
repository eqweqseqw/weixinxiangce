<?php
namespace common\wechat\clicks;
use Yii;
use common\wechat\interfaces\Ireply; 
use common\tools\WXmlResponseFormatter as WXRF;
use common\tools\WeChatTool;
use common\tools\HttpTool;

class ReplyCustomService implements Ireply
{
    protected $_postObject;
    protected $_fromUserName;
    protected $_toUserName;
    
    public function __construct($postObj) {
        // 获取参数   
        $this->_postObject = $postObj;     
        $this->_fromUserName = $this->_postObject->FromUserName;      
        $this->_toUserName =  $this->_postObject->ToUserName;    
        $this->SendCustomMessage(); 
    }
    public function createReMsg(){	                     
    	   
        return $this->transmitService();              
    }
    
    private function SendCustomMessage(){

    	$url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".WeChatTool::getToken();
 	
    	//发送文本消息
    	$postData = '{
                    "touser":"'.$this->_fromUserName.'",
                    "msgtype":"text",
                    "text":
                    {
                        "content":"请您耐心等待，正在拼命呼叫客服小二。。。\n或者QQ联系： \n'.Yii::$app->params["weixinConf"]["kefuQQ"].'"
                    }
                }';
                            
        HttpTool::http_post($url, $postData);       
    }    
    
    
        
    //回复多客服消息
    private function transmitService(){   	
        return [
                "ToUserName"=>[$this->_fromUserName,WXRF::CDATA=>true],
                "FromUserName"=>[$this->_toUserName,WXRF::CDATA=>true],
                "CreateTime"=>time(),
                "MsgType"=>['transfer_customer_service',WXRF::CDATA=>true],         
                ];
    }    
}
