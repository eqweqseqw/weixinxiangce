<?php
$p = \Yii::$app->controller->module->templateAsset."/duanwujie/";
?>

<style type="text/css">
	*
	{
        padding: 0px;
        margin: 0px;
        -webkit-box-sizing: border-box;
	}

   html,body,.main-body
    {
        height: 100%;
        background-color: black;
    }

    #biyeji_container
    {
            position: absolute;
            width:  500px;
            height: 100%;
            overflow:hidden;
    }

	.biyeji_pic
	{
		position: absolute;
		display: none;
		/*change*/
	}

	#biyeji_page0_0
	{
		top:  372px;
		left: 233px;
		background-image: url(<?=$p?>images/0fc163f942a1b857639c.png);
		width: 191px;
		height: 189px;
	}

	#biyeji_page0_0 > div
	{
		position: absolute;
		width: 145px;
		height: 135px;
		-webkit-transform: rotate(-13deg);
		top: 23px;
		left: 23px;
		overflow: hidden;

	}

	#biyeji_page0_1
	{
		top:  220px;
		left: 280px;
		background-image: url(<?=$p?>images/be1e361e63a0d2b946b2.png);
		width: 155px;
		height: 152px;
	}

	#biyeji_page0_1 > div
	{
		position: absolute;
		width: 112px;
		height: 110px;
		-webkit-transform: rotate(-15deg);
		top: 18px;
		left: 20px;
		overflow: hidden;
	}

	#biyeji_page0_2
	{
		top:  200px;
		left: 58px;
		background-image: url(<?=$p?>images/733934e538d65d4da272.png);
		width: 217px;
		height: 215px;

	}

	#biyeji_page0_2 > div
	{
		position: absolute;
		width: 165px;
		height: 160px;
		-webkit-transform: rotate(11deg);
		left: 23px;
		top: 19px;
		overflow: hidden;
	}

	#biyeji_page1_0
	{
		top:  180px;
		left: 29px;
		background-image: url(<?=$p?>images/adaeff640b214c0e4c6e.png);
		width: 305px;
		height: 239px;
	}

	#biyeji_page1_0 > div
	{
		position: absolute;
		width: 254px;
		height: 180px;
		-webkit-transform: rotate(-8deg);
		top: 28px;
		left: 18px;
		overflow: hidden;
	}

	#biyeji_page1_1
	{
		top:  400px;
		left: 252px;
		background-image: url(<?=$p?>images/835dc409595d4cbc1d2a.png);
		width: 150px;
		height: 148px;
	}

	#biyeji_page1_1 > div
	{
		position: absolute;
		width: 113px;
		height: 110px;
		-webkit-transform: rotate(13deg);
		top: 18px;
		left: 19px;
		overflow: hidden;
	}

	#biyeji_page1_2
	{
		top:  260px;
		left: 332px;
		background-image: url(<?=$p?>images/faf9ccc04fc421c2c52e.png);
		width: 130px;
		height: 125px;
	}

	#biyeji_page1_2 > div
	{
		position: absolute;
		width: 113px;
		height: 110px;
		-webkit-transform: rotate(-2deg);
		top: 5px;
		left: 9px;
		overflow: hidden;
	}

	#biyeji_page1_3
	{
		top:  430px;
		left: 88px;
		background-image: url(<?=$p?>images/c5b3e7d76b4423481540.png);
		width: 135px;
		height: 131px;
	}

	#biyeji_page1_3 > div
	{
		position: absolute;
		width: 106px;
		height: 102px;
		-webkit-transform: rotate(-9deg);
		top: 14px;
		left: 17px;
		overflow: hidden;
	}

	#biyeji_page2_0
	{
		top: 230px;
		left: 62px;
		background-image: url(<?=$p?>images/fc9664977cacc482460e.png);
		width: 377px;
		height: 286px;
	}

	#biyeji_page2_0 > div
	{
		width: 335px;
		height: 232px;
		position: absolute;
		-webkit-transform: rotate(1deg);
		top: 18px;
		left: 21px;
		overflow: hidden;
	}

	#biyeji_page3_0
	{
		top: 385px;
		left: 231px;
		background-image: url(<?=$p?>images/37119bff3679de2c4f46.png);
		width: 235px;
		height: 169px;
	}

	#biyeji_page3_0 > div
	{
		position: absolute;
		width: 188px;
		height: 120px;
		-webkit-transform: rotate(7.5deg);
		top: 20px;
		left: 22px;
		overflow: hidden;
	}

	#biyeji_page3_1
	{
		top: 170px;
		left: 45px;
		background-image: url(<?=$p?>images/85926ed917904caf16cc.png);
		width: 315px;
		height: 240px;
	}

	#biyeji_page3_1 > div
	{
		position: absolute;
		width: 259px;
		height: 168px;
		-webkit-transform: rotate(-10deg);
		top: 29px;
		left: 22px;
		overflow: hidden;
	}

	#biyeji_page4_0
	{
		top: 168px;
		left: 50px;
		background-image: url(<?=$p?>images/b9320dd10f227795c9e4.png);
		width: 262px;
		height: 260px;
	}

	#biyeji_page4_0 > div
	{
		position: absolute;
		width: 182px;
		height: 175px;
		-webkit-transform: rotate(25deg);
		left: 41px;
		top: 37px;
		overflow: hidden;
	}

	#biyeji_page4_1
	{
		top: 392px;
		left: 265px;
		background-image: url(<?=$p?>images/e608efb2aded972a16ce.png);
		width: 174px;
		height: 179px;
	}

	#biyeji_page4_1 > div
	{
		position: absolute;
		width: 150px;
		height: 148px;
		top: 9px;
		left: 11px;
		-webkit-transform: rotate(1deg);
		overflow: hidden;
	}

	#biyeji_page5_0
	{
		top: 398px;
		left: 193px;
		background-image: url(<?=$p?>images/030536b35ab952a653dc.png);
		width: 232px;
		height: 225px;
	}

	#biyeji_page5_0 > div
	{
		width: 158px;
		height: 154px;
		position: absolute;
		-webkit-transform: rotate(-20deg);
		left: 31px;
		top: 32px;
		overflow: hidden;
	}

	#biyeji_page5_1
	{
		top: 210px;
		left: 40px;
		background-image: url(<?=$p?>images/2d88cf8cf9ef371bc27e.png);
		width: 292px;
		height: 229px;
	}

	#biyeji_page5_1 > div
	{
		width: 241px;
		height: 168px;
		position: absolute;
		-webkit-transform: rotate(-12deg);
		left: 23px;
		top: 28px;
		overflow: hidden;
	}

	 @-webkit-keyframes aa_in_bounce_down
    {
        0%{
            opacity:0;
            -webkit-transform:translateY(-2000px)
        }
        60%{
            opacity:1;
            -webkit-transform:translateY(30px)
        }
        80%{
            -webkit-transform:translateY(-10px)
        }
        100%{
            -webkit-transform:translateY(0)
        }
    }

    @-webkit-keyframes aa_out_bounce_down
    {
        0%{
            -webkit-transform:translateY(0)
        }
        20%{
            opacity:1;
            -webkit-transform:translateY(-20px)
        }
        100%{
            opacity:0;
            -webkit-transform:translateY(2000px)
        }
    }
/*---������---*/
    @-webkit-keyframes aa_in_bounce_up
    {
        0%{
            opacity:0;
            -webkit-transform:translateY(2000px)
        }
        60%{
            opacity:1;
            -webkit-transform:translateY(-30px)
        }
        80%{
            -webkit-transform:translateY(10px)
        }
        100%{
            -webkit-transform:translateY(0)
        }
    }
    @-webkit-keyframes aa_out_bounce_up
    {
       0%{
        -webkit-transform:translateY(0)
        }
        20%{
            opacity:1;
            -webkit-transform:translateY(20px)
        }
        100%{
            opacity:0;
            -webkit-transform:translateY(-2000px)
        }
    }   
/*---������---*/
    @-webkit-keyframes aa_in_bounce_left
    {
        0%{
            opacity:0;
            -webkit-transform:translateX(-2000px)
        }
        60%{
            opacity:1;
            -webkit-transform:translateX(30px)
        }
        80%{
            -webkit-transform:translateX(-10px)
        }
        100%{
            -webkit-transform:translateX(0)
        }
    }
    @-webkit-keyframes aa_out_bounce_left0
    {
        0%{
            -webkit-transform:translateX(0)
        }
        20%{
            opacity:1;
            -webkit-transform:translateX(20px)
        }
        100%{
            opacity:0;
            -webkit-transform:translateX(-2000px)
        }
    }

/*---������---*/
    @-webkit-keyframes aa_in_bounce_right0
    {
        0%{
            opacity:0;
            -webkit-transform:translateX(2000px)
        }
        60%{
            opacity:1;
            -webkit-transform:translateX(-30px)
        }
        80%{
            -webkit-transform:translateX(10px)
        }
        100%{
            -webkit-transform:translateX(0)
        }
    }
    @-webkit-keyframes aa_out_bounce_right
    {
        0%{
            -webkit-transform:translateX(0)
        }
        20%{
            opacity:1;
            -webkit-transform:translateX(-20px)
        }
        100%{
            opacity:0;
        -webkit-transform:translateX(2000px)
        }
    }
    
    @-webkit-keyframes aa_in_bounce_center
    {
        0%{
            opacity:0;
            -webkit-transform:scale(.3)
        }
        50%{
            opacity:1;
            -webkit-transform:scale(1.05)
        }
        70%{
            -webkit-transform:scale(.9)
        }
        100%{
            -webkit-transform:scale(1)
        }
    }

    @-webkit-keyframes piaoluo
    {
    	from{-webkit-transform:translate(0px,0px) rotate(0deg);}
    	to{-webkit-transform:translate(-400px,1200px) rotate(360deg);}
    }
</style>

<div id='biyeji_container'>

	<img id='biyeji_background' src='<?=$p?>images/322ec3f610d9df345954.jpg'>

	<div id='biyeji_page0_0' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_0_0' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page0_1' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_0_1' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page0_2' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_0_2' style='position:absolute' src=''>
		</div>
	</div>

	<div id='biyeji_page1_0' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_1_0' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page1_1' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_1_1' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page1_2' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_1_2' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page1_3' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_1_3' style='position:absolute' src=''>
		</div>
	</div>

	<div id='biyeji_page2_0' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_2_0' style='position:absolute' src=''>
		</div>
	</div>

	<div id='biyeji_page3_0' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_3_0' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page3_1' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_3_1' style='position:absolute' src=''>
		</div>
	</div>

	<div id='biyeji_page4_0' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_4_0' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page4_1' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_4_1' style='position:absolute' src=''>
		</div>
	</div>

	<div id='biyeji_page5_0' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_5_0' style='position:absolute' src=''>
		</div>
	</div>
	<div id='biyeji_page5_1' class='biyeji_pic'>
		<div>
			<img id='biyeji_page_5_1' style='position:absolute' src=''>
		</div>
	</div>

	<img src='<?=$p?>images/aa6fbcc77793d4a9bbb4.png' style='position:absolute;top:-100px;left:200px;width:60px;-webkit-animation:piaoluo 10s linear infinite'>
	<img src='<?=$p?>images/edb7f969562538280aba.png' style='position:absolute;top:-100px;left:300px;width:40px;-webkit-animation:piaoluo 10s linear 2.5s infinite'>
	<img src='<?=$p?>images/b5a3ba9c72bb379b449e.png' style='position:absolute;top:-100px;left:400px;width:50px;-webkit-animation:piaoluo 10s linear 7.5s infinite'>
	<img src='<?=$p?>images/aa6fbcc77793d4a9bbb4.png' style='position:absolute;top:-100px;left:500px;width:60px;-webkit-animation:piaoluo 10s linear 5s infinite'>


</div>

<script>

	var biyeji_index=0; 
	var images_url_index=0;

	var biyeji_range=[[{w:145,h:135},{w:110,h:112},{w:165,h:160}],[{w:254,h:175},{w:110,h:108},{w:113,h:110},{w:106,h:102}],[{w:335,h:232}],[{w:188,h:120},{w:259,h:168}],[{w:182,h:175},{w:150,h:148}],[{w:158,h:154},{w:241,h:168}]]


	function biyeji_init()
	{
		biyeji_showpage_index();
		setInterval(biyeji_showpage_index,4700);
		// biyeji_cut_image(0,0,1)
		// biyeji_loadimage();
	}

	function biyeji_showpage_index()
	{
		switch(biyeji_index)
		{
			case 0:biyeji_showpage0();break;
			case 1:biyeji_showpage1();break;
			case 2:biyeji_showpage2();break;
			case 3:biyeji_showpage3();break;
			case 4:biyeji_showpage4();break;
			case 5:biyeji_showpage5();break;
		}
		biyeji_index++;
		if(biyeji_index==6)
			biyeji_index=0;
	}
	
	function id (name) 
	{
		return document.getElementById(name)
	}

	function biyeji_showpage0 () 
	{

		for(var i=0;i<3;i++)
		{
			if(images_url_index>=slider_images_url.length)
				images_url_index=0;
			var myimg=id('biyeji_page_0_'+i);
			biyeji_cut_image(biyeji_index,i,images_url_index);
			myimg.src=slider_images_url[images_url_index];
			images_url_index++;
		}
		id('biyeji_page0_1').style.display='block';
		id('biyeji_page0_1').style.webkitAnimation='aa_in_bounce_right0 2s ease';

		// alert(id('biyeji_page_0_0').src+id('biyeji_page_0_0').width+id('biyeji_page_0_0').style.left)

		setTimeout(function()
		{
			id('biyeji_page0_0').style.display='block';
			id('biyeji_page0_0').style.webkitAnimation='aa_in_bounce_up 2s ease';
		},100);

		setTimeout(function()
		{
			id('biyeji_page0_2').style.display='block';
			id('biyeji_page0_2').style.webkitAnimation='aa_in_bounce_left 2s ease';
		},200);

		setTimeout(function()
		{
			id('biyeji_page0_0').style.webkitAnimation='aa_out_bounce_down 2s ease forwards';
			id('biyeji_page0_1').style.webkitAnimation='aa_out_bounce_right 2s ease forwards';
			id('biyeji_page0_2').style.webkitAnimation='aa_out_bounce_left0 2s ease forwards';
		},4000);

		// alert(id('biyeji_page_0_0').width)
	}

	function biyeji_showpage1 ()
	{
		for(var i=0;i<4;i++)
		{
			if(images_url_index>=slider_images_url.length)
				images_url_index=0;
			var myimg=id('biyeji_page_1_'+i);
			biyeji_cut_image(biyeji_index,i,images_url_index);
			myimg.src=slider_images_url[images_url_index];
			images_url_index++;
		}

		id('biyeji_page1_0').style.display='block';
		id('biyeji_page1_0').style.webkitAnimation='aa_in_bounce_down 2s ease';


		id('biyeji_page1_1').style.display='block';
		id('biyeji_page1_1').style.webkitAnimation='aa_in_bounce_up 2s ease';

		setTimeout(function()
		{
			id('biyeji_page1_2').style.display='block';
			id('biyeji_page1_2').style.webkitAnimation='aa_in_bounce_right0 2s ease';

			id('biyeji_page1_3').style.display='block';
			id('biyeji_page1_3').style.webkitAnimation='aa_in_bounce_left 2s ease';
		},100);

		setTimeout(function()
		{
			id('biyeji_page1_0').style.webkitAnimation='aa_out_bounce_up 2s ease forwards';
			id('biyeji_page1_1').style.webkitAnimation='aa_out_bounce_down 2s ease forwards';
			id('biyeji_page1_2').style.webkitAnimation='aa_out_bounce_right 2s ease forwards';
			id('biyeji_page1_3').style.webkitAnimation='aa_out_bounce_left0 2s ease forwards';
		},4000)
	}

	function biyeji_showpage2 ()
	{
		for(var i=0;i<1;i++)
		{
			if(images_url_index>=slider_images_url.length)
				images_url_index=0;
			var myimg=id('biyeji_page_2_'+i);
			biyeji_cut_image(biyeji_index,i,images_url_index);
			myimg.src=slider_images_url[images_url_index];
			images_url_index++;
		}

		id('biyeji_page2_0').style.display='block';
		id('biyeji_page2_0').style.webkitAnimation='aa_in_bounce_center 2s ease';

		setTimeout(function()
		{
			id('biyeji_page2_0').style.webkitAnimation='aa_out_bounce_left0 2s ease forwards';
		},4000)
	}

	function biyeji_showpage3 ()
	{
		for(var i=0;i<2;i++)
		{
			if(images_url_index>=slider_images_url.length)
				images_url_index=0;
			var myimg=id('biyeji_page_3_'+i);
			biyeji_cut_image(biyeji_index,i,images_url_index);
			myimg.src=slider_images_url[images_url_index];
			images_url_index++;
		}

		id('biyeji_page3_0').style.display='block';
		id('biyeji_page3_0').style.webkitAnimation='aa_in_bounce_right0 2s ease';

		id('biyeji_page3_1').style.display='block';
		id('biyeji_page3_1').style.webkitAnimation='aa_in_bounce_left 2s ease';

		setTimeout(function()
		{
			id('biyeji_page3_0').style.webkitAnimation='aa_out_bounce_down 2s ease forwards';
			id('biyeji_page3_1').style.webkitAnimation='aa_out_bounce_up 2s ease forwards';
		},4000)
	}

	function biyeji_showpage4 ()
	{
		for(var i=0;i<2;i++)
		{
			if(images_url_index>=slider_images_url.length)
				images_url_index=0;
			var myimg=id('biyeji_page_4_'+i);
			biyeji_cut_image(biyeji_index,i,images_url_index);
			myimg.src=slider_images_url[images_url_index];
			images_url_index++;
		}

		id('biyeji_page4_0').style.display='block';
		id('biyeji_page4_0').style.webkitAnimation='aa_in_bounce_right0 2s ease';

		id('biyeji_page4_1').style.display='block';
		id('biyeji_page4_1').style.webkitAnimation='aa_in_bounce_left 2s ease';

		setTimeout(function()
		{
			id('biyeji_page4_0').style.webkitAnimation='aa_out_bounce_up 2s ease forwards';
			id('biyeji_page4_1').style.webkitAnimation='aa_out_bounce_down 2s ease forwards';
		},4000)
	}

	function biyeji_showpage5 ()
	{
		for(var i=0;i<2;i++)
		{
			if(images_url_index>=slider_images_url.length)
				images_url_index=0;
			var myimg=id('biyeji_page_5_'+i);
			biyeji_cut_image(biyeji_index,i,images_url_index);
			myimg.src=slider_images_url[images_url_index];
			images_url_index++;
		}

		id('biyeji_page5_0').style.display='block';
		id('biyeji_page5_0').style.webkitAnimation='aa_in_bounce_up 2s ease';

		id('biyeji_page5_1').style.display='block';
		id('biyeji_page5_1').style.webkitAnimation='aa_in_bounce_down 2s ease';

		setTimeout(function()
		{
			id('biyeji_page5_0').style.webkitAnimation='aa_out_bounce_right 2s ease forwards';
			id('biyeji_page5_1').style.webkitAnimation='aa_out_bounce_left0 2s ease forwards';
		},4000)
	}

	function biyeji_cut_image(pageid,pageid_index,image_id)
	{
		// var img=new Image();
		// img.src=slider_images_url[image_id];
		var imgheight=imagesize[image_id*2+1];
		var imgwidth=imagesize[image_id*2];
		// alert(imgheight+'-'+imgwidth);
		// var imgheight=id('biyeji_page_'+pageid+'_'+pageid_index).height;
		// var imgwidth=id('biyeji_page_'+pageid+'_'+pageid_index).width;
		var image_bili=imgwidth/imgheight;

		var my_img=id('biyeji_page_'+pageid+'_'+pageid_index);
		// id('biyeji_page_'+pageid+'_'+pageid_index).src=slider_images_url[image_id];

		if((biyeji_range[pageid][pageid_index].w/biyeji_range[pageid][pageid_index].h)>image_bili)
		{
			my_img.width=biyeji_range[pageid][pageid_index].w;
			my_img.height=biyeji_range[pageid][pageid_index].w/image_bili;
			my_img.style.left='0px';
			my_img.style.top='-'+(my_img.height-biyeji_range[pageid][pageid_index].h)/2+'px';
		}
		else
		{
			my_img.height=biyeji_range[pageid][pageid_index].h;
			my_img.width=biyeji_range[pageid][pageid_index].h*image_bili;
			my_img.style.top='0px';
			my_img.style.left='-'+(my_img.width-biyeji_range[pageid][pageid_index].w)/2+'px';
			// console.log(id('biyeji_page_'+pageid+'_'+pageid_index).height)
			// console.log(id('biyeji_page_'+pageid+'_'+pageid_index).width)
			// console.log(id('biyeji_page_'+pageid+'_'+pageid_index).style.top)
			// console.log(id('biyeji_page_'+pageid+'_'+pageid_index).style.left)
		}


	}
	var myimage=[];
	var ready=0;
	var readynum=0;
	var myimg_name=['biyeji_page_0_0','biyeji_page_0_1','biyeji_page_0_2','biyeji_page_1_0','biyeji_page_1_1','biyeji_page_1_2','biyeji_page_1_3','biyeji_page_2_0','biyeji_page_3_0','biyeji_page_3_1','biyeji_page_4_0','biyeji_page_4_1','biyeji_page_5_0','biyeji_page_5_1']

	var imagesize=[];
	function biyeji_loadimage()
	{
		var img;
		for(var i=0;i<slider_images_url.length;i++)
		{
			img=new Image();
			img.src=slider_images_url[i];
			img.index=i;
			// imagesize[i]=img.width;
			img.onload=myonload;
			// console.log(img.src)
			// imagesize[i].h=img.height;
			// console.log(i,img.width,img.src)
		}
		// img.onload=function()
		// 	{
		// 		// ready++;

		// 		// console.log(i,img.width,img.src)

		// 		// if(ready==slider_images_url.length)
		// 		// {
		// 			var j=0;
		// 			for(var i=0;i<myimg_name.length;i++)
		// 			{
		// 				if(j==slider_images_url.length)
		// 					j=0;
		// 				var name=myimg_name[i];
		// 				id(name).src=slider_images_url[j];
		// 				myimage[i]=id(name);
		// 				j++;

		// 			}
		// 			

		// 			mysetInterval=setInterval(function()
		// 			{
		// 				if(check_load()==true)
		// 				{
		// 					biyeji_init();
		// 					alert(imagesize[0])
		// 					clearInterval(mysetInterval);
		// 				}	
		// 			},50)
					
		// 		// }
		// 	}
	}

	function check_load()
	{
		for(var j=0;j<slider_images_url.length;j++)
		{
			if(myimage[j].width==0)
			{
				return false;
			}
		}
		// alert(myimage[0].width+'-'+myimage[1].width+'-'+myimage[2].width+'-'+myimage[3].width)
		return true;
	}

	var img_id=0;
	function myonload(event)
	{
		var img=event.target;
		// console.log(img.width);
		var this_index=img.index;
		imagesize[this_index*2]=img.width;
		imagesize[this_index*2+1]=img.height;
		img_id++;
		if(img_id==slider_images_url.length)
		{
			var j=0;
					for(var i=0;i<myimg_name.length;i++)
					{
						if(j==slider_images_url.length)
							j=0;
						var name=myimg_name[i];
						id(name).src=slider_images_url[j];
						myimage[i]=id(name);
						j++;

					}

					// mysetInterval=setInterval(function()
					// {
					// 	if(check_load()==true)
					// 	{
							biyeji_init();
							// alert(imagesize[0]+'-'+imagesize[1]+'-'+imagesize[2]+'-'+imagesize[3]+'-'+imagesize[4]+'-'+imagesize[5]+'-'+imagesize[6]+'-'+imagesize[7]+'-'+imagesize[8]+'-'+imagesize[9]+'-'+imagesize[10]+'-')
					// 		clearInterval(mysetInterval);
					// 	}	
					// },50)
		}
	}


	call_me(biyeji_loadimage);
</script> 