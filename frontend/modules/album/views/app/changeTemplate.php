<?php
use app\modules\album\assets\TemplateAsset;

TemplateAsset::register($this);
?>
<style>
    .img-thumbnail{
        max-height: 170px
    }
    .nav{
        z-index: 99;
        background: white;
        margin-left: -15px;
        margin-right: -15px;
        position: fixed;
        width: 100%;
        border-bottom: 1px solid rgb(191, 102, 9);
    }
    .swiper-container{
        height: 32px;
        line-height: 30px;
    }
    .swiper-slide{
        line-height: 32px;
        width: 50px !important;
        font-size: 20px;
        text-align: center;
        word-break: break-all;
        white-space: nowrap;
        overflow: hidden;
    }  
    .active{
        color: #c34113;
        border-bottom: 3px solid #c34113;
    }
    .template-name{
        color: #333;
        font-size: 14px;
        width: 100%;
        display: block;
        text-align: center;
    } 
    .column-album{
        padding-left: 5px;
        padding-right: 5px;
    }
    #template-data{
        padding-top: 36px;
        margin-bottom: 40px;
        display: table;
        width: 100%;
    }
</style>
<div class="nav">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide active" data-cid="0">推荐</div>
            <?php foreach ($class as $val):?>
            <div class="swiper-slide" data-cid="<?=$val->id?>"><?=$val->name?></div>
            <?php endforeach;?>        
        </div>
    </div>
</div>
<input id="aid" type="hidden" value="<?=$aid?>">
<div id="template-data">
<?php foreach ($templates as $template):?>
    <div class="col-md-3 col-xs-4 column-album">
        <a class="template-a" href="javascript:void(0)" data-id="<?=$template->id?>">
            <img alt="<?=$template->title?>" src="<?=$template->pic?>" class="img-thumbnail"/><br>
            <span class="template-name"><?=$template->title?></span>
        </a>
    </div>      
<?php endforeach;?>
</div>
<script id="temp-template" type="text/x-handlebars-template">
    {{#each templates}}
        <div class="col-md-3 col-xs-4 column-album">
        <a class="template-a" href="javascript:void(0)"  data-id="{{id}}">
            <img alt="{{title}}" src="{{pic}}" class="img-thumbnail"/><br>
            <span class="template-name">{{title}}</span>
        </a>
        </div>   
    {{/each}}
</script>