<?php

class replytext implements Ireply
{
    protected $_postObject;
    protected $_fromUserName;
    protected $_toUserName;
    protected $_content;
    protected $_flag;
    protected $_time;
    protected $_data;
    
    public function init($postObj) {
        // 获取参数   
        $this->_postObject = $postObj;
        $this->_flag = false;
        if ($this->_postObject == false) {          
            return false;
        }
        $this->_time = time();      
        $this->_fromUserName = $this->_postObject->FromUserName;      
        $this->_toUserName =  $this->_postObject->ToUserName;      
        $this->_content = $this->_postObject->Content;      
        if(!($this->_fromUserName && $this->_toUserName && $this->_content)) {
            return false;
        }
        return true;
    }
    
    public function createReMsg(){	             
        $content = "敬请期待，谢谢...";     
        $resultStr = myprint::print_text($this->_fromUserName,$this->_toUserName,$content);
        return $resultStr;
         
    }
}
