<?php
namespace app\modules\album\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\AlbumAppModel;
/**
 * AlbumApp model
 *
 * @property integer $id
 * @property string $uid 用户id
 * @property string $aid 相册id
 * @property integer $created_at 创建时间
 * @property integer $updated_at 更新时间
 */
class AlbumCollectModel extends ActiveRecord
{

    /**
     * @wangwei
     */
    public static function tableName()
    {
        return '{{%album_collect}}';
    }

    /**
     * @wangwei
     * 时间处理
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
    * @wangwei
    * 默认值
    */
    public function rules()
    {
        return [
            ['aid', 'required'],
            ['aid', 'integer'],
            
            ['uid', 'required'],
            ['uid', 'integer'],      
        ];
    }

     /**
     * @wangwei
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户',
            'aid' => "相册",         
            'created_at' => "创建时间",
            'updated_at' => "更新时间",
        ];
    }
    /**
     * Finds albumApp by uid
     * @wangwei
     * @param int $uid 用户id
     */
    public static function findOneById($id)
    {
        return static::findOne(['id' => $id]);
    }
    /**
     * Finds albumApp by uid
     * @wangwei
     * @param int $uid 用户id
     */
    public static function findByUid($uid)
    {
        return static::findByCondition(['uid' => $uid]);
    }
    /**
     * Finds albumApp by aid
     * @wangwei
     * @param int $aid 相册id
     */
    public static function findByAid($aid)
    {
        return static::findByCondition(['aid' => $aid]);
    }  
         
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }  
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     */
    public function getItems()
    {
        // 第一个参数为要关联的子表模型类名，
        // 第二个参数指定 通过子表的app_id，关联主表的id字段
        return $this->hasOne(AlbumAppModel::className(), ['id' => 'aid']);
    }
    
}
