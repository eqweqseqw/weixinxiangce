<?php
namespace common\models;
use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * AlbumApp model
 *
 * @property integer $id
 * @property string $uid 用户id
 * @property string $openid openid
 * @property string $title 标题
 * @property string $pic 封面
 * @property string $music 音乐
 * @property integer $template 模板
 * @property integer $views 浏览次数
 * @property integer $shares 分享次数
 * @property integer $zan 点赞次数
 * @property integer $des 简介
 * @property integer $is_delete 是否删除
 * @property integer $is_ok 是否完成创建
 * @property integer $created_at 创建时间
 * @property integer $updated_at 更新时间
 */
class AlbumAppModel extends ActiveRecord
{
    const STATUS_ME = 0;//私有
    const STATUS_PU = 2;//公开
    const STATUS_FR = 1;//好友可见
    
    const DELETE_OFF = 0;//不删除
    const DELETE_ON = 1;//删除
    
    const IS_OK = 1;//完成创建
    const IS_KO = 0;//未完成创建


    /**
     * @wangwei
     */
    public static function tableName()
    {
        return '{{%album_app}}';
    }

    /**
     * @wangwei
     * 时间处理
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @wangwei
     * 默认值
     */
    public function rules()
    {
        return [
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'required'],
            ['title', 'string', 'min' => 1, 'max' => 100],
            
            ['des', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'max' => 500],
            
            ['status', 'default', 'value' => self::STATUS_ME],
            ['status', 'in', 'range' => [self::STATUS_ME, self::STATUS_PU, self::STATUS_FR]],
            ['is_delete', 'default', 'value' => self::DELETE_OFF],
            ['is_delete', 'in', 'range' => [self::DELETE_OFF, self::DELETE_ON]],
            ['is_ok', 'default', 'value' => self::IS_OK],
            ['is_ok', 'in', 'range' => [self::IS_OK, self::IS_KO]],
        ];
    }
     /**
     * @wangwei
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户',
            'openid' => "openid",
            'title' => "标题",
            'pic' => "封面",
            'music' => "音乐",
            'template' => "模板",
            'views' => "浏览次数",
            'shares' => "分享次数",
            'zan' => "点赞次数",
            'des' => "简介",
            'is_delete' => "是否删除",
            'is_ok' => "是否完成",
            'created_at' => "创建时间",
            'updated_at' => "更新时间",  
        ];
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     */
    public static function findOneById($id,$status=0)
    {
        return static::findOne(['id' => $id, ['>=','status',$status]]);
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     */
    public function getItems()
    {
        // 第一个参数为要关联的子表模型类名，
        // 第二个参数指定 通过子表的app_id，关联主表的id字段
        return $this->hasMany(AlbumAppItemModel::className(), ['app_id' => 'id']);
    }
    
    /**
     * Finds albumApp by userid
     *
     * @param int $userid 用户id
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     * @return static|null
     */
    public static function findByUserid($userid,$status=0)
    {
        return static::findAll(['uid' => $username, ['>=','status',$status]]);
    }
    /**
     * Finds albumApp by openid
     *
     * @param int $userid 用户openid
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     * @return static|null
     */
    public static function findByOpenid($openid,$status=0)
    {
        return static::findAll(['openid' => $openid, ['>=','status',$status]]);
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }  
    /**
     * 浏览次数自增    
     */
    public function incrementViews()
    {
        $this->views++;
        $this->save();
    }  
    /**
     * zan
     */
    public function incrementZan()
    {
        $this->zan++;
        return $this->save();
    }
    public static function getNewAlbumApp(){
        $albumApp = static::findOne(['uid' => yii::$app->user->identity->id, ['=','is_ok',0]]);
        if(!empty($albumApp))
            return $albumApp;
        $albumAppModel             = new AlbumAppModel();
        $albumAppModel->uid       = yii::$app->user->identity->id;
        $albumAppModel->openid     = yii::$app->user->identity->openid;
        $albumAppModel->status     = 0;//默认私有
        $albumAppModel->title      = yii::$app->user->identity->wx_username."的相册";
        $albumAppModel->is_delete  = 0;
         $albumAppModel->is_ok     = 0;
        $albumAppModel->template   = \Yii::$app->params["weixinConf"]["template"];
        $albumAppModel->music   = \Yii::$app->params["weixinConf"]["music"];
        $albumAppModel->save();
        return $albumAppModel;
    }   
}
