<?php 
session_start();
header("Content-type: text/html; charset=utf-8");
?>
<style type="text/css">
	*
	{
        padding: 0px;
        margin: 0px;
        -webkit-box-sizing: border-box;
	}

    body
    {
        background-color: white;
    }
    #container
    {
    	width: 500px;	
    }
    #top
    {
    	width: 500px;
    	background-color: #44B549;
    	height: 60px;
    	line-height: 60px;
    	font-size: 30px;
    	color: #fff;
    	text-align: center;
    	font-weight: normal;
    }
    .right
    {
    	float: left;
	    width: 20px;
	    height: 20px;
	    border-top: 10px solid transparent;
	    border-left: 20px solid #3E3E3E;
	    border-bottom: 10px solid transparent;
	    margin: 10px;

    }
    .down
    {
    	float: left;
	    width: 20px;
	    height: 20px;
	    border-left: 10px solid transparent;
	    border-right: 10px solid transparent;
	    border-top: 20px solid #3E3E3E;
	    margin: 10px;
    }
    .question
    {
    	width: 500px;
    	/*padding-left: 10px;*/

    }
    .q_text
    {
    	width: 500px;
    	position: relative;
    	left: 0px;
    	top: 0px;
    	font-size: 23px;
    	line-height: 50px;
    	color: #006666;
    	background-color: #F4F5F9;
    	padding-left: 10px;
    }
    .q_text1
    {
    	width: 500px;
    	position: relative;
    	left: 0px;
    	top: 0px;
    	font-size: 23px;
    	line-height: 50px;
    	background-color:#E0EAF6;
    	color: #459AE9;
    	padding-left: 10px;
    }
    .answer
    {
    	width: 500px;
    	position: relative;
    	padding: 10px 10px 0px 10px;
    	line-height: 35px;
    	font-size: 22px;
    }
</style>

<div id='container'>
	<div id='top' style="text-align:center">
		相册常见问题  
	</div>
	<div id='q1' class='question' style='margin-top: 3px;' onclick='show(1)'>
		<div id='qu1' class='q_text'>1. 如何制作相册？</div>
		<div id='a1' class='answer' style='display:none'>点击下方 “音乐相册(或开始制作)” -> "新建相册"，选择图片上传后，点击“已收到X张图片，点这里开始制作”即自动生成。</div>
	</div>
	<div id='q2' class='question' style='margin-top: 3px;' onclick='show(2)'>
		<div id='qu2' class='q_text'>2. 如何查看或修改以前制作的相册？</div>
		<div id='a2' class='answer' style='display:none'>点击下方 “音乐相册(或开始制作)” => "我的相册"，打开即可看到所有相册，点击 “编辑” 即可对相册进行修改。</div>
	</div>
	<div id='q3' class='question' style='margin-top: 3px;' onclick='show(3)'>
		<div id='qu3' class='q_text'>3. 相册的安全性和保密性</div>
		<div id='a3' class='answer' style='display:none'>相册都经过安全和保密处理，除非您将相册主动分享出去，否则别人是看不到的。</div>
	</div>
	<div id='q4' class='question' style='margin-top: 3px;' onclick='show(4)'>
		<div id='qu4' class='q_text'>4. 相册是否收费?</div>
		<div id='a4' class='answer' style='display:none'>相册是完全免费的，请放心使用。</div>
	</div>
	<div id='q5' class='question' style='margin-top: 3px;' onclick='show(5)'>
		<div id='qu5' class='q_text'>5. 是否只能上传9张图片？</div>
		<div id='a5' class='answer' style='display:none'>由于微信一次性只能发9张图片，如需继续上传，点击“音乐相册(或开始制作)”->“增加图片”，即可继续添加。建议上传图片数目不要过多，否则加载会打开会比较慢，可分成多个相册制作。</div>
	</div>
	<div id='q6' class='question' style='margin-top: 3px;' onclick='show(6)'>
		<div id='qu6' class='q_text'>6. 如何修改音乐？</div>
		<div id='a6' class='answer' style='display:none'>打开相册，点击下方的“选音乐”，找到喜欢的音乐，并点击右方的“确定”,如：也可以点击“搜索”,快速找到歌曲，由于音乐库在不断更新，有的歌曲暂时搜索不到，我们会及时更新。</div>
	</div>
	<div id='q7' class='question' style='margin-top: 3px;' onclick='show(7)'>
		<div id='qu7' class='q_text'>7. 如何修改图片？</div>
		<div id='a7' class='answer' style='display:none'>打开相册，点击下方的“增删图”，即可删除图片。</div>
	</div>
	<div id='q8' class='question' style='margin-top: 3px;' onclick='show(8)'>
		<div id='qu8' class='q_text'>8. 如何添加文字？</div>
		<div id='a8' class='answer' style='display:none'>目前只支持修改相册的标题，方法如下：<br>打开相册，点击下方的“改标题”，即可输入文字。</div>
	</div>
	<div id='q9' class='question' style='margin-top: 3px;' onclick='show(9)'>
		<div id='qu9' class='q_text'>9. 相册能制作多少个？保存多久？</div>
		<div id='a9' class='answer' style='display:none'>相册的数目不限数量，如要制作新的相册，点击“新建相册”。<br>对于已经制作的相册，正常情况下，如果您不删除的话，相册将会永久保存。</div>
	</div>
	<div id='q10' class='question' style='margin-top: 3px;' onclick='show(10)'>
		<div id='qu10' class='q_text'>10. 相册过多是否会占用内存？</div>
		<div id='a10' class='answer' style='display:none'>所有相册都保存在我们的服务器中，不会占用您任何的存储空间的，如果您删除了已经分享的相册，那些已经分享出去的相册也会被删除掉。</div>
	</div>
	<div id='q11' class='question' style='margin-top: 3px;' onclick='show(11)'>
		<div id='qu11' class='q_text'>11. 相册打不开，没有音乐或不出照片？</div>
		<div id='a11' class='answer' style='display:none'>浏览相册时，请确保在网络通畅的情况下。如仍有问题，添加微信号xxxx,并将有问题的相册转发给我们，我们会为您查看问题并及时解决。</div>
	</div>
	<div id='q12' class='question' style='margin-top: 3px;' onclick='show(12)'>
		<div id='qu12' class='q_text'>12. 相册的播放次序问题</div>
		<div id='a12' class='answer' style='display:none'>除了3d类型的模板，绝大部分的模板都是按照顺序播放的。</div>
	</div>
</div>
<script>
function init_viewport()
{
    if(/Android (\d+\.\d+)/.test(navigator.userAgent))
    {
        var version = parseFloat(RegExp.$1);

        if(version>2.3)
        {
            var phoneScale = parseInt(window.screen.width)/500;
            document.write('<meta name="viewport" content="width=500, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', target-densitydpi=device-dpi">');
        }
        else
        {
            document.write('<meta name="viewport" content="width=500, target-densitydpi=device-dpi">');    
        }
    }
    else if(navigator.userAgent.indexOf('iPhone') != -1)
    {
        var phoneScale = parseInt(window.screen.width)/500;
        document.write('<meta name="viewport" content="width=500, height=750,initial-scale=' + phoneScale +', user-scalable=no" /> ');         //0.75   0.82
    }
    else 
    {
        document.write('<meta name="viewport" content="width=500, height=750,initial-scale=0.64" /> ');         //0.75   0.82

    }
}

init_viewport();


function show (index) 
{

	var obj = document.getElementById('a'+index);
	if(obj.style.display == 'block')
	{
		obj.style.display = 'none';
		var qu = document.getElementById('qu'+index);
		qu.className = 'q_text';
		return;
	}

	var as = document.getElementsByClassName('answer');
	var q = document.getElementsByClassName('q_text1');
	for(var i = 0; i<q.length; i++)
	{
		q[i].className = 'q_text';
	}
	for(var i = 0; i<as.length;i++)
	{
		as[i].style.display = 'none';
	}
	if(obj.style.display == 'none')
	{
		var qu = document.getElementById('qu'+index);
		qu.className = 'q_text1';
		obj.style.display = 'block';
	}
	else
	{
		var qu = document.getElementById('qu'+index);
		qu.className = 'q_text';
		obj.style.display = 'none';
	}
	
}

</script>

