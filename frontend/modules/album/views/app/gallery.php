<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\assets\ImagefitAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

ImagefitAsset::register($this);
?>
<?php
    NavBar::begin([
        'brandLabel' => \Yii::t('app', 'My Company'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => \Yii::t('app', 'Home'), 'url' => ['/site/index']],
        ['label' => \Yii::t('app', 'About'), 'url' => ['/site/about']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => \Yii::t('app', 'Signup'), 'url' => ['/site/signup']];
        $menuItems[] = ['label' => \Yii::t('app', 'Login'), 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                \Yii::t('app', 'Logout').' (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
<style>
.album-option{
    position: absolute;
    bottom: 0;
    width: 100%;
    color: #f9f9f9;
}
.album-label{
    position: absolute;
    width: 100%;
    color: #f9f9f9;
    background: rgba(12, 12, 12, 0.29);
}
.album-option .hand{
    float: right;
    margin-right: 10px;
    background: rgba(8, 8, 8, 0.14);
}
.option-icon{
    margin-right: 3px;
}
.pager .active a{
    background: rgba(233, 84, 32, 0.37);
}   
.album-option .option{
    float: right;
    margin-right: 10px;
    background: rgba(51, 51, 51, 0.68);;
    font-size: 12px;
    height: 30px;
    line-height: 30px;
    padding: 0 15px;
    border: 1px solid #333;
    border-radius: 2px;
    display: none;
}

</style>
<div class="row" style="margin-top: 55px;margin-bottom: 44px;">
    <?php if(count($albums)>0):?>
        <?php foreach ($albums as $album):?>
    <a href="<?=Url::to(['/album/'.$album->id]);?>" class="album-a">
            <div class="column-album img-thumbnail1" style=";height: 180px;padding:0;margin: 15px 0">   
                <img alt="<?=$album->title?>" src="<?=$album->pic?>" class="img"/>   
                <label class="album-label"><?=$album->title?></label>
                <label class="album-option">
                    <span class="glyphicon glyphicon-hand-left hand"></span>
                    <div class="option">                                 
                        <span class="album-zan" data-aid="<?=$album->id?>"><i class="glyphicon glyphicon-thumbs-up"></i>点赞</span>  
                        <span class="album-collect" data-aid="<?=$album->id?>"><i class="glyphicon glyphicon-star"></i>收藏</span>  
                    </div>

                </label>
            </div> 
        </a>    
        <?php endforeach;?>
        <?php else:?>              
       <h3> 空空如也。。。</h3>
    <?php endif;?>   
</div>

<?php if(isset($pages)):?>
<div><?= LinkPager::widget(['pagination' => $pages,'options' => ['class' => 'pager'],'maxButtonCount' => 5]); ?></div>
<?php endif;?> 