<?php
return [
    'Profile' => '资料',    
    'Sign Out' => '退出',
    'Users' => '用户列表',
    'User' => '用户',
    'Create User' => '增加用户',
    'Email' => '邮箱',
    'Status' => '状态',    
    'Password' => '密码',        
    'Created At' => '创建时间',    
    'Updated At' => '更新时间',
    ];
