<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ImagefitAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/dist';
    public $baseUrl = '@web';
   
    public $js = [
        'js/jquery.imagefit.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
