<?php
$p = \Yii::$app->controller->module->templateAsset."/fuqinjie/";
?>

<style type="text/css">
*
{
    padding: 0px;
    margin: 0px;
    -webkit-box-sizing: border-box;
}

html,body,.main-body
{
    background-color: white;
     height:100%;
}

#container
{
    width: 500px;
    height: 100%;
    position: absolute;
    overflow: hidden;
}
#container > div,#container > img
{
    position: absolute;
}
.machine
{
    left: 0px;
    top: 387px;
}
.guangshu
{
    left: 83px;
    top: 127px;
    -webkit-animation: guangshu 1.6s linear infinite;
}
.lun
{
    position: absolute;
    -webkit-animation: rotateing 7s linear infinite;
}

@-webkit-keyframes rotateing
{
    from  {-webkit-transform: rotate(0deg);}
    to    {-webkit-transform: rotate(360deg);}
}
.circle
{
    -webkit-transform: scale(0.5,1);
}
.circle1
{
    left: 0px;
    top: 385px;
    opacity: 0.4;
}
.circle2
{
    left: 75px;
    top: 320px; 
    opacity: 0.4;
}
.circle3
{
    left: 5px;
    top: 385px;
}
.circle4
{
    left: 80px;
    top: 320px; 
}
@-webkit-keyframes guangshu
{
    0%   {opacity: 1}
    10%  {opacity: 1}
    11%  {opacity: 0.8}
    15%  {opacity: 0.8}
    16%  {opacity: 0.4}
    20%  {opacity: 0.4}
    21%  {opacity: 1}
    60%  {opacity: 1}
    61%  {opacity: 0.6}
    65%  {opacity: 0.6}
    66%  {opacity: 1}
    70%  {opacity: 1}
    71%  {opacity: 0.6}
    75%  {opacity: 0.6}
    76%  {opacity: 1}
    100% {opacity: 1}
}
.bgh
{
    left: 81px;
    top: 137px;
    position: absolute;
}
.divh
{
    left: 103px;
    top: 160px;
    width: 383px;
    height: 266px;
    overflow: hidden;
    position: absolute;
}
.img
{
    position: absolute;
}
.bgv
{
    left: 53px;
    top: 89px;
    position: absolute;
    width: 500px;
    -webkit-transform: rotate(90deg);
}
.divv
{
    left: 148px;
    top: 49px;
    width: 307px;
    height: 450px;
    overflow: hidden;
    position: absolute;
    /*background-color: #fff;*/
}
@-webkit-keyframes fadein
{
    from  {opacity: 0}
    to    {opacity: 1}
}
@-webkit-keyframes fadeout
{
    from  {opacity: 1}
    to    {opacity: 0}
}
.titlebg
{
    position: absolute;
    left: 103px;
    top: 160px;
    width: 383px;
    height: 266px;
}
@-webkit-keyframes fadeupin
{
    from  {-webkit-transform: translate(0px,30px);opacity: 0}
    to    {-webkit-transform: translate(0px,0px);opacity: 1}
}
@-webkit-keyframes fadeupout
{
    from  {-webkit-transform: translate(0px,0px);opacity: 1}
    to    {-webkit-transform: translate(0px,-30px);opacity: 0}
}
</style>

<div id='container'>
    <img src='<?=$p?>images/bg.jpg'>

    <div id='div1' class='img' style='opacity:0;'>
        <div id='box1h' class='img'>
            <img class='bgh' src='<?=$p?>images/guang.png'>
            <div id='div1h' class='divh'>
                <img id='img1h' class='img'>
                <img class='img' src='<?=$p?>images/zaodian.gif'>
            </div>
        </div>

        <div id='box1v' class='img'>
            <img class='bgv' src='<?=$p?>images/guang.png'>
            <div id='div1v' class='divv'>
                <img id='img1v' class='img'>
                <img class='img' src='<?=$p?>images/zaodian.gif'>
            </div>
        </div>
    </div>

    <div id='div2' class='img' style='opacity:0'>
        <div id='box2h' class='img'>
            <img class='bgh' src='<?=$p?>images/guang.png'>
            <div id='div2h' class='divh'>
                <img id='img2h' class='img'>
                <img class='img' src='<?=$p?>images/zaodian.gif'>
            </div>
        </div>

        <div id='box2v' class='img'>
            <img class='bgv' src='<?=$p?>images/guang.png'>
            <div id='div2v' class='divv'>
                <img id='img2v' class='img'>
                <img class='img' src='<?=$p?>images/zaodian.gif'>
            </div>
        </div>
    </div>


    <div id='pagetitle' style='position:absolute;'>
        <img class='bgh' src='<?=$p?>images/guang.png'>
        <img class='titlebg' src='<?=$p?>images/9.jpg'>
        <div id='titlebox' style='position:absolute;width:321px;height:190px;top:20px;left:20px;overflow:hidden;display:table;padding: 10px 10px 10px 10px;overflow:hidden;left:133px;top:194px;opacity:0;'>
            <div id='titlecontent' style='width:360px;height:170px;vertical-align:middle;display:table-cell;text-align:center;font-size:28px;line-height:50px;color:#9A0100;overflow:hidden;'></div>
        </div>
    </div>


    <img class='guangshu' src='<?=$p?>images/guangshu.png'>
    <img class='machine' src='<?=$p?>images/fangyinji.png'>

    <div class='circle1 circle'>
        <img class='lun' src='<?=$p?>images/lun.png'>
    </div>
    <div class='circle2 circle'>
        <img class='lun' src='<?=$p?>images/lun.png'>
    </div>

    <div class='circle3 circle'>
        <img class='lun' src='<?=$p?>images/lun.png'>
    </div>
    <div class='circle4 circle'>
        <img class='lun' src='<?=$p?>images/lun.png'>
    </div>

</div>

<script>
function id(name)
{
    return document.getElementById(name)
}

function showtitle()
{
    id('titlebox').style.webkitAnimation = 'fadeupin 1s ease-out both';
    id('titlecontent').innerHTML = desc;
}

function liangziyun_kawa()
{
    id('titlebox').style.webkitAnimation = 'fadeupout 1s ease-in both';

    timeout[1] = setTimeout(show1,1000)
}

function show1()
{
    setImage('1');
    id('div1').style.webkitAnimation = 'fadein 1s linear both';
    id('pagetitle').style.webkitAnimation = 'fadeout 1s linear both';

    timeout[2] = setTimeout(show2,4000)
}

function show2()
{
    setImage('2');
    id('div2').style.webkitAnimation = 'fadein 1s linear both';
    id('div1').style.webkitAnimation = 'fadeout 1s linear both';

    timeout[3] = setTimeout(show3,4000)
}

function show3()
{
    setImage('1');
    id('div1').style.webkitAnimation = 'fadein 1s linear both';
    id('div2').style.webkitAnimation = 'fadeout 1s linear both';

    timeout[4] = setTimeout(show2,4000)
}


call_me(load_images)
var image_size_width=[];
var image_size_height=[];
var image_url_index=0;
var have_num = 0;
var error_num = 0;
var canshow = true;
var reshow = false;
var delaytime=3500;
var timeout = [];
var showfont = false;




//ÿ��ִ�м��غ�10��ͼƬ
var bl_keepload = 'first';
var step_loadnum = 5;
function step_load()
{
    var load_num = 0
    //��������X��
    if(image_url_index  == 0 && bl_keepload == 'first')
    {
        console.log('loading continue');
        if(slider_images_url.length > step_loadnum)
        {
            load_num = step_loadnum;
            bl_keepload = 'next';
        }
        else
        {
            load_num = slider_images_url.length;
            bl_keepload = 'end';
        }
        for(var i = 0; i< load_num; i++)
        {
            var img=new Image();
            img.index=i;
            img.src=slider_images_url[i];
            img.onload=image_onload;
            img.onerror= image_onerror;
            Onload_imgs_url[i] = 'loading';
        }
    }
    else if(bl_keepload == 'end')
    {
        return;
    }
    else
    {
         console.log('loading continue');
        if(slider_images_url.length - image_url_index >step_loadnum*2)
        { 
            load_num = step_loadnum;
        }
        else
        {
            load_num = slider_images_url.length - image_url_index - step_loadnum;
            bl_keepload = 'end';
        }
        for(var i = image_url_index +step_loadnum; i< image_url_index + step_loadnum + load_num; i++)
        {
            var img=new Image();
            img.index=i;
            img.src=slider_images_url[i];
            img.onload=image_onload;
            img.onerror= image_onerror;
            Onload_imgs_url[i] = 'loading';
        }
    }
}

function load_images()
{ 
    reshow = false;
    image_size_width=[];
    image_size_height=[];
    Onload_imgs_url=[];
    image_url_index=0;
    have_num = 0;
    error_num = 0;
    begin_titletime = new Date();
    begin_titletime = begin_titletime.getTime();
    canshow = true;
    showtitle();
    bl_keepload = 'first';
    // loading_first(); 
    step_load();     
}

function image_onerror(event)
{
    var img = event.target;
    var index = img.index;
    if(index<step_loadnum)
        error_num ++;
    Onload_imgs_url[index] = 'not find';

    if((have_num+error_num >= step_loadnum || slider_images_url.length == (have_num+error_num)) && canshow == true)
    {   
        reshow = false;
        canshow =false;
        if(have_num == 0)
            return;
        var end_titletime = new Date();
        end_titletime = end_titletime.getTime();
        var dis_titletime = Math.abs(end_titletime-begin_titletime);
        if(dis_titletime>delaytime)
        {
            liangziyun_kawa();
        }
        else
        {
            dis_titletime = delaytime- dis_titletime;
            timeout[0] = setTimeout(function()
                {
                    liangziyun_kawa();
                },dis_titletime);
        }
    }
}

function image_onload(event)
{
    if(reshow == true)
        return;

    var img = event.target;
    var index = img.index;

    if(index<step_loadnum)
    {
        have_num++;
    }
    Onload_imgs_url[index] = img.src;
    image_size_height[index] = img.height;
    image_size_width[index] = img.width;

    console.log(Onload_imgs_url[index]);

    if((have_num + error_num >= step_loadnum ||slider_images_url.length == (have_num+error_num)) && canshow == true)
    {   
        reshow = false;
        canshow =false;
        if(have_num == 0)
            return;
        var end_titletime = new Date();
        end_titletime = end_titletime.getTime();
        var dis_titletime = Math.abs(end_titletime-begin_titletime);
        if(dis_titletime>delaytime)
        {
            liangziyun_kawa();
        }
        else
        {
            dis_titletime = delaytime- dis_titletime;
            timeout[0] = setTimeout(function()
                {
                    liangziyun_kawa();
                },dis_titletime);
        }

    }
}

function setImage(idname)
{
    if(reshow == true)
        return;

    while(Onload_imgs_url[image_url_index] == 'not find' || Onload_imgs_url[image_url_index] == 'loading')
    {
        if(image_url_index % step_loadnum == 0)
        {
            step_load();
        }
        image_url_index++;
        if(image_url_index == Onload_imgs_url.length)
            image_url_index = 0;
    }

    if(image_url_index % step_loadnum == 0)
    {
        step_load();
    }
    var img_bili = image_size_width[image_url_index]/image_size_height[image_url_index];

    var div;
    var div1;
    var img;

    if(img_bili > 1)
    {
        div = id('box'+idname + 'h');
        div1 = id('box'+idname + 'v');
        idname = idname + 'h';
        width = 383;
        height = 266;
    }
    else
    {
        div = id('box'+idname + 'v');
        div1 = id('box'+idname + 'h');
        idname = idname + 'v';
        width = 307;
        height = 450;
    }

    div.style.display = 'block';
    div1.style.display = 'none';



    var img = id('img'+idname);
    img.src = Onload_imgs_url[image_url_index];
    console.log('setimg:'+img.src);

    if(img_bili > (width/height))
    {
        img.style.top = '0px';
        img.style.height = height + 'px';
        img.style.width = height*img_bili + 'px';
        img.style.left = -((height*img_bili-width)/2)+'px';
    }
    else
    {
        img.style.left = '0px';
        img.style.width = width+'px';
        img.style.height = width/img_bili + 'px';
        img.style.top = -((width/img_bili-height)/2) + 'px';
    }
    image_url_index ++;
    if(image_url_index == Onload_imgs_url.length)
      image_url_index = 0;
}
function reload_scene()
{
    clearnode();
    reshow = true;
    setTimeout(load_images,100);
}

function clearnode()
{

    for(var i = 0; i<timeout.length; i++)
    {
        clearTimeout(timeout[i])
    }

    id('titlecontent').innerHTML = '';

    var elements = ['div1','div2','pagetitle','titlebox'];
    for(var i=0;i<elements.length;i++)
    {
        id(elements[i]).style.webkitAnimation = '';
    }
    id('box1v').style.display = 'none';
    id('box2v').style.display = 'none';
    id('box1h').style.display = 'none';
    id('box2h').style.display = 'none';


}
</script>