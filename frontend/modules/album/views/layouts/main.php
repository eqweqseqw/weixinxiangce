<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use common\tools\WeChatTool;
use app\modules\album\assets\AlbumAppAsset;
AlbumAppAsset::register($this);
$weixinConf = \Yii::$app->params["weixinConf"];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src='http://res.wx.qq.com/open/js/jweixin-1.0.0.js'></script>
</head>
<body>
<?php 
$this->beginBody();
$time = time();   
$nonceStr = WeChatTool::createNonceStr();
?>
<script>      
wx.config({
    debug: false,
    appId: '<?php echo $weixinConf["appid"];?>',
    timestamp: <?php echo $time;?>,
    nonceStr: '<?php echo $nonceStr;?>',
    signature: '<?php echo WeChatTool::getSignature($time,$nonceStr);?>',
    jsApiList: [
        'chooseImage',    
        'previewImage',    
        'uploadImage',    
    ]
});   
</script>  
<?php $this->beginBody() ?>


<div class="container">
    <?= $content ?>
</div>

<footer class="footer">
        <p class="pull-left">&copy; 相册号 <?= date('Y') ?></p>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
