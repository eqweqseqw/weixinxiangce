<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class IPresenterAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web';
   
    public $js = [
        'js/ipresenter.packed.js',
    ];
    public $depends = [
        'app\modules\album\assets\AlbumAsset',
    ];
}
