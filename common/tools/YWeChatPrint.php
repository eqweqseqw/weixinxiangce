<?php

/* 
 * 自定义输出工具类
 */
namespace common\tools;

use common\tools\WXmlResponseFormatter as WXRF;

class YWeChatPrint{
    //回复文本消息
    static public function print_text($fromusername,$tousername,$content,$time=null){
        if($time==null){
           $time = time(); 
        }
        return [
                "ToUserName"=>[$fromusername,WXRF::CDATA=>true],
                "FromUserName"=>[$tousername,WXRF::CDATA=>true],
                "CreateTime"=>time(),
                "MsgType"=>['text',WXRF::CDATA=>true],
                "Content"=>[$content,WXRF::CDATA=>true],         
            ];	
    }
    //回复图片消息
    static public function print_image($fromusername,$tousername,$imageArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        return [
                "ToUserName"=>[$fromusername,WXRF::CDATA=>true],
                "FromUserName"=>[$tousername,WXRF::CDATA=>true],
                "CreateTime"=>time(),
                "MsgType"=>['image',WXRF::CDATA=>true],
                "Image"=>[
                    "MediaId"=>[$imageArray['MediaId'],WXRF::CDATA=>true],
                ]         
            ];	    
    }
    //回复声音消息
    static public function print_voice($fromusername,$tousername,$voiceArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        return [
                "ToUserName"=>[$fromusername,WXRF::CDATA=>true],
                "FromUserName"=>[$tousername,WXRF::CDATA=>true],
                "CreateTime"=>time(),
                "MsgType"=>['voice',WXRF::CDATA=>true],
                "Voice"=>[
                    "MediaId"=>[$voiceArray['MediaId'],WXRF::CDATA=>true],
                ]         
            ];	        
    }
    //回复视频消息
    static public function print_video($fromusername,$tousername,$videoArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        return [
              "ToUserName"=>[$fromusername,WXRF::CDATA=>true],
              "FromUserName"=>[$tousername,WXRF::CDATA=>true],
              "CreateTime"=>time(),
              "MsgType"=>['voice',WXRF::CDATA=>true],
              "Video"=>[
                  "MediaId"=>[$videoArray['MediaId'],WXRF::CDATA=>true],
                  "ThumbMediaId"=>[$videoArray['ThumbMediaId'],WXRF::CDATA=>true],
                  "Title"=>[$videoArray['Title'],WXRF::CDATA=>true],
                  "Description"=>[$videoArray['Description'],WXRF::CDATA=>true],
              ]         
          ];	          
    }
    //回复图文消息
    static public function print_news($fromusername,$tousername,$arr_item,$time=null){
        if(!is_array($arr_item))
            return;
        if($time==null){
           $time = time(); 
        }       
        $item_arr = array();
        $sum = count($arr_item);
        foreach ($arr_item as $item){
            array_push($item_arr, [             
                    "Title"=>[$item['Title'],WXRF::CDATA=>true],
                    "Description"=>[$item['Description'],WXRF::CDATA=>true],
                    "PicUrl"=>[$item['PicUrl'],WXRF::CDATA=>true],
                    "Url"=>[$item['Url'],WXRF::CDATA=>true],                        
            ]);
        }
                
        return [
              "ToUserName"=>[$fromusername,WXRF::CDATA=>true],
              "FromUserName"=>[$tousername,WXRF::CDATA=>true],
              "CreateTime"=>time(),
              "MsgType"=>['news',WXRF::CDATA=>true],
              "ArticleCount"=>count($arr_item),
              "Articles"=>$item_arr,            
        ];	         
    }
    //回复音乐消息
    static public function print_music($fromusername,$tousername,$musicArray,$time=null){
        if($time==null){
           $time = time(); 
        }
        return [
              "ToUserName"=>[$fromusername,WXRF::CDATA=>true],
              "FromUserName"=>[$tousername,WXRF::CDATA=>true],
              "CreateTime"=>time(),
              "MsgType"=>['music',WXRF::CDATA=>true],
               "Music"=>[
                   "Title"=>[ $musicArray['Title'],WXRF::CDATA=>true],
                   "Description"=>[ $musicArray['Description'],WXRF::CDATA=>true],
                   "MusicUrl"=>[ $musicArray['MusicUrl'],WXRF::CDATA=>true],
                   "HQMusicUrl"=>[ $musicArray['HQMusicUrl'],WXRF::CDATA=>true],
               ]              
        ];	           
    }   
}
