function objid(id){
    return document.getElementById(id);
}
//function play_music(){
//    if(music_url == ''){
//        return ;
//    }
//    music_player.src  = music_url;
//    music_player.loop = 'loop';
//    music_player.play();
//
//    if(objid('sound_image'))
//    {
//        objid('sound_image').style.webkitAnimation     = "zhuan 1s linear infinite";
//    }
//
//    bplay = 1;
//}  
function create_music(){
    if(music_url == '')
    {
        return ;
    }   
    music_player.src  = music_url;
    music_player.loop = 'loop';
    music_player.pause();
//
//    if(objid('sound_image'))
//    {
//        objid('sound_image').style.webkitAnimation     = "zhuan 1s linear infinite";
//    }
    sound_div = document.createElement("div");
    sound_div.setAttribute("ID", "cardsound");
    sound_div.style.cssText = "position:absolute;right:20px;top:25px;z-index:50;visibility:visible;";
    sound_div.onclick = switchsound;
    bg_htm  = "<img id='sound_image' src='"+ albumAssetsDir +"/images/music_note_big.png' style='-webkit-animation:'>";
    sound_div.innerHTML = bg_htm  ;
    document.body.appendChild(sound_div);
} 
call_me(create_music);
//call_me(play_music);
var bplay = 0; 
function switchsound(event){
    event.stopPropagation();
    au = music_player;
    ai = objid('sound_image');
    if(au.paused){
        au.play();
        ai.style.webkitAnimation     = "zhuan 1s linear infinite";
    }else{
        au.pause();
        ai.style.webkitAnimation     = "";
    }
}		


$(function(){
//    create_music();
    load_init_modules();
    $("#cardsound").click();
    
//    $("#cardsound").trigger("click")
    //底部菜单
    $("#template-b").bind("click",function(){
        var _this = $(this);   
        window.location.href="/album/app/changetemplate?aid="+_this.parent(".row").data("aid");
    })  
    $("#music-b").bind("click",function(){
        var _this = $(this);   
        window.location.href="/album/app/changemusic?aid="+_this.parent(".row").data("aid");
    })  
    $("#title-b").bind("click",function(){
        var _this = $(this);   
        window.location.href="/album/app/changetitle?aid="+_this.parent(".row").data("aid");
    })  
    $("#pic-b").bind("click",function(){
        var _this = $(this);   
        window.location.href="/album/app/changepic?aid="+_this.parent(".row").data("aid");
    }) 
    $("#collect-b").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "app/collect",//路径  
            data : {  
                aid : _this.parents(".row").data("aid") 
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                  alert( result.message );
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });      
    })
    $("#zan-b").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "app/zan",//路径  
            data : {  
                aid : _this.parents(".row").data("aid"),
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                  alert( result.message );
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });      
    })
    //预览页
    $("#template-ok").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _parent = $(this).parents(".row");
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "/album/app/dochange",//路径  
            data : {  
                template : _parent.data("templateid"),
                aid : _parent.data("aid"),
                changeMusic : 1,
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                    window.location.href="/album/"+result.aid;   
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });  
    })
    //只保存模板
    $("#template-ko").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _parent = $(this).parents(".row");
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "/album/app/dochange",//路径  
            data : {  
                template : _parent.data("template"),
                aid : _parent.data("aid"),
                changeMusic : 0,
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                    window.location.href="/album/"+result.aid;   
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });  
    })
    $("#cancel-b").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        history.go(-1);
    })
//    $(document).bind("click",function(){
//        alert("test");
//        $(".footer-option").toggle();     
//    })
//    $(window).click(function(){
//        alert("test");
//        $(".footer-option").toggle();     
//    })
    $(".main-body").bind("click",function(){
        $(".footer-option").toggle();     
    })
    if(footerClose)
    setTimeout('$(".footer-option").hide()',10000); 
})

