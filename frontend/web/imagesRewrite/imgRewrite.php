<?php 
//session_start(); 
define('ROOT', dirname(__FILE__).'/../');
class imgRewrite
{
    private $_file;
    private $_w;
    private $_h;
    private $_vw;
    private $_vh;
    private $_get;
    private $_path;
    private $_source;
    private $_info;
    private $_flag;
    private $_w_h;
    private $_type;
    private $type;
    private $_dir;
    private $_newfile;
    private $_name;

    public function __construct(){
        $this->_get   = $_GET;
        $this->_file  = $this->_get['file'];
        $this->_w     = $this->_get['w'];
        $this->_h     = $this->_get['h'];
        $this->_vw     = 500;
        $this->_vh     = 800;
        $this->_path  = ROOT.$this->_file;
        $this->_dir   = dirname($this->_path);
        $pathinfo     = pathinfo($this->_file);		
        $this->_name  = $pathinfo['filename'];		
        $this->type   = @end(explode('.', $this->_file));
    }
    private function _get_img(){		
        $this->_info = getimagesize($this->_path);
        $this->_type = image_type_to_extension($this->_info[2],false);
        if(empty($this->_type)){
            $this->_echo404();
        }
        $fun = "imagecreatefrom{$this->_type}";
        $this->_source = $fun($this->_path);
    }
    private function _check(){
        $w = $this->_info[0];
        $h = $this->_info[1];
        if($w>=$h){			
            $this->_flag = true;
        }else{			
            $this->_flag = false;
        }
        if($w<=$this->_vw&&$h<=$this->_vh)
            return false;
        return true;
    }
    private function _get_thumb(){	
        $start_w = $this->_vw>$this->_info[0]?($this->_w-$this->_info[0])/2:($this->_w-$this->_vw)/2;
        $start_h = $this->_vh>$this->_info[1]?($this->_h-$this->_info[1])/2:($this->_h-$this->_vh)/2;
        $end_w   = $this->_vw>$this->_info[0]?$this->_info[0]:$this->_vw;
        $end_h   = $this->_vh>$this->_info[1]?$this->_info[1]:$this->_vh;
        $image_thumb = imagecreatetruecolor($this->_w, $this->_h);
        //$image_thumb = imagecolorallocate($image, 255, 255, 255);              
//        $image_back = imagecolorallocatealpha($image_thumb, 255, 255,255,127);
//        imagesavealpha($image_thumb,true);
//        imagefilledrectangle($image_thumb, 0, 0, $this->_w, $this->_h, $image_back);    
//		imagealphablending($image_thumb,false);	
//		imagesavealpha($image_thumb,true);	
        $color=imagecolorallocate($image_thumb,255,255,255);
        imagecolortransparent($image_thumb,$color);
        imagefill($image_thumb,0,0,$color);
        imagecopyresampled($image_thumb, $this->_source, $start_w,$start_h, $this->_w_h[0], $this->_w_h[1],$end_w, $end_h, $this->_w_h[2], $this->_w_h[3]);
        //imagecopyresampled($image_thumb, $this->_source, 0, 0, 45, 0, $w, $h, 100, 190);
        imagedestroy($this->_source);
        $this->_source = null;
        //$func = "image{$this->_type}";	
		$func = "imagepng";	
        $func($image_thumb,$this->_newfile);			
    }	
    private function _get_w_h(){
        $differ = 0;
        $k      = 1;
        if($this->_info[0]>$this->_vw&&$this->_info[1]>=$this->_vh){
            $k_h             = $this->_info[1]/$this->_vh;
            $k_w             = $this->_info[0]/$this->_vw;
            if($k_h>=$k_w){
                $k = $k_w;
                $differ        = ($this->_info[1]-$this->_vh*$k)/2;
                $this->_w_h[0] = 0;
                $this->_w_h[2] = $this->_info[0];				
                $this->_w_h[1] = $differ;
                $this->_w_h[3] = $this->_vh*$k;
            }else{
                $k = $k_h;
                $differ        = ($this->_info[0]-$this->_vw*$k)/2;
                $this->_w_h[0] = $differ;
                $this->_w_h[2] = $this->_vw*$k;				
                $this->_w_h[1] = 0;
                $this->_w_h[3] = $this->_info[1];
            }                                                                        
        }else if($this->_info[0]>$this->_vw&&$this->_info[1]<$this->_vh){
            $differ        = ($this->_info[0]-$this->_vw)/2;
            $this->_w_h[0] = $differ;
            $this->_w_h[2] = $this->_vw;				
            $this->_w_h[1] = 0;
            $this->_w_h[3] = $this->_info[1];

        }else if($this->_info[0]<=$this->_vw&&$this->_info[1]>=$this->_vh){ 
                $this->_w_h[0] = 0;
                $this->_w_h[2] = $this->_info[0];
                $differ        = ($this->_info[1]-$this->_vh)/2;
                $this->_w_h[1] = $differ;
                $this->_w_h[3] = $this->_vh;
        }
    }
    private function _get_w_h_s(){         
        $this->_w_h[0] = 0;
        $this->_w_h[2] = $this->_info[0];				
        $this->_w_h[1] = 0;
        $this->_w_h[3] = $this->_info[1];

    }

    public function echo_img(){
        if(!file_exists($this->_path)){
            $this->_echo404();
        }
        $file;
        $this->_get_img();
        $this->_newfile = $this->_dir.'/'.$this->_name.'_'.$this->_w.'x'.$this->_h.'.png';
        if(!file_exists($this->_newfile)){			
            if(!$this->_check()){
                //$file = $this->_path;
                $this->_get_w_h_s();
            }else{
                $this->_get_w_h();

            } 
            $this->_get_thumb();                   
        }
        $file = $this->_newfile;
        $fun    = "imagecreatefrompng";              
        $source = $fun($file);
        //header('Content-type:'.$this->_info['mime']);
		header('Content-type:image/png');
       // $func = "image{$this->_type}";
		$func = "imagepng";
        $func($source);	
    }
    private function _echo($source){
        header('Content-type:'.$this->_info['mime']);
        $func = "image{$this->_type}";
        $func($source);
        die;
    }
	public function getFile(){
		return $this->_newfile;
	}
    private function _echo404(){
        header("HTTP/1.1 404 Not Found");  
        header("Status: 404 Not Found");  
        exit;  
    }
}
$img = new imgRewrite();
//$seconds_to_cache = 3600;
//$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
//header("Expires: $ts"); 
//header("Pragma: private");
//header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($img->getFile())) . ' GMT');
//header("Cache-Control: max-age=$seconds_to_cache");
//if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) 
//       && 
//  (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($img->getFile()))) {
//  // send the last mod time of the file back
//  header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($img->getFile())).' GMT', 
//  true, 304);
//  exit;
//}
$img->echo_img();