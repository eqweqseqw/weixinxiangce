function EVA_ALBUM_menghuanxingkong(t) {
    function i() {
        m._initElements()
    }
    function e(t, i, e, s) {
        var a = new Image;
        a.onload = function(n) {
            t.src = s;
            var o = calcImgClip(i, e, a.width, a.height);
            t.style.left = 0 - o.clipLeft + "px",
            t.style.top = 0 - o.clipTop + "px",
            t.style.width = o.objWidth + "px",
            t.style.height = o.objHeight + "px",
            t.style.clip = "rect(" + o.clipTop + "px " + Math.floor(o.clipLeft + i) + "px " + Math.floor(o.clipTop + e) + "px " + o.clipLeft + "px)"
        },
        a.src = s
    }
    M = Math.max(3, Math.min(g_images.length, 12)),
    this.maxWidth = t.max_width,
    this.maxHeight = t.max_height,
    this.timer_auto_move = null,
    this.timer_delay_move = null,
    this.inited = !1;
    var s = $node(t.canvas_id);
    this.div_root = document.createElement("div"),
    this.div_root.id = "EVA_ALBUM_menghuanxingkong",
    s.appendChild(this.div_root),
    this.div_root.style.width = this.maxWidth + "px",
    this.div_root.style.height = this.maxHeight + "px",
    this.div_root.innerHTML = '                <img id="EVA_ALBUM_menghuanxingkong_bkgnd">                <div id="EVA_ALBUM_menghuanxingkong_star_wrapper"></div>                <div id="EVA_ALBUM_menghuanxingkong_Axis"></div>';
    var a = Math.min(this.maxWidth, this.maxHeight);
    this.maxImgWidth = Math.floor(.3 * a),
    this.maxImgHeight = Math.floor(.4 * a);
    var n = document.getElementById("EVA_ALBUM_menghuanxingkong_bkgnd");
    e(n, this.maxWidth, this.maxHeight, APP_URL+"images/bg.jpg");
    for (var o = document.getElementById("EVA_ALBUM_menghuanxingkong_star_wrapper"), h = [[.4 * this.maxWidth, 10], [.2 * this.maxWidth, 80], [.7 * this.maxWidth, 60], [.75 * this.maxWidth, 50], [.8 * this.maxWidth, 30], [.85 * this.maxWidth, 40], [.65 * this.maxWidth, 60], [.45 * this.maxWidth, 150], [.6 * this.maxWidth, 210]], r = 0; r < h.length; ++r) {
        new t001_pentagon_flare(o, h[r][0], h[r][1])
    }
    this.div1 = document.getElementById("EVA_ALBUM_menghuanxingkong_Axis"),
    this.div1.style.height = this.div1.style.width = this.maxImgHeight + "px",
    this.div1.style.marginTop = this.div1.style.marginLeft = (0 - this.maxImgHeight) / 2 + "px",
    this.mouseStartX = 0,
    this.mouseStartY = 0,
    this.startX = 0,
    this.startY = 0,
    this.lastX = 0,
    this.lastY = 0,
    this.beginDrag = !1,
    this.div_images = this.div1.getElementsByClassName("ring_img_item"),
    this.x = -10,
    this.y = 0;
    var m = this;
    loadCSSFile(APP_URL + "main.css", i, i, "EVA_ALBUM_menghuanxingkong_Check_Loaded"),
    this._initElements = function() {
        if (1 == this.inited) return void log("warning", "re init");
        this.inited = !0;
        for (var t = this,
        i = 2,
        s = this.maxImgWidth + 2 * i,
        a = this.maxImgHeight + 2 * i,
        n = 1; M >= n; n++) {
            var o = document.createElement("div");
            o.className = "ring_img_item",
            o.style.width = s + "px",
            o.style.height = a + "px",
            o.style.marginLeft = 0 - s / 2 + "px",
            o.style.marginTop = 0 - a / 2 + "px",
            o.degY = 360 * (n - 1) / M,
            o.innerHTML = '<div class="ring_img_wrapper"><img></div>',
            o.onclick = this.onImgClick;
            var h = o.getElementsByClassName("ring_img_wrapper")[0];
            h.style.width = this.maxImgWidth + "px",
            h.style.height = this.maxImgHeight + "px",
            h.style.left = (s - this.maxImgWidth) / 2 + "px",
            h.style.top = (a - this.maxImgHeight) / 2 + "px";
            var r = getNextImg(),
            m = h.getElementsByTagName("img")[0];
            e(m, this.maxImgWidth, this.maxImgHeight, r.src),
            this.div1.appendChild(o)
        }
        $listen(this.div_root, "mousedown",
        function(i) {
            var e = i || event;
            t.onmousedown(e.clientX, e.clientY)
        }),
        $listen(this.div_root, "touchstart",
        function(i) {
            var e = i.touches[0];
            t.onmousedown(e.clientX, e.clientY)
        }),
        $listen(this.div_root, "mousemove",
        function(i) {
            var e = i || event;
            t.onmousemove(e.clientX, e.clientY)
        }),
        $listen(this.div_root, "touchmove",
        function(i) {
            var e = i.touches[0];
            t.onmousemove(e.clientX, e.clientY)
        }),
        $listen(this.div_root, "mouseup",
        function() {
            t.onmouseup()
        }),
        $listen(this.div_root, "touchend",
        function() {
            t.onmouseup()
        })
    },
    this.fixAll = function() {
        var t = this.y % 360;
        this.div1.style.WebkitTransform = "perspective(1000px) rotateX(" + this.x + "deg) rotateY(" + t + "deg)",
        this.div1.style.MozTransform = "perspective(1000px) rotateX(" + this.x + "deg) rotateY(" + t + "deg)",
        this.div1.style.msTransform = "perspective(1000px) rotateX(" + this.x + "deg) rotateY(" + t + "deg)",
        this.div1.style.OTransform = "perspective(1000px) rotateX(" + this.x + "deg) rotateY(" + t + "deg)",
        this.div1.style.transform = "perspective(1000px) rotateX(" + this.x + "deg) rotateY(" + t + "deg)";
        for (var i = 0; i < this.div_images.length; i++) {
            var s = this.div_images[i].degY + t,
            a = (s % 360 + 360) % 360;
            a = Math.abs(180 - a);
            var n = .1 + a / 180 * .9;
            if (n = n > .6 ? 1 : .2 > n ? .2 : n, .2 == n && .2 != this.div_images[i].style.opacity) {
                var o = this.div_images[i].getElementsByClassName("ring_img_wrapper")[0],
                h = getNextImg(),
                r = o.getElementsByTagName("img")[0];
                e(r, this.maxImgWidth, this.maxImgHeight, h.src)
            }
            this.div_images[i].style.opacity = n
        }
    },
    this.onmousedown = function(t, i) {
        return this.beginDrag = !0,
        this.stopAutoMove(),
        this.mouseStartX = t,
        this.mouseStartY = i,
        this.startX = this.x,
        this.startY = this.y,
        this.lastX = this.mouseStartX,
        this.lastY = this.mouseStartY,
        speedX = speedY = 0,
        this.stopMove(),
        !0
    },
    this.realRotateX = function(t) {
        return t > 20 ? t = 20 : -20 > t && (t = -20),
        t
    },
    this.onmousemove = function(t, i) {
        1 == this.beginDrag && (this.y = this.startY + (t - this.mouseStartX) / 10, this.x = this.realRotateX(this.startX - (i - this.mouseStartY) / 10), speedX = (t - this.lastX) / 2, speedY = (i - this.lastY) / 2, this.fixAll(), this.lastX = t, this.lastY = i)
    },
    this.onmouseup = function() {
        1 == this.beginDrag && (this.beginDrag = !1, this.startMove())
    },
    this.startMove = function() {
        clearInterval(this.timer_delay_move);
        var t = this;
        this.timer_delay_move = setInterval(function() {
            t.x = t.realRotateX(t.x - speedY),
            t.y += speedX,
            speedY *= .93,
            speedX *= .93,
            Math.abs(speedX) < .1 && Math.abs(speedY) < .1 && t.stopMove(),
            t.fixAll()
        },
        30)
    },
    this.stopMove = function() {
        clearInterval(this.timer_delay_move),
        this.startAutoMove()
    },
    this.stop = function() {},
    this.play = function() {
        this.div_root.style.display = "block";
        var t = this,
        i = window.setInterval(function() {
            if (t.inited === !0) {
                window.clearInterval(i);
                for (var e = t.div1.getElementsByClassName("ring_img_item"), s = t.maxImgWidth / (2 * Math.tan(Math.PI / M)) + 100, a = 0; a < e.length; ++a) {
                    var n = e[a]; !
                    function(i, e) {
                        setTimeout(function() {
                            var a = "rotateY(" + 360 * e / M + "deg) translateZ(" + s + "px)";
                            i.style.WebkitTransform = a,
                            i.style.MozTransform = a,
                            i.style.msTransform = a,
                            i.style.OTransform = a,
                            i.style.transform = a,
                            setTimeout(function() {
                                e == M && t.fixAll(),
                                setTimeout(function() {
                                    i.style.WebkitTransition = "none",
                                    i.style.MozTransition = "none",
                                    i.style.msTransition = "none",
                                    i.style.OTransition = "none",
                                    i.style.transition = "none",
                                    t.startAutoMove()
                                },
                                100)
                            },
                            1e3)
                        },
                        200 * (M + 3 - e))
                    } (n, a)
                }
            }
        },
        20)
    },
    this.setBkgnd = function(t) {},
    this.startAutoMove = function() {
        window.clearInterval(this.timer_auto_move);
        var t = this;
        this.timer_auto_move = window.setInterval(function() {
            t.x -= 0,
            t.y += -.1,
            t.fixAll()
        },
        30)
    },
    this.stopAutoMove = function() {
        window.clearInterval(this.timer_auto_move)
    },
    this.onImgClick = function() {
        g_albumPlayer.previewImage(this.getElementsByTagName("img")[0].src)
    }
}
function getNextImg() {
    return g_curImgIndex >= g_images.length ? g_curImgIndex = 0 : 0 > g_curImgIndex && (g_curImgIndex = 0),
    g_images.length < 1 ? null: g_images[g_curImgIndex++]
}
function t001_pentagon_flare(t, i, e) {
    this.oFlare = document.createElement("img"),
    this.oFlare.className = "t001_star",
    this.oFlare.src = APP_URL+"images/star.png",
    this.oFlare.style.width = "43px",
    this.oFlare.style.height = "44px";
    var s = 90 * Math.random(),
    a = 0,
    n = .8 * Math.random() + .2;
    this.oFlare.style.webkitTransform = "rotate(" + s + "deg) translate3d(0,0," + a + "px)scale3d(" + n + "," + n + "," + n + ")",
    this.oFlare.style.left = i + "px",
    this.oFlare.style.top = e + "px";
    var o = parseInt(5 + 5 * Math.random());
    this.oFlare.style.webkitAnimationDuration = o + "s",
    t.appendChild(this.oFlare);
    var h = this;
    window.setTimeout(function() {
        h.oFlare.style.display = "block"
    },
    1e3 * Math.random())
}
var M = 3,
g_curImgIndex = 0,
speedX = 0,
speedY = 0;