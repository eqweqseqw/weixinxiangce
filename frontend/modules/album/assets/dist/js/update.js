var mySwiper;
$(function(){
    $(".nav-tabs").find("li>a").bind("click",function(){
        if($(this).attr("href")=="#tab_2" && $("#tab_2").is(":hidden")){
            $("#tab_2").addClass("active");
            mySwiper = new Swiper('.swiper-container', {
                loop : true,
                calculateHeight:true,
                visibilityFullFit: true,  
                onInit:function(){
                    $(".img-thumbnail").css("max-height",$(window).height()-$(".nav-tabs").height()-11)
                }
            }) 
        }
    })     
})
