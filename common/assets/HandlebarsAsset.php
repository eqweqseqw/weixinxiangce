<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HandlebarsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/dist/handlebars';
    public $baseUrl = '@web';
    public $css = [
      
    ];
    public $js = [
        'handlebars-v4.0.5.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
