<?php
namespace common\wechat;

use common\wechat\interfaces\Ireply; 
use common\wechat\clicks\ReplyCustomService;
use common\tools\YWeChatPrint;
use common\wechat\clicks\ReplyAlbumCreateService;
use common\wechat\clicks\ReplyAlbumViewService;
use common\wechat\clicks\ReplyLoginService;

class ReplyClickEvent implements Ireply
{
    protected $_postObject;
    protected $_fromUserName;
    protected $_toUserName;
    protected $_eventkey;
    protected $_time;
    
    public function __construct($postObj) {
        // 获取参数   
        $this->_postObject = $postObj;        
        $this->_fromUserName = $this->_postObject->FromUserName;      
        $this->_toUserName =  $this->_postObject->ToUserName;      
        $this->_eventkey = $this->_postObject->EventKey;      
    }
    
   
    public function createReMsg(){
    	switch ($this->_eventkey)
    	{          
            case "customerService"://调用多客服系统     
                $worker = new ReplyCustomService($this->_postObject);
                $result = $worker->createReMsg();  
                break;	                									
            case "albumCreate"://创建相册   
                $worker = new ReplyAlbumCreateService($this->_postObject);
                $result = $worker->createReMsg();  
                break;	                									
            case "albumView"://我的相册   
                $worker = new ReplyAlbumViewService($this->_postObject);
                $result = $worker->createReMsg();  
                break;	                									
            case "login"://登录  
                $worker = new ReplyLoginService($this->_postObject);
                $result = $worker->createReMsg();  
                break;	                									
            default:
                $content = "你好！";
                $result = YWeChatPrint::print_text($this->_fromUserName, $this->_toUserName, $content);
                break;
        }
    	return $result;
    }                
}

