<?php
namespace common\wechat\libs;

use common\wechat\libs\FactoryAbstract;
use common\wechat\ReplyText;
/* 
 * 处理text类消息.工厂
 */

class TextFactory extends FactoryAbstract{
    protected function makeMetod() {
        return new ReplyText($this->_postObj);             
    }   
}