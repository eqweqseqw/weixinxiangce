<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * AlbumApp model
 *
 * @property integer $id
 * @property integer $app_id 关联相册id
 * @property string $openid openid
 * @property string $pic 封面
 * @property integer $des 简介
 * @property integer $show 是否显示
 * @property integer $created_at 创建时间
 * @property integer $updated_at 更新时间
 */
class AlbumAppItemModel extends ActiveRecord
{
   
    const SHOW_OFF = 0;//不删除
    const SHOW_ON = 1;//删除


    /**
     * @wangwei
     */
    public static function tableName()
    {
        return '{{%album_app_item}}';
    }

    /**
     * @wangwei
     * 时间处理
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @wangwei
     * 默认值
     */
    public function rules()
    {
        return [
            ['show', 'default', 'value' => self::SHOW_ON],
            ['show', 'in', 'range' => [self::SHOW_ON, self::SHOW_OFF]],
        ];
    }
     /**
     * @wangwei
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'app_id' => '相册',
            'openid' => "openid",
            'pic' => "封面",
            'des' => "简介",
            'show' => "是否显示",
            'created_at' => "创建时间",
            'updated_at' => "更新时间",
        ];
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     * @param int $show 是否可见（0不可见，1可见）
     */
    public static function findOneById($id,$show=0)
    {
        return static::findOne(['id' => $id, ['>=','show',$show]]);
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id   
     */
    public function getAlbum()
    {
        // 第一个参数为要关联的子表模型类名，
        // 第二个参数指定 通过子表的app_id，关联主表的id字段
        return $this->hasOne(AlbumApp::className(), ['id' => 'app_id']);
    }
    
    /**
     * Finds albumApp by userid
     *
     * @param int $userid 用户id
     * @param int $show 是否可见（0不可见，1可见）
     * @return static|null
     */
    public static function findByOpenid($openid,$show=0)
    {
        return static::findOne(['openid' => $openid, ['>=','show',$show]]);
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }     
}
