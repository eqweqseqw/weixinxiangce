<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\assets\ImagefitAsset;

ImagefitAsset::register($this);
?>
<style>
    .img{
        min-width: 100%;
    }
    /* USER PROFILE PAGE */
 .card {
    padding: 30px 0;
    background-color: rgba(214, 224, 226, 0.2);
    -webkit-border-top-left-radius:5px;
    -moz-border-top-left-radius:5px;
    border-top-left-radius:5px;
    -webkit-border-top-right-radius:5px;
    -moz-border-top-right-radius:5px;
    border-top-right-radius:5px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.card.hovercard {
    width: 100%;
    z-index: 99;
    position: fixed;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
    background-color: #fff;
    background-color: rgba(255, 255, 255, 1);
}
.card.hovercard .card-background {
    height: 130px;
}
.card-background img {
    -webkit-filter: blur(25px);
    -moz-filter: blur(25px);
    -o-filter: blur(25px);
    -ms-filter: blur(25px);
    filter: blur(25px);
    margin-left: -100px;
    margin-top: -200px;
    min-width: 130%;
}
.card.hovercard .useravatar {
    position: fixed;
    top: 15px;
    left: 0;
    right: 0;
}
.card.hovercard .useravatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255, 255, 255, 0.5);
}
.card.hovercard .card-info {
    position: fixed;
    left: 0;
    right: 0;
}
.card.hovercard .card-info .card-title {
    padding:0 5px;
    font-size: 20px;
    line-height: 1;
    color: #262626;
    background-color: rgba(255, 255, 255, 0.1);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}
.card.hovercard .card-info {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}
.card.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}
.btn-pref .btn {
    -webkit-border-radius:0 !important;
}
.well{
    border:none;
    background-color: #ffffff;
    margin-top: 170px;
}
.column-album{
    
}
.img-thumbnail1 {
    padding: 4px;
    line-height: 1.42857143;
    background-color: #ffffff;
    border: 1px solid #dddddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;

    max-width: 100%;
}
.album-label{
    position: absolute;
    width: 100%;
    color: #f9f9f9;
    background: rgba(12, 12, 12, 0.29);
}
.btn-group-justified {
    position: fixed;
    top: 160px;    
    z-index: 99;
}
.album-option{
    position: absolute;
    bottom: 0;
    width: 100%;
    color: #f9f9f9;
}
.album-option .option{
    float: right;
    margin-right: 10px;
    background: rgba(51, 51, 51, 0.68);;
    font-size: 12px;
    height: 30px;
    line-height: 30px;
    padding: 0 15px;
    border: 1px solid #333;
    border-radius: 2px;
    display: none;
}
.album-option .hand{
    float: right;
    margin-right: 10px;
    background: rgba(8, 8, 8, 0.14);
}
.option-icon{
    margin-right: 3px;
}
.pager .active a{
    background: rgba(233, 84, 32, 0.37);
}
</style>
<div class="row">
    <div class="card hovercard">
        <div class="card-background">
            <?php if(!empty($user->wx_face)):?>
            <img class="card-bkimg" alt="<?=empty($user->wx_username)?$user->username:$user->wx_username?>" src="<?=$user->wx_face?>">
            <?php else:?>
            <img class="card-bkimg" alt="<?=empty($user->wx_username)?$user->username:$user->wx_username?>" src="/images/touxiang.png">
            <?php endif;?>
        </div>
        <div class="useravatar">
            <?php if(!empty($user->wx_face)):?>
            <img alt="" src="<?=$user->wx_face?>">
            <?php else:?>
            <img alt="" src="/images/touxiang.png">
            <?php endif;?>
        </div>
        <div class="card-info"> <span class="card-title"><?=empty($user->wx_username)?$user->username:$user->wx_username?></span>

        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-book" aria-hidden="true"></span>
                <div class="hidden-xs">相册</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <div class="hidden-xs">收藏</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <div class="hidden-xs">我</div>
            </button>
        </div>
    </div>

        <div class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
            <?php if(count($albums)>0):?>
                <?php foreach ($albums as $album):?>
                <a href="<?=Url::to(['/album/'.$album->id]);?>" class="album-a">
                    <div class="column-album img-thumbnail1" style=";height: 180px;padding:0;margin: 15px 0">   
                        <img alt="<?=$album->title?>" src="<?=$album->pic?>" class="img"/>   
                        <label class="album-label"><?=$album->title?></label>
                        <label class="album-option">
                            <span class="glyphicon glyphicon-hand-left hand"></span>
                            <div class="option">                                 
                                <span class="album-del" data-aid="<?=$album->id?>"><i class="glyphicon glyphicon-trash option-icon"></i>删除</span>  
                            </div>
                            
                        </label>
                    </div> 
                </a>    
                <?php endforeach;?>
                <?php else:?>              
               <h3> 空空如也。。。</h3>
            <?php endif;?>
            <?php if(isset($pages)):?>
                <div><?= LinkPager::widget(['pagination' => $pages,'options' => ['class' => 'pager'],'maxButtonCount' => 5]); ?></div>
            <?php endif;?> 
        </div>
        <div class="tab-pane fade in" id="tab2">
           <?php if(count($calbums)>0):?>
                <?php foreach ($calbums as $album):?>
                <?php $album = $album->items?>
                <a href="<?=Url::to(['/album/'.$album->id]);?>" class="album-a">
                    <div class="column-album img-thumbnail1" style=";height: 180px;padding:0;margin: 15px 0">   
                        <img alt="<?=$album->title?>" src="<?=$album->pic?>" class="img"/>   
                        <label class="album-label"><?=$album->title?></label>
                        <label class="album-option">
                            <span class="glyphicon glyphicon-hand-left hand"></span>
                            <div class="option">                                 
                                <span class="album-uncollect" data-aid="<?=$album->id?>"><i class="glyphicon glyphicon-star option-icon"></i>取消收藏</span>  
                            </div>
                            
                        </label>
                    </div> 
                </a>    
                <?php endforeach;?>
                <?php else:?>              
               <h3> 空空如也。。。</h3>
            <?php endif;?>         
        </div>
        <div class="tab-pane fade in" id="tab3">
          <h3>敬请期待。。。</h3>
        </div>
      </div>
    </div>
    
</div>
