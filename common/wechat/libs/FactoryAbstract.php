<?php
namespace common\wechat\libs;
/* 
 * 工厂抽象基类.
 */
abstract class FactoryAbstract{
    protected $_postObj;
    
    protected abstract function makeMetod();
    public function __construct($postObj) {
        $this->_postObj = $postObj;
    }

    public function  startFactory(){
        return $this->makeMetod();
    }
}
