<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * AlbumApp model
 *
 * @property integer $id
 * @property string $name 名称
 * @property string $sort 排序
 */
class AlbumMusicClassModel extends ActiveRecord
{
    const SORT = 0;//排序
    
    /**
     * @wangwei
     */
    public static function tableName()
    {
        return '{{%album_music_class}}';
    }
  
    /**
     * @wangwei
     * 默认值
     */
    public function rules()
    {
        return [
            ['sort', 'default', 'value' => self::SORT],
        ];
    }
     /**
     * @wangwei
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'sort' => "排序",
            'key' => "QQ接口关键字",
            'create_at' => "创建时间",
            'update_at' => "更新时间",
        ];
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     */
    public static function findOneById($id)
    {
        return static::findOne(['id' => $id]);
    }
    
    /**
     * Finds albumApp by name
     *
     * @param int $name 名称
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::findOne(['name' => $name]);
    }
     
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }  
 
}
