<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@tools', dirname(__DIR__). '/tools');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@editors', dirname(dirname(__DIR__)) . '/editors');
