<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class SlimscrollAsset extends AssetBundle
{
    public $sourcePath = '@bower/adminlte/plugins/slimScroll';
    public $js = [
        'jquery.slimscroll.min.js',
    ];
}
