<?php
$p = \Yii::$app->controller->module->templateAsset."/shuimodanqing/";
?>

<style type="text/css">

.showwords 
{
    font-size: 22px;
    color: black;
    letter-spacing: 5px;
    position: absolute;
    font-weight: bold;
    z-index: 1;
    top: 100%;
    margin: -90px;
    left: 90px;
    width: 100%;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-shadow: 3px 3px 10px white, -3px -3px 10px white, -3px 3px 10px white, 3px -3px 10px white;
    /*background-color: rgba(255, 255, 255, 0.4);*/
}

*
{
    padding: 0px;
    margin: 0px;
    -webkit-box-sizing: border-box;
}

body
{
    background-color: black;
}

#container
{
    width: 500px;
    height: 815px;
    position: absolute;
    overflow: hidden;
}
#container > div,#container >img
{
    position: absolute;
}
#gray
{
    top: 400px;
    left: 0px;
    -webkit-filter: grayscale(1);
}
#t1-1
{
    left: 251px;
    top: 669px;
    width: 250px;
}
#t1-2
{
    left: 252px;
    top: 672px;
    width: 250px;
    opacity: 0;
}
#t1-0
{
    opacity: 0;
}
#t1-3
{
    left: 103px;
    top: 215px;
    opacity: 0;
}
#line1
{
    left: 307px;
    top: 267px;
    opacity: 0;
}
#line2
{
    left: 250px;
    top: 267px;
    opacity: 0;
}
#line3
{
    left: 192px;
    top: 267px;
    opacity: 0;
}
.line
{
    color: #000;
    font-family: '΢���ź�';
    font-size: 30px;
    line-height: 30px;
    font-weight: bold; 
}
.yin
{
    left: 420px;
    top: 625px;
    width: 30px;
}
#he1
{
    -webkit-transform: scale(-1,1);
    width: 60px;
    left: 86px;
    top: 58px;
    /*-webkit-animation: he1 7s linear both;*/
}
#he2
{
    width: 40px;
    left: 116px;
    top: 48px;
    /*-webkit-animation: he1 7s linear both;*/
}
#pagediv0
{
    left: 0px;
    top: 0px;
    width: 500px;
    height: 815px;
}
#pagediv0 > div,#pagediv0 > img,#pagediv1 > div,#pagediv1 > img,#div1>img,#div1>div
{
    position: absolute;
}

#t2-0
{
    left: 0px;
    top: 0px;
    opacity: 0;
}
#t2-1
{
    left: 130px;
    top: 647px;
    opacity: 0;
}
#t2-2
{
    left: 213px;
    top: 595px;
    position: absolute;
    opacity: 0;
    /*-webkit-animation: birdin 1s linear both, birdmove 1s 2s linear infinite alternate;*/
}
#t2-3
{
    left: 207px;
    top: 576px;
    position: absolute;
    opacity: 0;
    /*-webkit-animation: birdin 1s linear both, birdmove 1s 3s linear infinite alternate;*/
}
#kuang1h
{
    left: 49px;
    top: 108px;
    -webkit-transform: rotate(90deg);
    display: none;
}
#kuang1v
{
    left: 52px;
    top: 101px;
    display: none;
}
#div1h-gray
{
    width: 460px;
    height: 328px;
    left: 19px;
    top: 207px;
    overflow: hidden;
    -webkit-filter: grayscale(100%);
    opacity: 0.8;
    display: none;
}   
#img1h-gray
{
    width: 460px;
    height: 328px;
}
#div1h
{
    width: 460px;
    height: 328px;
    left: 19px;
    top: 207px;
    overflow: hidden;
    display: none;
}  
#div1v
{
    width: 328px;
    height: 456px;
    left: 84px;
    top: 130px;
    overflow: hidden;
    display: none;
}
#div1v-gray
{
    width: 328px;
    height: 456px;
    left: 84px;
    top: 130px;
    overflow: hidden;
    -webkit-filter: grayscale(100%);
    display: none;
}
#img1v-gray
{
    width: 328px;
    height: 456px;
}
#div1
{
    opacity: 0;
}
.div1hitem,#img1h-1,#img1h-2,#img1h-3,#img1h-4
{
    width: 115px;
    position: absolute;
    height: 328px;
    overflow: hidden;
}
.div1vitem,#img1v-1,#img1v-2,#img1v-3,#img1v-4
{
    width: 82px;
    position: absolute;
    height: 456px;
    overflow: hidden;
}
.img 
{
    position: absolute;
}
#kuangimg1h-1
{
    position: absolute;
    left: 29px;
    top: -99px;
    -webkit-transform: rotate(90deg);
}
#kuangimg1h-2
{
    position: absolute;
    left: -86px;
    top: -99px;
    -webkit-transform: rotate(90deg);
}
#kuangimg1h-3
{
    position: absolute;
    left: -201px;
    top: -99px;
    -webkit-transform: rotate(90deg);
}
#kuangimg1h-4
{
    position: absolute;
    left: -316px;
    top: -99px;
    -webkit-transform: rotate(90deg);
}
#kuangimg1v-1
{
    left: -32px;
    top: -30px;
    position: absolute;
}
#kuangimg1v-2
{
    left: -114px;
    top: -30px;
    position: absolute;
}
#kuangimg1v-3
{
    left: -196px;
    top: -30px;
    position: absolute;
}
#kuangimg1v-4
{
    left: -278px;
    top: -30px;
    position: absolute;
}
#t3-0
{
    left: 350px;
    top: 476px;
    position: absolute;
    opacity: 0;
}



#kuang2v
{
    left: 25px;
    top: 102px;
    position: absolute;
    width: 450px;
}
#div2v-gray
{
    position: absolute;
    left: 73px;
    top: 130px;
    width: 379px;
    height: 501px;
}
#img2v-gray
{
    position: absolute;
    width: 379px;
    height: 501px;
    -webkit-filter: grayscale(100%);
    opacity: 0.8;
}
.div2vitem
{
    position: absolute;
    width: 379px;
    height: 501px;
/*    left: 73px;
    top: 130px;
    -webkit-transform: rotate(0deg);*/
}
#div2v-1
{
    position: absolute;
    -webkit-transform: rotate(-45deg);
    /*opacity: 0.5;*/
    width: 500px;
    height: 240px;
    /*background-color: #000;*/
    left: -116px;
    top: 267px;
    overflow: hidden;
    -webkit-transform-origin: 0% 0%;
}
#img2v-1
{
    position: absolute;
    left: -1px;
    top: 97px;
    -webkit-transform: rotate(45deg);
}
#div2v-2
{
    position: absolute;
    -webkit-transform: rotate(-45deg);
    /*opacity: 0.5;*/
    width: 715px;
    height: 250px;
    /*background-color: #000;*/
    left: -46px;
    top: 536px;
    overflow: hidden;
    -webkit-transform-origin: 0% 0%;
}
#img2v-2
{
    position: absolute;
    left: 138px;
    top: -142px;
    -webkit-transform: rotate(45deg);
}

#div2v-3
{
    position: absolute;
    -webkit-transform: rotate(-45deg);
    /*opacity: 0.5;*/
    width: 715px;
    height: 250px;
    /*background-color: #000;*/
    left: 125px;
    top: 718px;
    overflow: hidden;
    -webkit-transform-origin: 0% 0%;
}
#img2v-3
{
    position: absolute;
    left: 146px;
    top: -391px;
    -webkit-transform: rotate(45deg);
}

#kuangimg2v-1
{
    left: -47px;
    top: 60px;
    position: absolute;
    -webkit-transform: rotate(45deg);
    width: 450px;
}
#kuangimg2v-2
{
    left: 95px;
    top: -179px;
    position: absolute;
    -webkit-transform: rotate(45deg);
    width: 450px;
}
#kuangimg2v-3
{
    left: 102px;
    top: -429px;
    position: absolute;
    -webkit-transform: rotate(45deg);
    width: 450px;
}
#div2h-gray
{
    width: 436px;
    height: 335px;
    left: 30px;
    top: 213px;
    position: absolute;
    /*background-color: #000;*/
}
#kuang2h
{
    -webkit-transform: rotate(90deg);
    left: 56px;
    top: 127px;
    position: absolute;
    width: 390px;
}
#img2h-gray
{
    position: absolute;
    width: 436px;
    height: 335px;
    -webkit-filter: grayscale(100%);
    opacity: 0.8;
}
.div2hitem
{
    width: 436px;
    height: 335px;
}
#div2h-1
{
    position: absolute;
    left: -140px;
    top: 355px;
    -webkit-transform: rotate(-45deg);
    width: 441px;
    height: 201px;
    -webkit-transform-origin: 0% 0%;
    overflow: hidden;
    /*display: none;*/
}
#img2h-1
{
    -webkit-transform: rotate(45deg);
    left: 37px;
    top: 125px;
    position: absolute;
}
#div2h-2
{
    position: absolute;
    left: -34px;
    top: 533px;
    -webkit-transform: rotate(-45deg);
    width: 602px;
    height: 195px;
    -webkit-transform-origin: 0% 0%;
    overflow: hidden;
    /*display: none;*/
}
#img2h-2
{
    -webkit-transform: rotate(45deg);
    left: 88px;
    top: -76px;
    position: absolute;
}
#div2h-3
{
    position: absolute;
    left: 177px;
    top: 598px;
    -webkit-transform: rotate(-45deg);
    width: 602px;
    height: 195px;
    -webkit-transform-origin: 0% 0%;
    overflow: hidden;
    /*display: none;*/
}
#img2h-3
{
    -webkit-transform: rotate(45deg);
    left: -14px;
    top: -270px;
    position: absolute;
}
#pagediv2
{
    left: -10px;
}
#dahe1
{
    left: 512px;
    top: 60px;
    width: 170px;
    position: absolute;
}
#dahe2
{
    left: 611px;
    top: 82px;
    width: 170px;
    position: absolute
}
#pagediv3 > div, #pagediv3 > img
{
    position: absolute;
}
#t4-0
{
    left: 247px;
    top: 576px;
    opacity: 0;
}
#t4-1
{
    left: 0px;
    top: 0px;
    opacity: 0;
}
#t4-2
{
    left: 250px;
    top: 0px;
}
#t4-3
{
    left: 226px;
    top: 5px;
    opacity: 0;
}

#kuang3h
{
    position: absolute;
    left: 37px;
    top: 79px;
    -webkit-transform: rotate(90deg);
}
#img3h
{
    width: 465px;
    height: 355px;
    background-color: #9c9c9c;
    left: 13px;
    top: 173px;
    position: absolute;
}
#kuang3v
{
    position: absolute;
    left: 29px;
    top: 83px;
}
#img3v
{
    position: absolute;
    left: 72px;
    top: 114px;
    width: 360px;
    height: 468px;
}
#div3h,#div3v,#pagediv1,#pagediv2,#pagediv3
{
    display: none;
}
</style>

<style type="text/css">
@-webkit-keyframes fadeup
{
    0%  {-webkit-transform: translate(0px,0px);opacity: 0}
    20% {-webkit-transform: translate(0px,-4px);opacity: 1}
    100%{-webkit-transform: translate(0px,-20px);opacity: 1;}
}
@-webkit-keyframes moveright
{
    from  {-webkit-transform: translate(-10px,0px);}
    to    {-webkit-transform: translate(10px,0px);}
}
@-webkit-keyframes t4-2
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(0px,5px);}
}
@-webkit-keyframes fading
{
    0%  {opacity: 0}
    20% {opacity: 1}
    80% {opacity: 1}
    100%{opacity: 0}
}
@-webkit-keyframes t4-0
{
    0%   {opacity: 0;-webkit-transform: translate(0px,0px);}
    20%  {opacity: 1;-webkit-transform: translate(-60px,0px);}
    80%  {opacity: 1;-webkit-transform: translate(-240px,0px);}
    100% {opacity: 0;-webkit-transform: translate(-300px,0px);}
}
@-webkit-keyframes t4-3
{
    0%  {opacity: 0;-webkit-transform: translate(0px,0px);}
    20% {opacity: 1;-webkit-transform: translate(12px,0px);}
    80% {opacity: 1;-webkit-transform: translate(48px,0px);}
    100%{opacity: 0;-webkit-transform: translate(60px,0px);}
}
@-webkit-keyframes fadeinleft
{
    from  {opacity: 0;-webkit-transform: translate(-50px,0px);}
    to    {opacity: 1;-webkit-transform: translate(0px,0px);}
}
@-webkit-keyframes fadeoutleft
{
    from  {opacity: 1;-webkit-transform: translate(0px,0px);}
    to    {opacity: 0;-webkit-transform: translate(40px,0px);}
}
@-webkit-keyframes t1-2
{
    from  {-webkit-transform: rotate(-2deg);}
    to    {-webkit-transform: rotate(2deg);}
}
@-webkit-keyframes fadein
{
    from  {opacity: 0}
    to    {opacity: 1}
}
@-webkit-keyframes he11
{
    0%   {-webkit-transform: translate(0px,0px) scale(-1,1);opacity: 1;}
    50%  {-webkit-transform: translate(115px,-15px) scale(-0.8,0.8);opacity: 1}
    100% {-webkit-transform: translate(230px,-30px) scale(-0.6,0.6);opacity: 0}
}
@-webkit-keyframes fadeout
{
    from  {opacity: 1}
    to    {opacity: 0}
}
@-webkit-keyframes scaleout
{
    from  {-webkit-transform: scale(1);opacity: 1}
    to    {-webkit-transform: scale(1.2);opacity: 0}
}
@-webkit-keyframes birdin
{
    from  {-webkit-transform: scale(0.5);}
    to    {-webkit-transform: scale(1);}
}
@-webkit-keyframes birdmove
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(0px,5px);}
}
@-webkit-keyframes img1h_in_toleft
{
    from  {-webkit-transform: translate(120px,0px);}
    to    {-webkit-transform: translate(0px,0px);}
}
@-webkit-keyframes kuang1h_in_toleft
{
    from  {-webkit-transform: translate(120px,0px)  rotate(90deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(90deg);}
}
@-webkit-keyframes img1h_in_toright
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(120px,0px);}
}
@-webkit-keyframes kuang1h_in_toright
{
    from  {-webkit-transform: translate(0px,0px) rotate(90deg);}
    to    {-webkit-transform: translate(120px,0px) rotate(90deg);}
}
@-webkit-keyframes img1h_out_toright
{
    from  {-webkit-transform: translate(-120px,0px);}
    to    {-webkit-transform: translate(0px,0px);}
}
@-webkit-keyframes img1h_out_toleft
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(-120px,0px);}
}
@-webkit-keyframes img1v_in_toleft
{
    from  {-webkit-transform: translate(85px,0px);}
    to    {-webkit-transform: translate(0px,0px);}
}
@-webkit-keyframes img1v_in_toright
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(85px,0px);}
}
@-webkit-keyframes img1v_out_toright
{
    from  {-webkit-transform: translate(-85px,0px);}
    to    {-webkit-transform: translate(0px,0px);}
}
@-webkit-keyframes img1v_out_toleft
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(-85px,0px);}
}
@-webkit-keyframes t2-0-out
{
    from  {-webkit-transform: translate(0px,0px);opacity: 1}
    to    {-webkit-transform: translate(-50px,-50px);opacity: 0}
}
@-webkit-keyframes t2-2-out
{
    from {-webkit-transform: translate(0px,0px);}
    to   {-webkit-transform: translate(-26px,35px);}
}
@-webkit-keyframes t3-2-out
{
    from  {-webkit-transform: translate(-26px,35px);opacity: 1}
    to    {-webkit-transform: translate(-26px,135px);opacity: 0}
}
@-webkit-keyframes t2-3-out
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(-36px,46px);}
}
@-webkit-keyframes t3-3-out
{
    from  {-webkit-transform: translate(-36px,46px);opacity: 1}
    to    {-webkit-transform: translate(-36px,146px);opacity: 0}
}

@-webkit-keyframes t3-0-in
{
    from  {-webkit-transform: translate(0px,-30px);opacity: 0}
    to    {-webkit-transform: translate(0px,0px);opacity: 1}
}
@-webkit-keyframes t3-0-out
{
    from  {-webkit-transform: translate(0px,0px);opacity: 1}
    to    {-webkit-transform: translate(0px,100px);opacity: 0}
}
@-webkit-keyframes div2h-1-in
{
    from  { -webkit-transform: translate(-300px,300px) rotate(-45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(-45deg);}
}
@-webkit-keyframes img2h-1-in
{
    from  {-webkit-transform: translate(424px,0px) rotate(45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(45deg);}
}

@-webkit-keyframes div2h-2-in
{
    from  {-webkit-transform: translate(-500px,500px) rotate(-45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(-45deg);}
}
@-webkit-keyframes img2h-2-in
{
    from  {-webkit-transform: translate(708px,0px) rotate(45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(45deg);}
}
@-webkit-keyframes div2h-3-in
{
    from  {-webkit-transform: translate(300px,-300px) rotate(-45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(-45deg);}
}
@-webkit-keyframes img2h-3-in
{
    from  {-webkit-transform: translate(-424px,0px) rotate(45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(45deg);}
}

@-webkit-keyframes div2v-1-in
{
    from  { -webkit-transform: translate(-500px,500px) rotate(-45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(-45deg);}
}
@-webkit-keyframes img2v-1-in
{
    from  {-webkit-transform: translate(707px,0px) rotate(45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(45deg);}
}

@-webkit-keyframes div2v-2-in
{
    from  {-webkit-transform: translate(-800px,800px) rotate(-45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(-45deg);}
}
@-webkit-keyframes img2v-2-in
{
    from  {-webkit-transform: translate(1131px,0px) rotate(45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(45deg);}
}
@-webkit-keyframes div2v-3-in
{
    from  {-webkit-transform: translate(500px,-500px) rotate(-45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(-45deg);}
}
@-webkit-keyframes img2v-3-in
{
    from  {-webkit-transform: translate(-707px,0px) rotate(45deg);}
    to    {-webkit-transform: translate(0px,0px) rotate(45deg);}
}
@-webkit-keyframes he1
{
    from  {-webkit-transform: translate(0px,0px);}
    to    {-webkit-transform: translate(-800px,0px);}
}
</style>


<div id='container'>
    <img src='<?=$p?>images/bg.jpg'>

    <div id='pagediv0'>
        <img id='t1-0' src='<?=$p?>images/t01.png'>
        <img id='t1-2' src='<?=$p?>images/t012.png'>
        <img id='t1-1' src='<?=$p?>images/t011.png'>

        <img id='t1-3' src='<?=$p?>images/biaoti.png'>
        <div id='line1' class='line'></div>
        <div id='line2' class='line'></div>
        <div id='line3' class='line'></div>
    </div>

    <div id='pagediv1'>


        <div id='div1'>
            <div id='div1h-gray'>
                <div id='img1h-gray' class='img'></div>
            </div>
            <div id='div1h'>
                <div id='div1h-1' class='div1hitem' style='left:0px;'>
                    <div id='img1h-1' class='img'></div>
                    <div id='kuang1h-1' class='div1hitem' >
                        <img id='kuangimg1h-1' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <div id='div1h-2' class='div1hitem' style='left:115px;'>
                    <div id='img1h-2' class='img'></div>
                    <div id='kuang1h-2' class='div1hitem' >
                        <img id='kuangimg1h-2' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <div id='div1h-3' class='div1hitem' style='left:230px;'>
                    <div id='img1h-3' class='img'></div>
                    <div id='kuang1h-3' class='div1hitem' >
                        <img id='kuangimg1h-3' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <div id='div1h-4' class='div1hitem' style='left:345px;'>
                    <div id='img1h-4' class='img'></div>
                    <div id='kuang1h-4' class='div1hitem' >
                        <img id='kuangimg1h-4' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <span class='showwords' id='word1h'></span>
            </div>
            <img id='kuang1h' src='<?=$p?>images/kuang01.png'>


            <div id='div1v-gray'>
                <div id='img1v-gray' class='img'></div>
            </div>
            <div id='div1v'>
                <div id='div1v-1' class='div1vitem' style='left:0px;'>
                    <div id='img1v-1' class='img'></div>
                    <div id='kuang1v-1' class='div1vitem' >
                        <img id='kuangimg1v-1' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <div id='div1v-2' class='div1vitem' style='left:82px;'>
                    <div id='img1v-2' class='img'></div>
                    <div id='kuang1v-2' class='div1vitem' >
                        <img id='kuangimg1v-2' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <div id='div1v-3' class='div1vitem' style='left:164px;'>
                    <div id='img1v-3' class='img'></div>
                    <div id='kuang1v-3' class='div1vitem' >
                        <img id='kuangimg1v-3' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <div id='div1v-4' class='div1vitem' style='left:246px;'>
                    <div id='img1v-4' class='img'></div>
                    <div id='kuang1v-4' class='div1vitem' >
                        <img id='kuangimg1v-4' src='<?=$p?>images/kuang01.png'>
                    </div>
                </div>
                <span id='word1v' class='showwords'></span>
            </div>
            <img id='kuang1v' src='<?=$p?>images/kuang01.png'>

        </div>

        <img id='t2-0' src='<?=$p?>images/t02.png'>
        <img id='t2-1' src='<?=$p?>images/t023.png'>

    </div>

    <div id='pagediv2'>

        <div id='div2v'  class='img' >
            <div id='div2v-gray'>
                <div id='img2v-gray' class='img'></div>
            </div>
            <div id='div2v-1'>
                <div id='img2v-1' class='div2vitem'></div>
               
            </div>
            <div id='div2v-2'>
                <div id='img2v-2' class='div2vitem'></div>
                
            </div>
            <div id='div2v-3'>
                <div id='img2v-3' class='div2vitem'></div>
               
            </div>
            <span id='word2v' class='showwords'></span>
            <img id='kuang2v' src='<?=$p?>images/kuang02.png'>
        </div>

        <div id='div2h' class='img'>
            <div id='div2h-gray'>
                <div id='img2h-gray' class='img'></div>
            </div>
            <div id='div2h-1'>
                <div id='img2h-1' class='div2hitem'></div>
            </div>
            <div id='div2h-2'>
                <div id='img2h-2' class='div2hitem'></div>
            </div>
            <div id='div2h-3'>
                <div id='img2h-3' class='div2hitem'></div>
            </div>
            <span id='word2h' class='showwords'></span>
            <img id='kuang2h' src='<?=$p?>images/kuang02.png'>
        </div>


        <img id='dahe1' src='<?=$p?>images/p01.png'>
        <img id='dahe2' src='<?=$p?>images/p01.png'>

        <img id='t3-0' src='<?=$p?>images/t03.png'>

    </div>


    <div id='pagediv3'>
        <div id='div3h'>
            <div id='img3h'><span id='word3h' class='showwords'></span></div>
            <img id='kuang3h' src='<?=$p?>images/kuang02.png'>
        </div>

        <div id='div3v'>
            <div id='img3v'><span id='word3v' class='showwords'></span></div>
            <img id='kuang3v' src='<?=$p?>images/kuang02.png'>
        </div>
        <img id='t4-0' src='<?=$p?>images/t04.png'>
       
        <img id='t4-2' src='<?=$p?>images/t042.png'>
        <img id='t4-3' src='<?=$p?>images/t043.png'>

    </div>


    
    <img class='yin' src='<?=$p?>images/dsfhdf.png'>
    <img id='he1' src='<?=$p?>images/he.gif'>
    <img id='he2' src='<?=$p?>images/he.gif'>
    <img id='t2-2' src='<?=$p?>images/t021.png'>
    <img id='t2-3' src='<?=$p?>images/t022.png'>
</div>

<script>


function id(name)
{
    return document.getElementById(name);
}

var he_arr = ['<?=$p?>images/p1.png','<?=$p?>images/p2.png','<?=$p?>images/p3.png','<?=$p?>images/p4.png','<?=$p?>images/p5.png','<?=$p?>images/p6.png','<?=$p?>images/p7.png','<?=$p?>images/p8.png','<?=$p?>images/p9.png','<?=$p?>images/p10.png','<?=$p?>images/p11.png','<?=$p?>images/p12.png','<?=$p?>images/p13.png']

function showtitle()
{
    // show3()
    // return;
    id('t1-0').style.webkitAnimation = 'fadeinleft 1s ease-out both';
    id('t1-1').style.webkitAnimation = 'fadein 1s linear both';
    id('t1-2').style.webkitAnimation = 't1-2 2s linear infinite alternate,fadein 1s linear both';
    id('t1-3').style.webkitAnimation = 'fadein 1s linear both';

    if(desc.length>20)
    {
        id('line1').style.left = '307px';
        id('line2').style.left = '250px';
        id('line3').style.left = '192px';
    }
    else if(desc.length < 11)
    {
        id('line1').style.left = '257px';
        id('line2').innerHTML = '';
    }
    else
    {
        id('line1').style.left = '290px';
        id('line2').style.left = '220px';
        id('line2').innerHTML = '';
        id('line3').innerHTML = '';
    }

    var innertext = '';
    for(var i = 0; i<desc.length;i++)
    {
        if(i <10)
        {
            innertext = innertext + desc[i] + '<br>';
            if(i == 9 || i == desc.length - 1)
            {
                id('line1').innerHTML = innertext;
                innertext = '';
            }
        }
        else if(i<20)
        {
            innertext = innertext + desc[i] + '<br>';
            if(i == 19 || i == desc.length - 1)
            {
                id('line2').innerHTML = innertext;
                innertext = '';
            }
        }
        else
        {
            innertext = innertext + desc[i] + '<br>';
            if(i ==  desc.length - 1)
            {
                id('line3').innerHTML = innertext;
            }
        }
    }

    id('line1').style.webkitAnimation = 'fadeinleft 1s ease-out both';
    id('line2').style.webkitAnimation = 'fadeinleft 1s 0.25s ease-out both';
    id('line3').style.webkitAnimation = 'fadeinleft 1s 0.5s ease-out both';
   
    id('he1').style.webkitAnimation = 'he11 7s linear both';
    id('he2').style.webkitAnimation = 'he11 7s linear both';

    // setTimeout(distitle,3000)
}

function distitle()
{
    id('t1-3').style.webkitAnimation = 'fadeout 1s linear both';
    id('line1').style.webkitAnimation = 'fadeoutleft 1s linear both';
    id('line2').style.webkitAnimation = 'fadeoutleft 1s linear both';
    id('line3').style.webkitAnimation = 'fadeoutleft 1s linear both';

    id('t1-0').style.webkitAnimation = 'scaleout 1s linear both';
    id('t1-1').style.webkitAnimation = 'scaleout 1s 1s linear both';
    id('t1-2').style.webkitAnimation = 'scaleout 1s 1s linear both';

    timeout[1] = setTimeout(show1,1000)
}

function show1()
{
    id('pagediv1').style.display = 'block';
    id('pagediv2').style.display = 'none';
    id('pagediv3').style.display = 'none';
    id('t2-0').style.webkitAnimation = 'fadein 1s linear both';
    id('t2-1').style.webkitAnimation = 'fadein 1s linear both';
    id('t2-2').style.webkitAnimation = 'birdin 1s linear both, birdmove 1s 2s linear infinite alternate';
    id('t2-3').style.webkitAnimation = 'birdin 1s linear both, birdmove 1s 3s linear infinite alternate';

    id('div1').style.webkitAnimation = 'fadeup 8s 0.5s linear both';
    setImage('1');


    id('div1h-1').style.webkitAnimation = 'img1h_out_toright 1s 2s linear both';
    id('div1h-2').style.webkitAnimation = 'img1h_out_toright 1s 2s linear both';
    id('div1h-3').style.webkitAnimation = 'img1h_out_toright 1s 2s linear both';
    id('div1h-4').style.webkitAnimation = 'img1h_out_toright 1s 2s linear both';

    id('img1h-1').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';
    id('img1h-2').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';
    id('img1h-3').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';
    id('img1h-4').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';

    id('kuang1h-1').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';
    id('kuang1h-2').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';
    id('kuang1h-3').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';
    id('kuang1h-4').style.webkitAnimation = 'img1h_in_toleft 1s 2s linear both';


    id('kuang1h').style.webkitAnimation = 'fadeout 1s 2.5s linear both';

    id('div1v-1').style.webkitAnimation = 'img1v_out_toright 1s 2s linear both';
    id('div1v-2').style.webkitAnimation = 'img1v_out_toright 1s 2s linear both';
    id('div1v-3').style.webkitAnimation = 'img1v_out_toright 1s 2s linear both';
    id('div1v-4').style.webkitAnimation = 'img1v_out_toright 1s 2s linear both';

    id('img1v-1').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';
    id('img1v-2').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';
    id('img1v-3').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';
    id('img1v-4').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';

    id('kuang1v-1').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';
    id('kuang1v-2').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';
    id('kuang1v-3').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';
    id('kuang1v-4').style.webkitAnimation = 'img1v_in_toleft 1s 2s linear both';

    
    // id('div1').style.webkitAnimation = 'moveup 8s linear both';
    id('kuang1v').style.webkitAnimation = 'fadeout 1s 2.5s linear both';

    timeout[2] = setTimeout(clear1,6000)
}

function clear1()
{
    id('img1v-gray').style.webkitAnimation = 'fadeout 1s linear both';
    id('img1h-gray').style.webkitAnimation = 'fadeout 1s linear both';

    id('div1h-1').style.webkitAnimation = 'img1h_out_toleft 1s 1.5s linear both';
    id('div1h-2').style.webkitAnimation = 'img1h_out_toleft 1s 1.5s linear both';
    id('div1h-3').style.webkitAnimation = 'img1h_out_toleft 1s 1.5s linear both';
    id('div1h-4').style.webkitAnimation = 'img1h_out_toleft 1s 1.5s linear both';

    id('img1h-1').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';
    id('img1h-2').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';
    id('img1h-3').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';
    id('img1h-4').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';

    id('kuang1h-1').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';
    id('kuang1h-2').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';
    id('kuang1h-3').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';
    id('kuang1h-4').style.webkitAnimation = 'img1h_in_toright 1s 1.5s linear both';


    id('div1v-1').style.webkitAnimation = 'img1v_out_toleft 1s 1.5s linear both';
    id('div1v-2').style.webkitAnimation = 'img1v_out_toleft 1s 1.5s linear both';
    id('div1v-3').style.webkitAnimation = 'img1v_out_toleft 1s 1.5s linear both';
    id('div1v-4').style.webkitAnimation = 'img1v_out_toleft 1s 1.5s linear both';

    id('img1v-1').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';
    id('img1v-2').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';
    id('img1v-3').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';
    id('img1v-4').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';

    id('kuang1v-1').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';
    id('kuang1v-2').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';
    id('kuang1v-3').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';
    id('kuang1v-4').style.webkitAnimation = 'img1v_in_toright 1s 1.5s linear both';

    id('t2-2').style.webkitAnimation = 't2-2-out 1s 2s linear both';
    id('t2-3').style.webkitAnimation = 't2-3-out 1s 2s linear both';
    id('t2-1').style.webkitAnimation = 'fadeout 1s 2s linear both';
    id('t2-0').style.webkitAnimation = 't2-0-out 1s 2s linear both';

    timeout[3] = setTimeout(show2,3500)
}

function show2()
{
    
    id('pagediv2').style.display = 'block';
    id('pagediv0').style.display = 'none';
    id('pagediv1').style.display = 'none';
    id('pagediv3').style.display = 'none';
    flyhe();
    id('t3-0').style.webkitAnimation = 't3-0-in 1s linear both';
    setImage('2');

    id('div2h').style.webkitAnimation = 'fadein 1s linear both';
    id('div2h-1').style.webkitAnimation = 'div2h-1-in 1s 1s linear both';
    id('img2h-1').style.webkitAnimation = 'img2h-1-in 1s 1s linear both';

    id('div2h-2').style.webkitAnimation = 'div2h-2-in 1.5s 1.5s linear both';
    id('img2h-2').style.webkitAnimation = 'img2h-2-in 1.5s 1.5s linear both';

    id('div2h-3').style.webkitAnimation = 'div2h-3-in 1s 1s linear both';
    id('img2h-3').style.webkitAnimation = 'img2h-3-in 1s 1s linear both';

    id('div2v').style.webkitAnimation = 'fadein 1s linear both';
    id('div2v-1').style.webkitAnimation = 'div2h-1-in 1s 1s linear both';
    id('img2v-1').style.webkitAnimation = 'img2h-1-in 1s 1s linear both';

    id('div2v-2').style.webkitAnimation = 'div2h-2-in 1.5s 1.5s linear both';
    id('img2v-2').style.webkitAnimation = 'img2h-2-in 1.5s 1.5s linear both';

    id('div2v-3').style.webkitAnimation = 'div2h-3-in 1s 1s linear both';
    id('img2v-3').style.webkitAnimation = 'img2h-3-in 1s 1s linear both';

    
    timeout[4] = setTimeout(clear2,7000)
}

function clear2()
{
       
    id('t3-0').style.webkitAnimation = 't3-0-out 1s linear both';

    id('div2h-1').style.webkitAnimation = 'fadeout 1s linear both';
    id('div2h-2').style.webkitAnimation = 'fadeout 1s linear both';
    id('div2h-3').style.webkitAnimation = 'fadeout 1s linear both';
    id('div2v-1').style.webkitAnimation = 'fadeout 1s linear both';
    id('div2v-2').style.webkitAnimation = 'fadeout 1s linear both';
    id('div2v-3').style.webkitAnimation = 'fadeout 1s linear both';

    id('div2h').style.webkitAnimation = 'fadeout 1s 1s linear both';
    id('div2v').style.webkitAnimation = 'fadeout 1s 1s linear both';

    id('t2-3').style.webkitAnimation = 't3-3-out 1s linear both';
    id('t2-2').style.webkitAnimation = 't3-2-out 1s linear both';

    timeout[5] = setTimeout(show3,2000)
}

function show3()
{
    id('pagediv2').style.display = 'none';
    id('pagediv1').style.display = 'none';
    id('pagediv3').style.display = 'block';

    id('t4-0').style.webkitAnimation = 't4-0 9s linear both';
    id('t4-3').style.webkitAnimation = 't4-3 9s linear both';
    id('t4-2').style.webkitAnimation = 't4-2 2s linear infinite alternate, fading 9s linear both';
    setImage('3');

    id('div3h').style.webkitAnimation = 'moveright 9s linear both, fading 9s linear both';
    id('div3v').style.webkitAnimation = 'moveright 9s linear both, fading 9s linear both';

    id('img1v-gray').style.webkitAnimation = '';
    id('img1h-gray').style.webkitAnimation = '';

    timeout[6] = setTimeout(show1,10000);
}

function flyhe()
{
    dahe1 = id('dahe1');
    dahe2 = id('dahe2');

    dahe1.style.webkitAnimation = 'he1 10s linear both';
    dahe2.style.webkitAnimation = 'he1 10s linear both';

    flytimes = 50;
    heindex = 0;

    baihe = setInterval(flying,200)
}

function flying()
{
    dahe1.src = he_arr[heindex];
    dahe2.src = he_arr[(heindex+6)%he_arr.length];
    heindex ++ ;
    if(heindex == he_arr.length)
        heindex = 0;
    flytimes --;
    // console.log(flytimes)
    if(flytimes <= 0) 
    {
        clearInterval(baihe);
        return;
        
    }
}


var imgs_width = {'1h':460,'1v':328,'2v':379,'2h':436,'3h': 465,'3v': 360}
var imgs_height = {'1h':328,'1v':456,'2v': 501,'2h':335,'3h': 355, '3v': 468}

var get_pid = function(url){
    var index1 = url.indexOf("?");
    if(index1 != -1)
    {
        url = url.substr(0, index1);
    }
    return url.toString().substr(url.lastIndexOf("/") + 1);
}
function setImage(idname)
{
    if(reshow == true)
        return;

    while(Onload_imgs_url[image_url_index] == 'not find' || Onload_imgs_url[image_url_index] == 'loading')
    {
        if(image_url_index % step_loadnum == 0)
        {
            step_load();
        }
        image_url_index++;
        if(image_url_index == Onload_imgs_url.length)
            image_url_index = 0;
    }

    if(image_url_index % step_loadnum == 0)
    {
        step_load();
    }

    var img_bili = image_size_width[image_url_index]/image_size_height[image_url_index];
    var div;
    var div1;
    var divname;

    if(img_bili > 1)
    {
        width = imgs_width[idname+'h'];
        height = imgs_height[idname +'h'];

        if(idname == '1')
        {
            setimgposition('img1h-gray',width,height,img_bili,0);
            setimgposition('img1h-1',width,height,img_bili,0);
            setimgposition('img1h-2',width,height,img_bili,115);
            setimgposition('img1h-3',width,height,img_bili,230);
            setimgposition('img1h-4',width,height,img_bili,345);
            id('div1h').style.display = 'block';
            id('div1h-gray').style.display = 'block';
            id('kuang1h').style.display = 'block';
            id('div1v').style.display = 'none';
            id('div1v-gray').style.display = 'none';
            id('kuang1v').style.display = 'none';

            setTimeout(function(){
                var word = id('word1h');
                var word_string = words[get_pid(Onload_imgs_url[image_url_index])];
                if(word_string != undefined)
                {
                    word_string = word_string.replace(/[\n]/ig,'');
                    word.innerText = word_string;
                    word.style.webkitAnimation = 'fadein 1s linear both';

                    var word_length = word_string.length;
                    if(word_length < 6)
                        word.style.fontSize = "25px";
                    else
                        word.style.fontSize = "22px";
                }
                else
                {
                    word.innerText = "";
                }
                    
            }, 2900);

            setTimeout(function(){
                var word = id('word1h');
                word.style.webkitAnimation = 'fadeout 1s linear both';
            }, 6500);

        }
        else if(idname == '2')
        {
            setimgposition('img2h-gray',width,height,img_bili,0);
            setimgposition('img2h-1',width,height,img_bili,0);
            setimgposition('img2h-2',width,height,img_bili,0);
            setimgposition('img2h-3',width,height,img_bili,0);

            id('div2h').style.display = 'block';
            id('div2v').style.display = 'none';

            setTimeout(function(){
                var word = id('word2h');
                var word_string = words[get_pid(Onload_imgs_url[image_url_index])];
                if(word_string != undefined)
                {
                    word_string = word_string.replace(/[\n]/ig,'');
                    word.innerText = word_string;
                    word.style.webkitAnimation = 'fadein 1s linear both';
                    word.style.margin = "0px";
                    word.style.top = "450px";
                    word.style.width = "500px";
                    word.style.left = "0px";

                    var word_length = word_string.length;
                    if(word_length < 6)
                        word.style.fontSize = "25px";
                    else
                        word.style.fontSize = "22px";
                }
                else
                {
                    word.innerText = "";
                }
                    
            }, 2900);

            setTimeout(function(){
                var word = id('word2h');
                word.style.webkitAnimation = 'fadeout 1s linear both';
            }, 6500);

        }
        else
        {
            setimgposition('img3h',width,height,img_bili,0);

            var word = id('word3h');
            var word_string = words[get_pid(Onload_imgs_url[image_url_index])];
            if(word_string != undefined)
            {
                word_string = word_string.replace(/[\n]/ig,'');
                word.innerText = word_string;
                word.style.webkitAnimation = 'fadein 1s linear both';

                var word_length = word_string.length;
                if(word_length < 6)
                    word.style.fontSize = "25px";
                else
                    word.style.fontSize = "22px";
            }
            else
            {
                word.innerText = "";
            }

            id('div3h').style.display = 'block';
            id('div3v').style.display = 'none';
        }

    }
    else
    {
        width = imgs_width[idname +'v'];
        height = imgs_height[idname + 'v'];

        if(idname == '1')
        {
            setimgposition('img1v-gray',width,height,img_bili,0);
            setimgposition('img1v-1',width,height,img_bili,0)
            setimgposition('img1v-2',width,height,img_bili,82)
            setimgposition('img1v-3',width,height,img_bili,164)
            setimgposition('img1v-4',width,height,img_bili,246)
            id('div1h').style.display = 'none';
            id('div1h-gray').style.display = 'none';
            id('kuang1h').style.display = 'none';
            id('div1v').style.display = 'block';
            id('div1v-gray').style.display = 'block';
            id('kuang1v').style.display = 'block';

            setTimeout(function(){
                var word = id('word1v');
                var word_string = words[get_pid(Onload_imgs_url[image_url_index])];
                if(word_string != undefined)
                {
                    word_string = word_string.replace(/[\n]/ig,'');
                    word.innerText = word_string;
                    word.style.webkitAnimation = 'fadein 1s linear both';

                    var word_length = word_string.length;
                    if(word_length < 6)
                        word.style.fontSize = "25px";
                    else
                        word.style.fontSize = "22px";
                }
                else
                {
                    word.innerText = "";
                }
                    
            }, 2900);

            setTimeout(function(){
                var word = id('word1v');
                word.style.webkitAnimation = 'fadeout 1s linear both';
            }, 6500);
        }
        else if(idname == '2')
        {
            setimgposition('img2v-gray',width,height,img_bili,0);
            setimgposition('img2v-1',width,height,img_bili,0)
            setimgposition('img2v-2',width,height,img_bili,0)
            setimgposition('img2v-3',width,height,img_bili,0)

            id('div2v').style.display = 'block';
            id('div2h').style.display = 'none';

            setTimeout(function(){
                var word = id('word2v');
                var word_string = words[get_pid(Onload_imgs_url[image_url_index])];
                if(word_string != undefined)
                {
                    word_string = word_string.replace(/[\n]/ig,'');
                    word.innerText = word_string;
                    word.style.webkitAnimation = 'fadein 1s linear both';
                    // word.style.color = "red";
                    word.style.margin = "0px";
                    word.style.top = "450px";
                    word.style.width = "500px";
                    word.style.left = "0px";

                    var word_length = word_string.length;
                    if(word_length < 6)
                        word.style.fontSize = "25px";
                    else
                        word.style.fontSize = "22px";
                }
                else
                {
                    word.innerText = "";
                }
                    
            }, 2900);

            setTimeout(function(){
                var word = id('word2v');
                word.style.webkitAnimation = 'fadeout 1s linear both';
            }, 6500);

        }
        else
        {
            setimgposition('img3v',width,height,img_bili,0);

            var word = id('word3v');
            var word_string = words[get_pid(Onload_imgs_url[image_url_index])];
            if(word_string != undefined)
            {
                word_string = word_string.replace(/[\n]/ig,'');
                word.innerText = word_string;
                word.style.webkitAnimation = 'fadein 1s linear both';

                var word_length = word_string.length;
                if(word_length < 6)
                    word.style.fontSize = "25px";
                else
                    word.style.fontSize = "22px";
            }
            else
            {
                word.innerText = "";
            }

            id('div3v').style.display = 'block';
            id('div3h').style.display = 'none';
        }
    }

    image_url_index++;
    if(image_url_index == Onload_imgs_url.length)
    {
        image_url_index = 0;
    }


}

function setimgposition(imgid,width,height,img_bili,changeleft)
{
    var img = id(imgid);
    img.style.backgroundImage = 'url('+Onload_imgs_url[image_url_index]+')';
    if(img_bili > (width/height))
    {
        img.style.backgroundSize = height*img_bili + 'px ' + height + 'px';
        img.style.backgroundPosition = -((height*img_bili-width)/2) - changeleft +'px '+ '0px';
        // img.style.top = '0px';
        // img.style.height = height + 'px';
        // img.style.width = height*img_bili + 'px';
        // img.style.left = -((height*img_bili-width)/2) - changeleft +'px';
    }
    else
    {
        img.style.backgroundSize =  width + 'px ' + width/img_bili + 'px';
        img.style.backgroundPosition =- changeleft +'px ' + (-((width/img_bili-height)/2)) + 'px';

        // img.style.left = - changeleft +'px';
        // img.style.width = width+'px';
        // img.style.height = width/img_bili + 'px';
        // img.style.top = -((width/img_bili-height)/2) + 'px';
    }

}




call_me(load_images)

var image_size_width=[];
var image_size_height=[];
var image_url_index=0;
var have_num = 0;
var error_num = 0;
var canshow = true;
var reshow = false;
var timeout = [];


var delaytime=6000;

function load_images()
{
    reshow = false;
    image_size_width=[];
    image_size_height=[];
    Onload_imgs_url=[];
    image_url_index=0;
    have_num = 0;
    error_num = 0;
    begin_titletime = new Date();
    begin_titletime = begin_titletime.getTime();
    showtitle();
    canshow = true;

    bl_keepload = 'first';
    step_load();       
}

var bl_keepload = 'first';
var step_loadnum = 5;
function step_load()
{
    var load_num = 0
    //��������X��
    if(image_url_index  == 0 && bl_keepload == 'first')
    {
        console.log('loading continue');
        if(slider_images_url.length > step_loadnum)
        {
            load_num = step_loadnum;
            bl_keepload = 'next';
        }
        else
        {
            load_num = slider_images_url.length;
            bl_keepload = 'end';
        }
        for(var i = 0; i< load_num; i++)
        {
            var img=new Image();
            img.index=i;
            img.src=slider_images_url[i];
            img.onload=image_onload;
            img.onerror= image_onerror;
            Onload_imgs_url[i] = 'loading';
        }
    }
    else if(bl_keepload == 'end')
    {
        return;
    }
    else
    {
         console.log('loading continue');
        if(slider_images_url.length - image_url_index >step_loadnum*2)
        { 
            load_num = step_loadnum;
        }
        else
        {
            load_num = slider_images_url.length - image_url_index - step_loadnum;
            bl_keepload = 'end';
        }
        for(var i = image_url_index +step_loadnum; i< image_url_index + step_loadnum + load_num; i++)
        {
            var img=new Image();
            img.index=i;
            img.src=slider_images_url[i];
            img.onload=image_onload;
            img.onerror= image_onerror;
            Onload_imgs_url[i] = 'loading';
        }
    }
}
function image_onerror(event)
{
    var img = event.target;
    var index = img.index;
    if(index<step_loadnum)
        error_num ++;
    Onload_imgs_url[index] = 'not find';
    console.log(Onload_imgs_url[index]);
    if((have_num+error_num >= step_loadnum || slider_images_url.length == (have_num+error_num)) && canshow == true)
    {   
        reshow = false;
        canshow =false;
        if(have_num == 0)
            return;
        var end_titletime = new Date();
        end_titletime = end_titletime.getTime();
        var dis_titletime = Math.abs(end_titletime-begin_titletime);
        if(dis_titletime>delaytime)
        {
            distitle();
        }
        else
        {
            dis_titletime = delaytime- dis_titletime;
            timeout[0] = setTimeout(function()
                {
                    distitle();
                },dis_titletime);
        }
    }
}

function image_onload(event)
{
    if(reshow == true)
        return;

    var img = event.target;
    var index = img.index;

    if(index<step_loadnum)
    {
        have_num++;
    }
    Onload_imgs_url[index] = img.src;
    image_size_height[index] = img.height;
    image_size_width[index] = img.width;

    console.log(Onload_imgs_url[index]);

    if((have_num + error_num >= step_loadnum ||slider_images_url.length == (have_num+error_num)) && canshow == true)
    {   
        reshow = false;
        canshow =false;
        if(have_num == 0)
            return;
        var end_titletime = new Date();
        end_titletime = end_titletime.getTime();
        var dis_titletime = Math.abs(end_titletime-begin_titletime);
        if(dis_titletime>delaytime)
        {
            distitle();
        }
        else
        {
            dis_titletime = delaytime- dis_titletime;
            timeout[0] = setTimeout(function()
                {
                    distitle();
                },dis_titletime);
        }

    }
}

function reload_scene()
{
    clearnode();
    reshow = true;
    setTimeout(load_images,100);
}

function clearnode()
{

    for(var i = 0; i<timeout.length; i++)
    {
        clearTimeout(timeout[i])
    }

    id('pagediv0').style.display = 'block';
    id('pagediv1').style.display = 'none';
    id('pagediv2').style.display = 'none';
    id('pagediv3').style.display = 'none';

    id('img1v-gray').style.webkitAnimation = '';
    id('img1h-gray').style.webkitAnimation = '';

}
</script>