<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="main-div">
<?php $form = ActiveForm::begin([
    'options'=>[
        'class'=>'form-horizontal'
    ]
]); ?>
<?= $form->field($model, 'title', [
    'labelOptions' => ['class'=>'col-lg-2 control-label'],
    'template' => '
            {label}
            <div class="col-lg-5 ">
            {input}
            {error}
            </div>
            ',
])->textInput([
    'maxlength' => 100,
    'class' => 'form-control',
]) ?>
<?= $form->field($model, 'des', [
    'labelOptions' => ['class'=>'col-lg-2 control-label'],
    'template' => '
            {label}
            <div class="col-lg-5">
            {input}
            {error}
            </div>
            ',
])->textarea(['rows'=>5,'class' => 'form-control']) ?>
<div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
        <?php
        echo Html::submitButton($model->isNewRecord ? '增加' : '更新', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success'])
        ?>
    </div>
</div>  
<?php ActiveForm::end(); ?>
</div>