<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MusicAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web';
    public $css = [
   
    ];
    public $js = [
        'js/music.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\SwiperAsset',
        'common\assets\HandlebarsAsset',
    ];
}
