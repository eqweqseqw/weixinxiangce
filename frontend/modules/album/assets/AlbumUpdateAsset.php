<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AlbumUpdateAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web';
    public $css = [
        'css/update.css',
    ];
    public $js = [
        'js/update.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',       
//        'yii\bootstrap\BootstrapThemeAsset',
        'common\assets\WebUploadAsset',
        'common\assets\SwiperAsset',
    ];
}
