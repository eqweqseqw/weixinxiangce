<?php

namespace app\modules\album\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AlbumAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/album/assets/dist';
    public $baseUrl = '@web';
    public $css = [
        'css/buttons.css',
//        'css/guanzhu.css',
        'css/app.css',
    ];
    public $js = [
        'js/viewport.js',
//        'js/xmlHttp.js',
        'js/show.js',
    ];
    public $depends = [
         'yii\web\YiiAsset',
//        'common\assets\JqueryAsset',
//        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapThemeAsset',
    ];  
}
