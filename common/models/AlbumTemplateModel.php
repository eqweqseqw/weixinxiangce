<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * AlbumApp model
 *
 * @property integer $id
 * @property string $title 标题
 * @property string $pic 封面
 * @property string $music 音乐
 * @property integer $dir 模板目录
 * @property integer $class_id 分类
 * @property integer $is_good 推荐
 * @property integer active 是否有效
 * @property integer $created_at 创建时间
 * @property integer $updated_at 更新时间
 */
class AlbumTemplateModel extends ActiveRecord
{   
    const ACTIVE_OFF = 0;//删除
    const ACTIVE_ON = 1;//不删除


    /**
     * @wangwei
     */
    public static function tableName()
    {
        return '{{%album_template}}';
    }

    /**
     * @wangwei
     * 时间处理
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @wangwei
     * 默认值
     */
    public function rules()
    {
        return [
            ['active', 'default', 'value' => self::ACTIVE_ON],
            ['active', 'in', 'range' => [self::ACTIVE_ON, self::ACTIVE_OFF]],
        ];
    }
     /**
     * @wangwei
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => '用户',
            'title' => "标题",
            'pic' => "封面",
            'music' => "音乐",
            'dir' => "模板目录",
            'active' => "是否删除",
            'class_id' => "分类",
            'created_at' => "创建时间",
            'updated_at' => "更新时间",
        ];
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     * @param int $active 是否有效
     */
    public static function findOneById($id)
    {
        return static::findOne(['id' => $id]);
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     */
    public function getItems()
    {
        // 第一个参数为要关联的子表模型类名，
        // 第二个参数指定 通过子表的app_id，关联主表的id字段
        return $this->hasOne(AlbumTemplateClassModel::className(), ['id' => 'class_id']);
    }
    
    /**
     * Finds albumApp by dir
     *
     * @param int $dir 模板目录
     * @param int $active 是否有效
     * @return static|null
     */
    public static function findByDir($dir,$active=1)
    {
        return static::findOne(['dir' => $dir, ['>=','active',$active]]);
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }  
}
