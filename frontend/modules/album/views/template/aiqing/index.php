<?php
$p = \Yii::$app->controller->module->templateAsset."/aiqing/";
?>
<style type='text/css'>

    *
    {
        padding: 0px;
        margin: 0px;
        -webkit-box-sizing: border-box;
    }

    body
    {
        /*overflow: hidden;*/
    }
    div#content{
/*            position:relative;   */
/*            border:1px solid #000; */
            width:500px;        
            height:800px;
            margin:0 auto; 
            top:0px;    
            overflow:hidden;   
            clear:both;
        }
        #ppT9lwFAC-an-obj-1 {
            position:absolute;
            -webkit-transform: translate3d(0px, 0px, 0px);
            width: 500px;
            height: 815px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-2 {
            position:absolute;
            -webkit-transform: translate3d(-43px, 598px, 0px) scale3d(0.84, 0.84, 1);
            width: 226px;
            height: 253px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-3 {
            position:absolute;
            -webkit-transform: translate3d(304px, 428px, 0px) scale3d(0.87, 0.87, 1);
            width: 219px;
            height: 414px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-4 {
            position:absolute;
            -webkit-transform: translate3d(286px, -70px, 0px) scale3d(0.78, 0.78, 1);
            width: 281px;
            height: 445px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-5 {
            position:absolute;
            -webkit-transform: translate3d(200px, 351px, 0px);
            width: 300px;
            height: 130px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-6 {
            position:absolute;
            -webkit-transform: translate3d(-30px, 246px, 0px) scale3d(0.94, 0.94, 1);
            width: 187px;
            height: 442px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-7 {
            position:absolute;
            -webkit-transform: translate3d(245px, 178px, 0px) scale3d(0.11, 0.11, 1);
            width: 250px;
            height: 206px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-7-0 2.8304s 1.6895s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-7-0 {
            0% {
                -webkit-transform: translate3d(245px, 239px, 0px) scale3d(0.11, 0.11, 1);
                opacity: 0;
                -webkit-animation-timing-function:linear;
            }
            19.0051% {
                -webkit-transform: translate3d(245px, 178px, 0px) scale3d(0.11, 0.11, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(213px, -121px, 0px) scale3d(0.11, 0.11, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-8 {
            position:absolute;
            -webkit-transform: translate3d(215px, 264px, 0px) scale3d(0.15, 0.15, 1);
            width: 250px;
            height: 206px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-8-0 2.7004s 1.5054s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-8-0 {
            0% {
                -webkit-transform: translate3d(215px, 264px, 0px) scale3d(0.15, 0.15, 1);
                opacity: 0;
                -webkit-animation-timing-function:linear;
            }
            13.5027% {
                -webkit-transform: translate3d(213px, 190px, 0px) scale3d(0.15, 0.15, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
            52.0053% {
                -webkit-transform: translate3d(154px, 53px, 0px) scale3d(0.15, 0.15, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(192px, -132px, 0px) scale3d(0.15, 0.15, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-9 {
            position:absolute;
            -webkit-transform: translate3d(314px, 231px, 0px) scale3d(0.11, 0.11, 1);
            width: 250px;
            height: 206px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-9-0 2.8303s 1.8953s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-9-0 {
            0% {
                -webkit-transform: translate3d(314px, 292px, 0px) scale3d(0.11, 0.11, 1);
                opacity: 0;
                -webkit-animation-timing-function:linear;
            }
            19.0051% {
                -webkit-transform: translate3d(314px, 231px, 0px) scale3d(0.11, 0.11, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(308px, -117px, 0px) scale3d(0.11, 0.11, 1);
                opacity: 1;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-10 {
            position:absolute;
            -webkit-transform: translate3d(272px, 256px, 0px) scale3d(0.54, 0.54, 1);
            width: 250px;
            height: 206px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-10-0 4.7982s 0s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-10-0 {
            0% {
                -webkit-transform: translate3d(272px, 256px, 0px) scale3d(0.162, 0.162, 1);
                opacity: 0;
                -webkit-animation-timing-function:linear;
            }
            90% {
                -webkit-transform: translate3d(272px, 256px, 0px) scale3d(0.81, 0.81, 1);
                opacity: 0.7;
                -webkit-animation-timing-function:ease;
            }
            100% {
                -webkit-transform: translate3d(272px, 256px, 0px) scale3d(0.81, 0.81, 1);
                opacity: 0.7;
                -webkit-animation-timing-function:ease;
            }
        }

        #ppT9lwFAC-an-obj-11 {
            position:absolute;
            -webkit-transform: translate3d(166px, 370px, 0px);
            width: 20px;
            height: 20px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-12 {
            position:absolute;
            -webkit-transform: translate3d(241px, 11px, 0px) scale3d(0.8, 0.8, 1);
            width: 70px;
            height: 44px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-13 {
            position:absolute;
            -webkit-transform: translate3d(199px, -136px, 0px) scale3d(0.52, 0.52, 1);
            width: 150px;
            height: 213px;
            top:0; left:0;
        }

        #eye_1 {
            position:absolute;
            top:34px; left:25px;
            display: none;
            -webkit-transform:rotate(-12deg);
        }
        #eye_2 {
            position:absolute; 
            top:34px; left:25px;
        }

        #ppT9lwFAC-an-obj-14 {
            position:absolute;
            -webkit-transform: translate3d(212px, -76px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-19deg) scale3d(0.44, 0.44, 1);
            width: 57px;
            height: 130px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-14-0 2.4982s 0s infinite linear alternate both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-14-0 {
            0% {
                -webkit-transform: translate3d(200px, -47px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-19deg) scale3d(0.44, 0.44, 1);
                -webkit-transform-origin: 86% 19%;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(194px, -49px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(20deg) scale3d(0.44, 0.44, 1);
                -webkit-transform-origin: 86% 19%;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-15 {
            position:absolute;
            -webkit-transform: translate3d(276px, -96px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-8deg) scale3d(0.3, 0.3, 1);
            width: 57px;
            height: 190px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-15-0 2.4982s 0s infinite linear alternate both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-15-0 {
            0% {
                -webkit-transform: translate3d(288px, -65px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-8deg) scale3d(0.3, 0.3, 1);
                -webkit-transform-origin: 23% 22%;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(292px, -64px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-27deg) scale3d(0.3, 0.3, 1);
                -webkit-transform-origin: 23% 22%;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-16 {
            position:absolute;
            -webkit-transform: translate3d(157px, 11px, 0px) scale3d(0.8, 0.8, 1);
            width: 70px;
            height: 44px;
            top:0; left:0;
        }

        #ppT9lwFAC-an-obj-17 {
            position:absolute;
            -webkit-transform: translate3d(120px, -126px, 0px) scale3d(0.59, 0.59, 1);
            width: 150px;
            height: 194px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-17-0 2.4982s 0s infinite linear alternate both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-17-0 {
            0% {
                -webkit-transform: translate3d(120px, -126px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-10deg) scale3d(0.59, 0.59, 1);
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(134px, -126px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(6deg) scale3d(0.59, 0.59, 1);
                -webkit-animation-timing-function:linear;
            }
        }
        #eye_3 {
            position:absolute;
            top:40px; left:30px;
            display: none;
            
        }
        #eye_4 {
            position:absolute; 
            top:40px; left:30px;
            -webkit-transform:rotate(15deg);
        }

        #ppT9lwFAC-an-obj-18 {
            position:absolute;
            -webkit-transform: translate3d(127px, -75px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-19deg) scale3d(0.44, 0.44, 1);
            width: 57px;
            height: 130px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-18-0 2.4982s 0s infinite linear alternate both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-18-0 {
            0% {
                -webkit-transform: translate3d(115px, -47px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-19deg) scale3d(0.44, 0.44, 1);
                -webkit-transform-origin: 86% 19%;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(123px, -56px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(32deg) scale3d(0.44, 0.44, 1);
                -webkit-transform-origin: 86% 19%;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-19 {
            position:absolute;
            -webkit-transform: translate3d(188px, -92px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-6deg) scale3d(0.3, 0.3, 1);
            width: 57px;
            height: 190px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-19-0 2.4982s 0s infinite linear alternate both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-19-0 {
            0% {
                -webkit-transform: translate3d(204px, -59px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-6deg) scale3d(0.3, 0.3, 1);
                -webkit-transform-origin: 23% 22%;
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(216px, -52px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-3deg) scale3d(0.3, 0.3, 1);
                -webkit-transform-origin: 23% 22%;
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-20 {
            position:absolute;
            -webkit-transform: translate3d(193px, 349px, 0px) scale3d(0.16, 0.16, 1);
            width: 82px;
            height: 100px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-20-0 4.0361s 0s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-20-0 {
            0% {
                -webkit-transform: translate3d(193px, 349px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            24.9553% {
                -webkit-transform: translate3d(153px, 446px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(22deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            62.0751% {
                -webkit-transform: translate3d(213px, 574px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-24deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(213px, 771px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(49deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-21 {
            position:absolute;
            -webkit-transform: translate3d(193px, 349px, 0px) scale3d(0.16, 0.16, 1);
            width: 82px;
            height: 100px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-21-0 4.0361s 1.7599s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-21-0 {
            0% {
                -webkit-transform: translate3d(159px, 338px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            24.9553% {
                -webkit-transform: translate3d(144px, 454px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(22deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            62.0751% {
                -webkit-transform: translate3d(154px, 574px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-24deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(152px, 771px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(49deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-22 {
            position:absolute;
            -webkit-transform: translate3d(193px, 349px, 0px) scale3d(0.16, 0.16, 1);
            width: 82px;
            height: 100px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-22-0 4.0361s 0.76s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-22-0 {
            0% {
                -webkit-transform: translate3d(265px, 401px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-67deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            24.9553% {
                -webkit-transform: translate3d(287px, 504px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(22deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            62.0751% {
                -webkit-transform: translate3d(281px, 620px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-24deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(301px, 772px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(49deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
        }

        #ppT9lwFAC-an-obj-23 {
            position:absolute;
            -webkit-transform: translate3d(193px, 349px, 0px) scale3d(0.16, 0.16, 1);
            width: 82px;
            height: 100px;
            top:0; left:0;
            -webkit-animation: ani-ppT9lwFAC-an-obj-23-0 4.0361s 2.8177s infinite linear both ;
        }

        @-webkit-keyframes ani-ppT9lwFAC-an-obj-23-0 {
            0% {
                -webkit-transform: translate3d(222px, 403px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-67deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            27.8141% {
                -webkit-transform: translate3d(237px, 511px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(22deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            62.0751% {
                -webkit-transform: translate3d(231px, 630px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(-24deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
            100% {
                -webkit-transform: translate3d(251px, 776px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(49deg) scale3d(0.16, 0.16, 1);
                -webkit-animation-timing-function:linear;
            }
        }


        @-webkit-keyframes center_ani
        {
            0%  {-webkit-transform:translate(1020px,1020px) scale(0,0) rotate(0deg);}
            24% {-webkit-transform:translate(1020px,1020px) scale(1.1,1.1) rotate(360deg);}
            29% {-webkit-transform:translate(1020px,1020px) scale(1,1) rotate(360deg);}
            73% {-webkit-transform:translate(1020px,1020px) scale(1,1) rotate(360deg);}
            78% {-webkit-transform:translate(1020px,1020px) scale(1.1,1.1) rotate(360deg);}
            100%{-webkit-transform:translate(1020px,1020px) scale(0,0) rotate(0deg);}
        }

        @-webkit-keyframes roll_ani
        {
            0%  {-webkit-transform:translate(0px,1020px) rotate(-700deg);opacity: 0}
            30%  {-webkit-transform:translate(1020px,1020px) rotate(360deg);opacity: 1}
/*            27%  {-webkit-transform:translate(0px,1020px) rotate(0deg);}
            73%  {-webkit-transform:translate(0px,1020px) rotate(0deg);}*/
            70%  {-webkit-transform:translate(1020px,1020px) rotate(360deg);opacity: 1}
            100%  {-webkit-transform:translate(2040px,1020px) rotate(1060deg);opacity: 1}

        }
</style>


<body>

           <div id='content' style='position:absolute;width:500px;height:815px;overflow:hidden'>
        <div id="ppT9lwFAC-an-obj-1"><img  height="815" width="500"  src="<?=$p?>images/bg.jpg"></div>
       
        <div id="ppT9lwFAC-an-obj-5"><img  height="130" width="300"  src="<?=$p?>images/10.png"></div>
       
        <div id="ppT9lwFAC-an-obj-7"><img  height="206" width="250"  src="<?=$p?>images/9.png"></div>
        <div id="ppT9lwFAC-an-obj-8"><img  height="206" width="250"  src="<?=$p?>images/9.png"></div>
        <div id="ppT9lwFAC-an-obj-9"><img  height="206" width="250"  src="<?=$p?>images/9.png"></div>
        <div id="ppT9lwFAC-an-obj-10"><img  height="206" width="250"  src="<?=$p?>images/9.png"></div>
        <div id="ppT9lwFAC-an-obj-11">
            <div id="ppT9lwFAC-an-obj-12"><img  height="44" width="70"  src="<?=$p?>images/7.png"></div>
            <div id="ppT9lwFAC-an-obj-13">
                <img  height="213" width="150"  src="<?=$p?>images/8.png">
                <img id='eye_1'width='100' src="<?=$p?>images/5.png">
                <img id='eye_2' width='100' src="<?=$p?>images/4.png">
            </div>
            <div id="ppT9lwFAC-an-obj-14"><img  height="130" width="57"  src="<?=$p?>images/3.png"></div>
            <div id="ppT9lwFAC-an-obj-15"><img  height="190" width="57"  src="<?=$p?>images/2.png"></div>
            <div id="ppT9lwFAC-an-obj-16"><img  height="44" width="70"  src="<?=$p?>images/7.png"></div>
            <div id="ppT9lwFAC-an-obj-17">
                <img  height="194" width="150"  src="<?=$p?>images/6.png">
                <img id='eye_3'width='93' src="<?=$p?>images/5.png">
                <img id='eye_4' width='93' src="<?=$p?>images/4.png">
            </div>
            <div id="ppT9lwFAC-an-obj-18"><img  height="130" width="57"  src="<?=$p?>images/3.png"></div>
            <div id="ppT9lwFAC-an-obj-19"><img  height="190" width="57"  src="<?=$p?>images/2.png"></div>
        </div>
        <div id="ppT9lwFAC-an-obj-20"><img  height="100" width="82"  src="<?=$p?>images/1.png"></div>
        <div id="ppT9lwFAC-an-obj-21"><img  height="100" width="82"  src="<?=$p?>images/1.png"></div>
        <div id="ppT9lwFAC-an-obj-22"><img  height="100" width="82"  src="<?=$p?>images/1.png"></div>
        <div id="ppT9lwFAC-an-obj-23"><img  height="100" width="82"  src="<?=$p?>images/1.png"></div>
   </div>

</body>


<script>

started = false;
show_images = [];
bubbles = [];
current_image = 0;

one   = [{x:250, y:250, r:230}];
two   = [{x:177, y:221, r:320}, {x:380, y:600, r:218}]
three = [{x:112, y:80,  r:230}, {x:190, y:593, r:248}, {x:390, y:338, r:180}]
four  = [{x:137, y:104, r:173}, {x:289, y:427, r:280}, {x:403, y:124, r:135},
         {x:397, y:689, r:103}]

paths = [one, two, three, four];


function calc_image(img)
{
    var w = img.width;
    var h = img.height;

    h = h * (500 / w);
    w = 500;

    if(h < 500)
    {
        w = w * (500 / h);
        h = 500;
    }

    img.w = w;
    img.h = h;

    img.l = Math.floor((500 - w) / 2);
    img.t = Math.floor((500 - h) / 2);
}


function insert_css(code)
{
    var style  = document.createElement("style");
    style.type = "text/css";
    style.innerHTML = code;
    document.head.appendChild(style);
}


function make_all_path_css()
{
    for(var i=0; i<paths.length; i++)
    {
        var objs = paths[i];

        for(var k=0; k<objs.length; k++)
        {
            one = objs[k];

            make_path_css(-500, -500, one.x, one.y, one.r);
            make_path_css(1000, -500, one.x, one.y, one.r);
            make_path_css(-500, 1350, one.x, one.y, one.r);
            make_path_css(1000, 1350, one.x, one.y, one.r);            
        }
    }
}


function rand(min, max)
{
    return Math.floor(Math.random() * (max - min));
}

function rand_list()
{
    var nums = ['0', '0.1', '0.2', '0.3', '0.4'];

    for(var i=0; i<5; i++)
    {
        var i0 = rand(0, 5);
        var i1 = rand(0, 5);

        var tmp = nums[i0];
        nums[i0] = nums[i1];
        nums[i1] = tmp;
    }

    return nums;
}


function make_path_css(sx, sy, dx, dy, r)
{
    var name = "ani_" + sx + "_" + sy + "_" + dx + "_" + dy + "_" + r;
    name = name.replace(/\-/g, "n");

    sx += 1000;
    sy += 1000;
    dx += 1000;
    dy += 1000;

    var mx = dx + 40;
    var my = dy + 40;
    var mx2 = dx - 40;
    var my2 = dy - 40;

    if(dx < sx)
    {
        mx = dx - 40;
        mx2 = dx + 40;
    }

    if(dy < sy)
    {
        my = dy - 40;
        my2 = dy + 40;
    }
    
    var dx2, dy2;

    if(sx - 1000< 0)
    {
        dx2 = 1000 + 1000;
    }
    else
    {
        dx2 = -500 + 1000;
    }

    if(sy - 1000< 0)
    {
        dy2 = 1350 + 1000;
    }
    else
    {
        dy2 = -500 + 1000;
    }

    var css = "";
    

    css += "@-webkit-keyframes " + name + "{\n";
    css += "0%{-webkit-transform: translate(" + (sx-r)  + "px, " + (sy-r) + "px) rotate(0deg)}\n";
    css += "24%{-webkit-transform: translate(" + (mx-r) + "px, " + (my-r) + "px) rotate(360deg)}\n";
    css += "29%{-webkit-transform: translate(" + (dx-r) + "px, " + (dy-r) + "px) rotate(360deg)}\n";
    css += "73%{-webkit-transform: translate(" + (dx-r) + "px, " + (dy-r) + "px) rotate(360deg)}\n";
    css += "78%{-webkit-transform: translate(" + (mx2-r) + "px, " + (my2-r) + "px) rotate(360deg)}\n";
    css += "100%{-webkit-transform: translate(" + (dx2-r) + "px, " + (dy2-r) + "px) rotate(720deg)}\n";
    css += "}\n";

    insert_css(css);
    console.log(css);
}


function crate_bubbles()
{
    for(var i=0; i<5; i++)
    {
        var one = document.createElement('div');
        bubbles.push(one);
        
        one.style.position = 'absolute';
        one.style.borderRadius = '99em';
        one.style.opacity = 0.98;

        one.style.left = '-1000px';
        one.style.top  = '-1000px';
        one.style.width = '100px';
        one.style.height = '100px';
        one.style.visibility = 'visible';
        one.style.backgroundColor = 'white';

        var circle = new Image();
        
        one.appendChild(circle);
        
        var src_rand=rand(0,1)
        if(src_rand==0)
            circle.src = "<?=$p?>/images/11.png";
        else
            circle.src = "<?=$p?>/images/12.png";
        circle.width = "100";
        circle.height = "100";
        circle.style.visibility = 'visible';
        
        //document.body.appendChild(circle);

        one.circle = circle;

        id('content').appendChild(one);
    }
}


function get_ok_image()
{
    for(var i=0; i<show_images.length; i++)
    {
        var sel = (current_image + i) % show_images.length;

        if(show_images[sel].ok)
        {
            current_image = (sel + 1) % show_images.length;
            return show_images[sel];
        }
    }

    return false;
}


function apply_image(bubble, w, h)
{
    var img = get_ok_image();
    var iw  = img.width;
    var ih  = img.height;

    ih = ih * w / iw;
    iw = w;

    if(ih < h)
    {
        iw = iw * h / ih;
        ih = h;
    }

    iw = Math.floor(iw);
    ih = Math.floor(ih);

    var x = Math.floor((w - iw) / 2);
    var y = Math.floor((h - ih) / 2);

    bubble.style.backgroundImage = "url(" + img.src + ")";
    bubble.style.backgroundSize  = iw + "px " + ih + "px";
    bubble.style.backgroundPosition = x + "px " + y + "px";
}

var style_1=0; 

function begin_show()
{
    var i = rand(0, paths.length);
    var objs = paths[i];

    var m = rand(0, 4);
    var nums = rand_list();

    for(var k=0; k<objs.length; k++)
    {
        // if(objs.length==1)
            // alert();

        o = objs[k];
        b = bubbles[k];

        b.style.width  = o.r * 2 + "px";
        b.style.height = o.r * 2 + "px";

        apply_image(b, o.r*2, o.r*2);

        b.circle.width = o.r * 2;
        b.circle.height = o.r * 2;
        b.circle.style.position = "absolute";
        b.circle.style.left = "0px";
        b.circle.style.top  = "0px";

        var sx, sy;

        if(m == 0)
        {
            sx = -500;
            sy = -500;
        }
        else if(m == 1)
        {
            sx = 1000;
            sy = -500;
        }
        else if(m == 2)
        {
            sx = -500;
            sy = 1350;
        }
        else if(m == 3)
        {
            sx = 1000;
            sy = 1350;
        }

        var name = "ani_" + sx + "_" + sy + "_" + o.x + "_" + o.y + "_" + o.r;
        name = name.replace(/\-/g, "n");

        console.log("ani select " + name);

        //var wt = nums[k];
        wt = 0.7 * k;

        if(objs.length==1)
        {
            // b.style.webkitAnimation='center_ani 3.5s ease-in-out both';
            if(style_1==0)
            {
                b.style.webkitAnimation='center_ani 3.5s ease-in-out both';
                style_1=1;
            }   
            else
            {
                b.style.webkitAnimation='roll_ani 3.5s ease-in-out both';
                style_1=0;
            } 

        }
        else
            b.style.webkitAnimation = name + " 3.5s ease-in-out " + wt + "s both";

    }
    var ts = (objs.length -1) * 700 + 4000;
    setTimeout("clean_ani()", ts);
}


function clean_ani()
{
    for(var i=0; i<bubbles.length; i++)
    {
        bu = bubbles[i];
        bu.style.webkitAnimation = "";
    }

    setTimeout("begin_show()", 10);
}


function on_image_load(event)
{
    var img = event.target;
    img.ok = true;
    calc_image(img);

    if(!started && show_images[0].ok &&show_images[1].ok &&show_images[2].ok &&show_images[3].ok)
    {
        started = true;
        begin_show();
    }
}


function set_background()
{
    // document.body.style.backgroundImage = "url(http://7xjeju.com2.z0.glb.qiniucdn.com/pic/love/lovebg.jpg)";
    document.body.style.backgroundSize = "500px " + document.body.scrollHeight + "px";
}

function id(name)
{
    return document.getElementById(name)
}
function scene_init()
{
    eyeOpen();
    set_background();
    crate_bubbles();
    make_all_path_css();

    if(slider_images_url.length == 1)
    {
        slider_images_url[1] = slider_images_url[0];
    }

    if(slider_images_url.length == 2)
    {
        slider_images_url[2] = slider_images_url[0];
    }

    if(slider_images_url.length == 3)
    {
        slider_images_url[3] = slider_images_url[0];
    }
    console.log(slider_images_url.length)
    for(var i=0; i<slider_images_url.length; i++)
    {
        var img = new Image();
        show_images.push(img);
        img.i = i;
        img.ok = false;
        img.onload = on_image_load;
        img.src = slider_images_url[i];
    }
}

function eyeOpen()
        {
            var p = setInterval(function()
            {
                id('eye_2').style.display = 'none';
                id('eye_1').style.display = 'block';
                id('eye_4').style.display = 'none';
                id('eye_3').style.display = 'block';
            setTimeout(function()
            {
                id('eye_1').style.display = 'none';
                id('eye_2').style.display = 'block';
                id('eye_3').style.display = 'none';
                id('eye_4').style.display = 'block';
            },3000);
            },5000);
            
        }

call_me(scene_init);


</script>
