$(function(){
    if($.fn.imagefit!=undefined){
        $('.column-album').imagefit({
            mode:'outside',
            halign:'center',
            valign:'middle'
        });
    }
   
    $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
        // $(".tab").addClass("active"); // instead of this do the below 
        $(this).removeClass("btn-default").addClass("btn-primary");   
    });
    
    $(".hand").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault();   
        $(this).next().toggle();
    })
    $(".album-del").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "app/delete",//路径  
            data : {  
                aid : _this.data("aid") 
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                   _this.parents(".album-a").remove();
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });      
    })
    $(".album-zan").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "zan",//路径  
            data : {  
                aid : _this.data("aid"),
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                  alert( result.message );
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });      
    })
    $(".album-collect").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "collect",//路径  
            data : {  
                aid : _this.data("aid") 
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                  alert( result.message );
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });      
    })
    $(".album-uncollect").bind("click",function(event){
        event.stopPropagation();
        event.preventDefault(); 
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "app/collect",//路径  
            data : {  
                aid : _this.data("aid") 
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                   _this.parents(".album-a").remove();
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });      
    })
})


