$(function(){
    var swiper = new Swiper('.swiper-container',{
            slidesPerView: 3,
            loop:false
        });
    $(".swiper-slide").bind("click",function(){
        var _this = $(this);
        _this.addClass("active").siblings().removeClass("active");
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "templateajax",//路径  
            data : {  
                classid : _this.data("cid") 
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                    var myTemplate = Handlebars.compile($("#temp-template").html());
                    $('#template-data').html(myTemplate(result));   
                    $(".template-a").bind("click",templateClick);
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });          
    });
    
    var templateClick = function(){
        var _this = $(this);
        window.location.href="/album/template/preview?aid="+$("#aid").val()+"&template="+ _this.data("id");        
    }
    $(".template-a").bind("click",templateClick);
})

