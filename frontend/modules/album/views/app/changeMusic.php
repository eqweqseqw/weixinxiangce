<?php
use app\modules\album\assets\MusicAsset;

MusicAsset::register($this);
?>
<style>
    .img-thumbnail{
        max-height: 170px
    }
    .nav{
        z-index: 99;
        background: white;
        margin-left: -15px;
        margin-right: -15px;
        position: fixed;
        width: 100%;
        border-bottom: 1px solid rgb(191, 102, 9);
    }
    .margin-top {
        margin-top: 40px;
    }
    .swiper-container{
        height: 32px;
        line-height: 30px;
    }
    .swiper-slide{
        line-height: 32px;
        width: 50px !important;
        font-size: 20px;
        text-align: center;
        word-break: break-all;
        white-space: nowrap;
        overflow: hidden;
    }  
    .active{
        color: #c34113;
        border-bottom: 3px solid #c34113;
    }
    .music-name{
        color: #333;
        font-size: 25px;
        width: 100%;
        display: block;
    }  
    .form-horizontal .margin-r0 {
         margin-right:0 !important;
    }
    .container{
        padding-bottom: 40px;
    }   
</style>
<div class="nav">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide" data-key="0"><i class="glyphicon glyphicon-search search"></i></div>
            <?php foreach ($class as $val):?>
            <div class="swiper-slide" data-key="<?=$val->key?>"><?=$val->name?></div>
            <?php endforeach;?>        
        </div>
    </div>
</div>
<input id="aid" type="hidden" value="<?=$aid?>">
<div class="margin-top"></div>
<form class="form-horizontal" role="form" id="searchfrom" onsubmit="">
<div class="form-group margin-r0">
  <div class="col-xs-9">
  <input  class="form-control" id="keyword" placeholder="请输入关键字">
  </div>
    <button  id="search" class="btn btn-primary col-xs-3">搜索</button>
</div>
</form>
<ul class="list-group" id="music-data">
 
</ul>
<script id="music-template" type="text/x-handlebars-template">
    {{#each list}}              
        <li data-music="{{m4a}}" class="list-group-item music-li">{{{songname}}}<span class="music-icon"></span></li>               
    {{/each}}
</script>