<?php
namespace common\wechat\libs;

use common\wechat\libs\FactoryAbstract;
use common\wechat\ReplySubscribeEvent;
use common\wechat\ReplyUnsubscribeEvent;
use common\wechat\ReplyClickEvent;
use common\wechat\replytext;
use common\wechat\clicks\ReplyLoginService;
/* 
 * 处理event类消息.工厂
 */

class EventFactory extends FactoryAbstract{
    
    /*按事件的类型封装不通的类，目前想到的分别有subscribe/unsubscribe; LOCATION; CLICK(菜单点击)等3个类*/
    protected function makeMetod() {
        
        $content = "";
        
    	switch ($this->_postObj->Event)
    	{
    	   case "subscribe":   //关注事件
                $worker = new ReplySubscribeEvent($this->_postObj);             
                break;
           case "unsubscribe": //取消关注事件
                $worker = new ReplyUnsubscribeEvent($this->_postObj);             
                break;          
            /*菜单点击*/
    		case "CLICK":
                $worker = new ReplyClickEvent($this->_postObj);
                break;
            
            /*默认对应一个类*/
            default:
                $worker = new replytext($this->_postObj);         
                break;          
 	    }
        
        return $worker;
    }
    
}