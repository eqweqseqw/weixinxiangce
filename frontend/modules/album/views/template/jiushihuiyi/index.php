<?php
$p = \Yii::$app->controller->module->templateAsset."/jiushihuiyi/";
?>

<style type="text/css">
	
    *
    {
        padding: 0px;
        margin: 0px;
        -webkit-box-sizing: border-box;
    }

    html,body,.main-body
    {
        background-color: black;
        height: 100%;
    }

    #container
    {
        width: 500px;
        height: 100%;
        position: absolute;
        overflow: hidden;
    }
    #container > div,#container >img
    {
        position: absolute;
    }

    #topdiv
    {
        width:380px;
        height:110px;
        z-index: 2;
        position:absolute;
        left:60px;
        top:0px;
    }
    #maodiv
    {
        width:74px;
        height:50px;
        z-index: 1;
        position:absolute;
        left:190px;
        top:22px;
        -webkit-animation: yumao 2s linear 0s infinite alternate; 
    }

    #sjdiv
    {
        width:44px;
        height:64px;
        z-index: 1;
        position:absolute;
        left:150px;
        top:35px;
        -webkit-animation:shijian 2s linear 0s infinite alternate;
    }

    #liushengjidiv
    {
        width:135px;
        height:183px;
        z-index: 1;
        position:absolute;
        left:10px;
        bottom:80px;
        -webkit-animation:liushengji 3s linear 0s infinite alternate;
    }
    #musicdiv
    {
        width:237px;
        height:176px;
        z-index: 2;
        position:absolute;
        left:90px;
        bottom:190px;
    }
    #yinfudiv
    {
        width:15px;
        height:40px;
        z-index: 3;
        position:absolute;
/*        left:10px;
        bottom:100px;*/
        left:90px;bottom:200px;
        -webkit-animation: yinfu 3s infinite linear;
    }
    #yinfu01div
    {
        width:22px;
        height:40px;
        z-index: 3;
        position:absolute;
/*        left:10px;
        bottom:100px;*/
        left:90px;bottom:200px;
        -webkit-animation: yinfu01 3s infinite linear;
    }
    #yinfu02div
    {
        width:20px;
        height:36px;
        z-index: 3;
        position:absolute;
/*        left:10px;
        bottom:100px;
*/       
        left:90px;bottom:200px;
         -webkit-animation: yinfu02 5s infinite linear;
    }

    #yinfu03div
    {
        width:20px;
        height:36px;
        z-index: 3;
        position:absolute;
/*        left:10px;
        bottom:100px;*/
        left:90px;bottom:200px;
        -webkit-animation: yinfu03 6s infinite linear;
    }

    #butterfly01div
    {
        width: 30px;
        height: 30px;
        position: absolute;
        right: 10px;
        bottom: 120px;
        z-index: 3;
        -webkit-transform:scale(0.5,0.5) rotate(-45deg) perspective(600px) rotateY(60deg);
    }
    #butterfly02div
    {
        width: 30px;
        height: 30px;
        position: absolute;
        z-index: 3;
        right: 110px;bottom: 140px;
        -webkit-animation:butterfly02 8s infinite linear;
    }

    #butterfly03div
    {
        width: 30px;
        height: 30px;
        position: absolute;
        z-index: 3;
        right: 160px;bottom: 105px;
        -webkit-animation:butterfly03 4s infinite linear;
    }

    #butterfly04div
    {
        width: 30px;
        height: 30px;
        position: absolute;
        left: 500px;top: 650px;
        z-index: 3;
        -webkit-animation:butterfly04 8s infinite linear;
    }

    #butterfly05div
    {
        width: 30px;
        height: 30px;
        position: absolute;
        right: 130px;
        bottom: 120px;
        z-index: 3;
        -webkit-transform:scale(0.5,0.5) rotate(45deg) perspective(600px) rotateY(60deg);
    }

    .divv
    {
        width: 373px;
        height: 515px; 
        left:68px; 
        top:120px;
        /*border: 36px;
        border-image:url(kuang01.png) 36 55 55 36 round;*/
        -webkit-transform: translate(600px,0px);
    }
    /*.imgconv
    {
        
        width: 300px;
        height: 440px;
        left:1px; 
        top:1px;
        background-color: #fff;
        position: absolute;
    }*/
    .conv
    {
        position: absolute;
        width: 304px;
        height: 450px;
        left:43px; 
        top:40px;
        background-color: #666;
        -webkit-animation: divv_in_left 6s linear both;
        overflow: hidden;
    }
    .divh
    {
        width: 444px;
        height: 332px;
        left:33px;
        top:158px;
        /*border: 36px;
        border-image:url(kuang02.png) 36 round;*/
        -webkit-transform: translate(600px,0px);
    }
    /*.imgconh
    {
        width: 368px;
        height: 256px;
        left:36px;
        top:36px;
        background-color: #fff;
        position: absolute;
    }*/
    .conh
    {
        position: absolute;
        width: 364px;
        height: 252px;
        left:37px;
        top:37px;
        background-color: #666;
        overflow: hidden;
    }
    @-webkit-keyframes yumao
    {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(20deg);}
    }
    @-webkit-keyframes shijian
    {
        from {-webkit-transform-origin:top;-webkit-transform:rotate(0deg);}
        to {-webkit-transform-origin:top;-webkit-transform:rotate(5deg);}
    }
    @-webkit-keyframes liushengji
    {
        from {-webkit-transform-origin:bottom;-webkit-transform:scaleY(1);opacity:0.8;}
        to {-webkit-transform-origin:bottom;-webkit-transform:scaleY(0.9);opacity:1;}
    }
    @-webkit-keyframes yinfu
    {
        0%{-webkit-transform:translate(0px,0px) rotate(90deg);opacity:0.5;}
        50%{-webkit-transform:translate(60px,-40px) rotate(45deg);opacity:1;}
        100%{-webkit-transform:translate(110px,-80px) rotate(0deg);opacity:0;}
    }
    @-webkit-keyframes yinfu01
    {
        0%{-webkit-transform:translate(0px,0px) rotate(90deg);opacity:0.5;}
        50%{-webkit-transform:translate(110px,-70px) rotate(45deg);opacity:1;}
        100%{-webkit-transform:translate(210px,-150px) rotate(0deg);opacity:0;}
    }
    @-webkit-keyframes yinfu02
    {
        0%{-webkit-transform:translate(0px,0px) rotate(0deg);opacity:0.5;}
        50%{-webkit-transform:translate(90px,-50px) rotate(-60deg);opacity:1;}
        100%{-webkit-transform:translate(160px,-100px) rotate(-120deg);opacity:0;}
    }
    @-webkit-keyframes yinfu03
    {
        0%{-webkit-transform:translate(0px,0px) rotate(0deg);opacity:0.5;}
        50%{-webkit-transform:translate(60px,-40px) rotate(30deg);opacity:1;}
        100%{-webkit-transform:translate(130px,-100px) rotate(60deg);opacity:0;}
    }
    @-webkit-keyframes butterfly02
    {
        0%{-webkit-transform:translate(0px,0px) scale(0.4,0.4) rotate(75deg);
            opacity: 1;}
        10%{-webkit-transform:translate(60px,10px) scale(0.4,0.4) rotate(120deg);}
        20%{-webkit-transform:translate(85px,20px)scale(0.4,0.4) rotate(140deg);}   
        30%{-webkit-transform:translate(140px,40px)scale(0.4,0.4) rotate(160deg);}
        50%{-webkit-transform:translate(140px,30px) scale(0.4,0.4) rotate(-45deg);}
        60%{-webkit-transform:translate(50px,10px) scale(0.4,0.4) rotate(-75deg);}
        80%{-webkit-transform:translate(10px,40px) scale(0.4,0.4) rotate(-120deg);
            opacity: 1;}
        90%{-webkit-transform:translate(-40px,30px) scale(0.4,0.4) rotate(-100deg);opacity: 1;}
        100%{-webkit-transform:translate(-40px,30px) scale(0.4,0.4) rotate(-100deg);opacity: 0;}
    }
    @-webkit-keyframes butterfly03
    {
        0%{-webkit-transform:translate(0px,0px) scale(0.5,0.5) rotate(135deg);
            opacity: 0;}
        30%{-webkit-transform:translate(50px,10px) scale(0.5,0.5) rotate(90deg);
            opacity: 1;}
        60%{-webkit-transform:translate(90px,-15px) scale(0.5,0.5) rotate(0deg);
            opacity: 1;}
        90%{-webkit-transform:translate(60px,-45px) scale(0.5,0.5) rotate(-90deg);
            opacity: 1;}
        100%{-webkit-transform:translate(40px,-35px) scale(0.5,0.5) rotate(-135deg);
            opacity: 0;}
    }
    @-webkit-keyframes butterfly04
    {
        0%{
            -webkit-transform:scale(0.4,0.4) rotate(0deg);
            opacity: 1;}
        8%{
            -webkit-transform:translate(-100px,-50px) scale(0.4,0.4) rotate(-75deg);
            opacity: 1;}
        14%{
            -webkit-transform:translate(-140px,-70px) scale(0.4,0.4) rotate(-90deg);
            opacity: 1;}
        30%{
            -webkit-transform:translate(-320px,-120px) scale(0.4,0.4) rotate(-45deg);
            opacity: 1;}
        35%{
            -webkit-transform:translate(-340px,-160px) scale(0.4,0.4) rotate(-45deg);
            opacity: 1;}
        70%{
            -webkit-transform:translate(-460px,-530px) scale(0.4,0.4) rotate(-45deg);
            opacity: 1;}
        80%{
            -webkit-transform:translate(-530px,-570px) scale(0.4,0.4) rotate(-90deg);
            opacity: 1;}
        100%{
            -webkit-transform:translate(-530px,-570px) scale(0.4,0.4) rotate(-90deg);
            opacity: 0;}
    }
    @-webkit-keyframes title_in
    {
        from  {opacity: 0;-webkit-transform: translate(400px,0px);}
        to    {opacity: 1;-webkit-transform: translate(0px,0px);}
    }
    @-webkit-keyframes title_out
    {
        from  {opacity: 1;-webkit-transform: translate(0px,0px);}
        to    {opacity: 0;-webkit-transform: translate(-400px,0px);}
    }
    
    @-webkit-keyframes chance
    {
        0%    {opacity: 0;-webkit-transform: translate(600px,0px);-webkit-animation-timing-function: ease-out;}
        17.2% {opacity: 1;-webkit-transform: translate(0px,0px);}
        20.6% {opacity: 1;-webkit-transform: translate(10px,0px);}
        79.3% {opacity: 1;-webkit-transform: translate(10px,0px);}
        82.7% {opacity: 1;-webkit-transform: translate(0px,0px);-webkit-animation-timing-function: ease-in;}
        100%  {opacity: 0;-webkit-transform: translate(-600px,0px);}
    }
    @-webkit-keyframes chance1
    {
        0%    {opacity: 0;-webkit-transform: translate(600px,0px);-webkit-animation-timing-function: ease-out;}
        17.2% {opacity: 1;-webkit-transform: translate(0px,0px);}
        20.6% {opacity: 1;-webkit-transform: translate(10px,0px);}
        79.3% {opacity: 1;-webkit-transform: translate(10px,0px);}
        82.7% {opacity: 1;-webkit-transform: translate(0px,0px);-webkit-animation-timing-function: ease-in;}
        100%  {opacity: 0;-webkit-transform: translate(-600px,0px);}
    }
    @-webkit-keyframes chance_3
    {
        0%    {opacity: 0;-webkit-transform: translate(600px,0px) rotate(90deg) scale(0.4);-webkit-animation-timing-function: ease-out;}
        17.2% {opacity: 1;-webkit-transform: translate(0px,0px) rotate(0deg) scale(0.8);}
        20.6% {opacity: 1;-webkit-transform: translate(10px,0px) rotate(-2deg) scale(1);}
        79.3% {opacity: 1;-webkit-transform: translate(10px,0px) rotate(2deg) scale(1);}
        82.7% {opacity: 1;-webkit-transform: translate(0px,0px) rotate(0deg) scale(0.8);-webkit-animation-timing-function: ease-in;}
        100%  {opacity: 0;-webkit-transform: translate(-600px,0px) rotate(-90deg) scale(0.4);}
    }
    @-webkit-keyframes chance_1
    {
        0%    {opacity: 0;-webkit-transform: translate(0px,-500px) rotate(90deg);-webkit-animation-timing-function: ease-out;}
        17.2% {opacity: 1;-webkit-transform: translate(0px,0px) rotate(0deg);}
        20.6% {opacity: 1;-webkit-transform: translate(0px,10px) rotate(-5deg);}
        79.3% {opacity: 1;-webkit-transform: translate(0px,10px) rotate(5deg);}
        82.7% {opacity: 1;-webkit-transform: translate(0px,0px) rotate(0deg);-webkit-animation-timing-function: ease-in;}
        100%  {opacity: 0;-webkit-transform: translate(0px,500px) rotate(-90deg);}
    }
    @-webkit-keyframes chance_2
    {
         0%    {opacity: 0;-webkit-transform: scale(0.4);-webkit-animation-timing-function: ease-out;}
        17.2% {opacity: 1;-webkit-transform: scale(1);}
        20.6% {opacity: 1;-webkit-transform: scale(0.9);}
        79.3% {opacity: 1;-webkit-transform: scale(0.9);}
        82.7% {opacity: 1;-webkit-transform: scale(1);-webkit-animation-timing-function: ease-in;}
        100%  {opacity: 0;-webkit-transform: scale(0.4);}
    }
</style>

<div id='container'>
    <img src='<?=$p?>images/background.jpg'>
    <div id='topdiv'>
        <img id='top' src='<?=$p?>images/shang.png'>
    </div>
    <div id='sjdiv'>
        <img id='sj' src='<?=$p?>images/sj01.png'>
    </div>
    <div id='maodiv'>
        <img id='mao' src='<?=$p?>images/mao01.png'>
    </div>
    <div id='liushengjidiv'>
        <img id='liushengji' src='<?=$p?>images/liushengji.png'>
    </div>
    <div id='musicdiv'>
        <img id='music' src='<?=$p?>images/wuxian.png'>
    </div>
    <div id="yinfudiv">
        <img id='yinfu' src='<?=$p?>images/yinfu.png'>
    </div>
    <div id="yinfu01div">
        <img id='yinfu01' src='<?=$p?>images/yinfu01.png'>
    </div>
    <div id="yinfu02div">
        <img id='yinfu02' src='<?=$p?>images/yinfu02.png'>
    </div>
    <div id="yinfu03div">
        <img id='yinfu03' src='<?=$p?>images/yinfu02.png'>
    </div>
    <div id="butterfly01div">
        <img id='butterfly01' src='<?=$p?>images/baihu.gif'>
    </div>
    <div id="butterfly02div">
        <img id='butterfly02' src='<?=$p?>images/baihu.gif'>
    </div>
    <div id="butterfly03div">
        <img id='butterfly03' src='<?=$p?>images/heihu.gif'>
    </div>
    <div id="butterfly04div">
        <img id='butterfly04' src='<?=$p?>images/heihu.gif'>
    </div>
    <div id="butterfly05div">
        <img id='butterfly05' src='<?=$p?>images/heihu.gif'>
    </div>

    <div id='pagetitle' style='position:absolute;width:349px;height:454px;top:190px;left:130px;opacity:0;'>
        <img src='<?=$p?>images/dizi.png'>
        <div style='position:absolute;width:220px;height:279px;top:35px;left:25px;overflow:hidden;display:table;'>
            <div id='titlecontent' style='width:100px;height:250px;vertical-align:middle;display:table-cell;text-align:center;font-size:24px;color:#50546B;z-index: 2;line-height:40px;'></div>
        </div>
    </div>
    <div id='divv' class='divv'>
        <div id='boxv' class='imgconv'>
            <img class='kuangv' src='<?=$p?>images/kuang01.png'>
            <div id='conv' class='conv'>
                <img id='imgv' style='position:absolute'>
            </div>
        </div>
    </div>
    <div id='divh' class='divh'>
        <div id='boxh' class='imgconh'>
            <img class='kuangh' src='<?=$p?>images/kuang02.png'>
            <div id='conh' class='conh'>
                <img id='imgh' style='position:absolute'>
            </div>
        </div>
    </div>
</div>



<script>

var image_size_width=[];
var image_size_height=[];
var image_url_index=0;
var have_num = 0;
var error_num = 0;
var canshow = true;
var reshow = false;
var delaytime=4000;
var timeout0;
var timeout1;
var timeout2;
var timeout3;

function id (name) 
{
    return document.getElementById(name);
}


function showtitle () 
{
    id('pagetitle').style.webkitAnimation = 'title_in 2s ease-out both';
    id('titlecontent').innerHTML = desc;  
}
function liangziyun_kawa () 
{
    id('pagetitle').style.webkitAnimation = 'title_out 2s ease-in both';

    timeout1 = setTimeout(show1,1000);
}

var arr_ani = ['1','2','3'];
var ani_index = 0;
function show1()
{
    id('divh').style.webkitAnimation = 'chance_'+arr_ani[ani_index%3]+' 4s linear both';
    setImage();
    id('divv').style.webkitAnimation = 'chance 4s linear both';
    ani_index ++;
    if(ani_index == 3)
        ani_index = 0;
    timeout2 = setTimeout(show2,4500);
}
function show2()
{
    id('divh').style.webkitAnimation = 'chance_'+arr_ani[ani_index%3]+' 4s linear both';
    setImage();
    id('divv').style.webkitAnimation = 'chance1 4s linear both';
    ani_index ++;
    if(ani_index == 3)
        ani_index = 0;
    timeout3 = setTimeout(show1, 4500)
}
function setImage()
{
    if(reshow == true)
        return;

    while(Onload_imgs_url[image_url_index] == 'not find' || Onload_imgs_url[image_url_index] == 'loading')
    {
        image_url_index++;
        if(image_url_index == Onload_imgs_url.length)
            image_url_index = 0;
    }

    if(image_url_index % step_loadnum == 0)
    {
        step_load();
    }
    console.log('setimg:' + Onload_imgs_url[image_url_index]);

    var img_bili = image_size_width[image_url_index]/image_size_height[image_url_index];
    var div;
    var div1;
    var img;

    if(img_bili > 1)
    {
        div = id('divh');
        div1 = id('divv');
        width = 372;
        height = 250;
        img = id('imgh');
    }
    else
    {
        div = id('divv');
        div1 = id('divh');
        width = 310;
        height = 450;
        img = id('imgv')
    }

    div.style.display = 'block';
    div1.style.display = 'none';

    img.src = Onload_imgs_url[image_url_index];
    // console.log(img.src);

    if(img_bili > (width/height))
    {
        img.style.top = '0px';
        img.style.height = height + 'px';
        img.style.width = height*img_bili + 'px';
        img.style.left = -((height*img_bili-width)/2)+'px';
    }
    else
    {
        img.style.left = '0px';
        img.style.width = width+'px';
        img.style.height = width/img_bili + 'px';
        img.style.top = -((width/img_bili-height)/2) + 'px';
    }

    image_url_index++;
    if(image_url_index==Onload_imgs_url.length)
    {
        image_url_index = 0;
    }
        
}
call_me(load_images);


function load_images()
{
    reshow = false;
    image_size_width=[];
    image_size_height=[];
    Onload_imgs_url=[];
    image_url_index=0;
    have_num = 0;
    error_num = 0;
    begin_titletime = new Date();
    begin_titletime = begin_titletime.getTime();
    canshow = true;
    showtitle();

    bl_keepload = 'first';
    step_load();
         
}


var bl_keepload = 'first';
var step_loadnum = 5;
function step_load()
{
    var load_num = 0

    if(image_url_index  == 0 && bl_keepload == 'first')
    {
        console.log('loading continue');
        if(slider_images_url.length > step_loadnum)
        {
            load_num = step_loadnum;
            bl_keepload = 'next';
        }
        else
        {
            load_num = slider_images_url.length;
            bl_keepload = 'end';
        }
        for(var i = 0; i< load_num; i++)
        {
            var img=new Image();
            img.index=i;
            img.src=slider_images_url[i];
            img.onload=image_onload;
            img.onerror= image_onerror;
            Onload_imgs_url[i] = 'loading';
        }
    }
    else if(bl_keepload == 'end')
    {
        return;
    }
    else
    {
         console.log('loading continue');
        if(slider_images_url.length - image_url_index >step_loadnum*2)
        { 
            load_num = step_loadnum;
        }
        else
        {
            load_num = slider_images_url.length - image_url_index - step_loadnum;
            bl_keepload = 'end';
        }
        for(var i = image_url_index +step_loadnum; i< image_url_index + step_loadnum + load_num; i++)
        {
            var img=new Image();
            img.index=i;
            img.src=slider_images_url[i];
            img.onload=image_onload;
            img.onerror= image_onerror;
            Onload_imgs_url[i] = 'loading';
        }
    }
}

function image_onerror(event)
{
    var img = event.target;
    var index = img.index;
    if(index<step_loadnum)
        error_num ++;
    Onload_imgs_url[index] = 'not find';
    console.log(Onload_imgs_url[index]);
    // console.log(have_num + '-' + error_num);
    if((have_num+error_num >= step_loadnum || slider_images_url.length == (have_num+error_num)) && canshow == true)
    {   
        // loading_others();
        reshow = false;
        canshow =false;
        if(have_num == 0)
            return;
        var end_titletime = new Date();
        end_titletime = end_titletime.getTime();
        var dis_titletime = Math.abs(end_titletime-begin_titletime);
        if(dis_titletime>delaytime)
        {
            liangziyun_kawa();
        }
        else
        {
            dis_titletime = delaytime- dis_titletime;
            timeout0 = setTimeout(function()
                {
                    liangziyun_kawa();
                },dis_titletime);
        }
    }
}

function image_onload(event)
{
    if(reshow == true)
        return;

    var img = event.target;
    var index = img.index;

    if(index<step_loadnum)
    {
        have_num++;
    }
    Onload_imgs_url[index] = img.src;
    image_size_height[index] = img.height;
    image_size_width[index] = img.width;

    console.log(Onload_imgs_url[index]);
    // console.log(have_num + '-' + error_num);

    if((have_num + error_num >= step_loadnum ||slider_images_url.length == (have_num+error_num)) && canshow == true)
    {   
        // loading_others();
        reshow = false;
        canshow =false;
        if(have_num == 0)
            return;
        var end_titletime = new Date();
        end_titletime = end_titletime.getTime();
        var dis_titletime = Math.abs(end_titletime-begin_titletime);
        if(dis_titletime>delaytime)
        {
            liangziyun_kawa();
        }
        else
        {
            dis_titletime = delaytime- dis_titletime;
            timeout0 = setTimeout(function()
                {
                    liangziyun_kawa();
                },dis_titletime);
        }

    }
}
function reload_scene()
{
    clearnode();
    reshow = true;
    setTimeout(load_images,100);
}

function clearnode()
{
    id('divv').style.webkitAnimation = '';
    id('divh').style.webkitAnimation = '';
    id('divv').style.display = 'none';
    id('divh').style.display = 'none';
    id('conv').style.webkitAnimation = '';
    id('conh').style.webkitAnimation = '';
    id('boxv').style.webkitAnimation = '';
    id('boxh').style.webkitAnimation = '';
    id('pagetitle').style.webkitAnimation = '';
    flag = 0;
    clearTimeout(timeout0);
    clearTimeout(timeout1);
    clearTimeout(timeout2);
    clearTimeout(timeout3);

}

</script>