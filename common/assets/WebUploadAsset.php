<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class WebUploadAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/dist/webuploader';
    public $baseUrl = '@web';
    public $css = [
        'webuploader.css',
        'style.css',
    ];
    public $js = [
        'webuploader.min.js',
        'upload.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
