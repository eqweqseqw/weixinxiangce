<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $verifyPassword;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => '哎呀，大侠,该用户名太好了已经被占用了'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
//            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => '大侠,该邮箱已经注册过了'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['verifyPassword', 'required'],
            ['verifyPassword', 'string', 'min' => 6],
            ['verifyPassword', 'compare', 'compareAttribute'=>'password', 'message'=>'请再输入确认密码'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        if(!($this->password===$this->rpassword))
            return null;
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
    public function attributeLabels(){
        return [
            'username' =>'用户名' ,
            'email' => '邮箱',
            'password' => '密码',
            'verifyPassword' => '确认密码',
        ];
    }
}
