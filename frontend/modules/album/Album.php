<?php

namespace app\modules\album;

/**
 * album module definition class
 */
class Album extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\album\controllers';
    public $templateAsset;
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::$app->assetManager->publish( '@app/modules/album/assets/template' );
        $templateAsset = \Yii::$app->assetManager->getPublishedUrl('@app/modules/album/assets/template');
        \Yii::configure($this,["templateAsset"=>$templateAsset]);
        // custom initialization code goes here            
    }
}
