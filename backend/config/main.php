<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'),
    require(__DIR__ . '/params-weixin.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'aliases' => [ 
        '@mdm/admin' => '@vendor/mdmsoft/yii2-admin',
    ],
    'modules' => [
        'admin' => [ 
            'class' => 'mdm\admin\Module', 
        ],
        'album' => [
            'class' => 'app\modules\album\Album',
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            //这里是允许访问的action
            //controller/action
        // * 表示允许所有，后期会介绍这个
            //'*'
        ]
    ],
    'defaultRoute' => 'admin/default',  
    'components' => [
//        'view' => [
//            'theme' => [
//                'pathMap' => [ 
//                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app' 
//                ],
//            ],
//        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
//        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
//        ],
        'user' => [
            'identityClass' => 'mdm\admin\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ], 
        'authManager' => [ 
            'class' => 'yii\rbac\DbManager', 
            'defaultRoles' => ['guest'], 
        ], 
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false, 
            'showScriptName' => false,
            'suffix' => '', 
            'rules' => [
//                "<controller:\w+>/<id:\d+>"=>"<controller>/view", 
//                "<controller:\w+>/<action:\w+>"=>"<controller>/<action>" 
            ],
        ],      
        'assetManager' => [ 
            'linkAssets' => true, 
        ], 
        'i18n' => [
            'translations' => [
               'app*' => [
                  'class' => 'yii\i18n\PhpMessageSource',
                  'fileMap' => [
                     'app' => 'app.php'
                  ],
               ],
            ],
         ],
        'language' => 'zh-CN',
    ],
     
    'params' => $params,
];
