$(function(){
//    var swiper = new Swiper('.swiper-container');
    var music_player = new Audio();
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        loop:false
    });
    
    var musicClick = function(){
        var _this = $(this);
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "dochange?type=music",//路径          
            data : {  
                music : _this.data("music") ,
                aid : $("#aid").val(),
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(result.code!=1) {
                   alert( result.message );
                } else {  
                   window.location.href="/album/"+result.aid; 
                }  
            },
            error:function(){
                alert( '哎呀，系统开小差了！' );
            }
        });  
    }   
    var openMusic = function(event){
        event.stopPropagation();
        var _this = $(this);
        var $li = _this.parents("li");
        var music = $li.data("music");
        if(music_player.paused||music_player.src!=music){
            music_player.src  = music;
            music_player.loop = 'loop'; 
            music_player.play();
            $li.siblings("li").find(".music-icon").removeClass("openMusic-icon") 
            _this.addClass("openMusic-icon");
        }else{
            music_player.pause();          
            _this.removeClass("openMusic-icon");
        }
    }   
    var getMusic = function(key){
        $.ajax({
            async: true,
            type : "post",  //提交方式  
            url : "getmusic",//路径  
            dataType:"json",
            data : {  
                key : key,
            },//数据，这里使用的是Json格式进行传输  
            success : function(result) {//返回数据根据结果进行相应的处理  
                if(!result.list) {
                    getMusic(key);
//                   alert( "哎呀，系统开小差了！" );
                } else {  
                    var myTemplate = Handlebars.compile($("#music-template").html());
                    $('#music-data').html(myTemplate(result));                 
                    $(".music-li").bind("click",musicClick);
                    $(".music-icon").bind("click",openMusic);
                }  
            },
            error:function(){
                getMusic(key);
//                alert( '哎呀，系统开小差了！' );
            }
        }); 
    } 
    $("#searchfrom").bind("submit",function(){
        getMusic($("#keyword").val())
        return false;
    })
    $(".swiper-slide").bind("click",function(){
        var _this = $(this);
        var key   = _this.data("key");
         _this.addClass("active").siblings().removeClass("active");
        if(key == 0){//搜索
             $("#searchfrom").show();          
            return ;
        }
        $("#searchfrom").hide();
        getMusic(key);        
    });
    $(".swiper-slide").eq(1).click();      
})

