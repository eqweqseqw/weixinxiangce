<?php
namespace common\wechat\libs;

use common\wechat\libs\FactoryAbstract;
use common\wechat\ReplyImage;

use common\models\AlbumAppModel;
use common\models\AlbumAppItemModel;

/* 
 * 处理text类消息.工厂
 */

class ImageFactory extends FactoryAbstract{
    protected function makeMetod() {
        return new ReplyImage($this->_postObj);             
    }   
}