<?php
$p = Yii::$app->controller->module->templateAsset."/langmanzhouye/";

?>

<style type="text/css">  
html,body,.main-body{
    height:100%
}
.stage {
	position: relative;height:100%;width:100%;top:0px;
	background: #ddf5f7;overflow:hidden;
	-webkit-animation: skyset 55s infinite linear;
	-moz-animation: skyset 55s infinite linear;
	-o-animation: skyset 55s infinite linear;
	animation: skyset 55s infinite linear;
}
@-webkit-keyframes skyset {
 0% {
 background: #ddf5f7;
}
 23% {
 background: #ddf5f7;
}
 25% {
 background: #350847;
}
 27% {
 background: #0f192c;
}
 50% {
 background: #0f192c;
}
 68% {
 background: #0f192c;
}
 75% {
 background: #f9c7b8;
}
 82% {
 background: #ddf5f7;
}
 100% {
 background: #ddf5f7;
}
}
 @-moz-keyframes skyset {
 0% {
 background: #ddf5f7;
}
 23% {
 background: #ddf5f7;
}
 25% {
 background: #350847;
}
 27% {
 background: #0f192c;
}
 50% {
 background: #0f192c;
}
 68% {
 background: #0f192c;
}
 75% {
 background: #f9c7b8;
}
 82% {
 background: #ddf5f7;
}
 100% {
 background: #ddf5f7;
}
}
@-o-keyframes skyset {
 0% {
 background: #ddf5f7;
}
 23% {
 background: #ddf5f7;
}
 25% {
 background: #350847;
}
 27% {
 background: #0f192c;
}
 50% {
 background: #0f192c;
}
 68% {
 background: #0f192c;
}
 75% {
 background: #f9c7b8;
}
 82% {
 background: #ddf5f7;
}
 100% {
 background: #ddf5f7;
}
}
 @keyframes skyset {
 0% {
 background: #ddf5f7;
}
 23% {
 background: #ddf5f7;
}
 25% {
 background: #350847;
}
 27% {
 background: #0f192c;
}
 50% {
 background: #0f192c;
}
 68% {
 background: #0f192c;
}
 75% {
 background: #f9c7b8;
}
 82% {
 background: #ddf5f7;
}
 100% {
 background: #ddf5f7;
}
}
/*-------- 天黑场景遮罩层以及动画 --------*/
.nightOverlay {
	z-index: 1;
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background: rgba(15, 25, 44, 0.7);
	opacity: 0;
	-webkit-animation: set 55s infinite linear;
	-moz-animation: set 55s infinite linear;
	-o-animation: set 55s infinite linear;
	animation: set 55s infinite linear;
}
 @-webkit-keyframes set {
 0% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
 50% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
 opacity: 1;
}
 100% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
}
 @-moz-keyframes set {
 0% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
 50% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
 opacity: 1;
}
 100% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
}
 @-o-keyframes set {
 0% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
 50% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
 opacity: 1;
}
 100% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
}
 @keyframes set {
 0% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
 50% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
 opacity: 1;
}
 100% {
 filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
 opacity: 0;
}
}
/*-------- 太阳自转以及动画 --------*/
.rotation {
	position: absolute;
	z-index: 1;
	left: 25%;
	bottom: 0%;
	height: 60%;
	width: 60%;
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
	-ms-transform: rotate(45deg);
	-o-transform: rotate(45deg);
	transform: rotate(45deg);
	-webkit-animation: rotation 55s infinite linear;
	-moz-animation: rotation 55s infinite linear;
	-o-animation: rotation 55s infinite linear;
	animation: rotation 55s infinite linear;
}
 @-webkit-keyframes rotation {
 0% {
 -webkit-transform: rotate(45deg);
}
 100% {
 -webkit-transform: rotate(405deg);
}
}
 @-moz-keyframes rotation {
 0% {
 -moz-transform: rotate(45deg);
}
 100% {
 -moz-transform: rotate(405deg);
}
}
 @-o-keyframes rotation {
 0% {
 -o-transform: rotate(45deg);
}
 100% {
 -o-transform: rotate(405deg);
}
}
 @keyframes rotation {
 0% {
 transform: rotate(45deg);
}
 100% {
 transform: rotate(405deg);
}
}
 
.sun, .moon {
	position: absolute;
	height: 45px;
	width: 45px;
	-webkit-border-radius: 50%;
	-moz-border-radius: 50%;
	-ms-border-radius: 50%;
	-o-border-radius: 50%;
	border-radius: 50%;
}
.sun {
	top: 0;
	left: 0;
	background: yellow;
}
.moon {
	bottom: 0;
	right: 0;
	background: #fff;
}
/*-------- 云以及动画 --------*/
.cloud {
	position: absolute;
}
#EVA_ALBUM_T041_Check_Loaded {
	display:none
}
#EVA_ALBUM_T041 {
	display:none;
	position:relative;
	overflow:hidden
}
.T041_butterfly {
	position:absolute;
	z-index:2
}
.T041_butterfly img {
	position:absolute
}
.kf_T041_butterfly {
	animation-iteration-count:infinite;
	-webkit-animation-iteration-count:infinite;
	animation-timing-function:linear;
	-webkit-animation-timing-function:linear;
	animation-name:y-spin;
	transform-origin:64% 50%;
	-webkit-transform-origin:64% 50%;
	-webkit-animation-name:y-spin;
	animation-duration:.4s;
	-webkit-animation-duration:.4s
}
@keyframes y-spin {
	0% {
	-webkit-transform:rotateY(0deg);
	transform:rotateY(0deg)
}
20% {
	-webkit-transform:rotateY(30deg);
	transform:rotateY(30deg)
}
40% {
	-webkit-transform:rotateY(60deg);
	transform:rotateY(60deg)
}
50% {
	-webkit-transform:rotateY(90deg);
	transform:rotateY(90deg)
}
60% {
	-webkit-transform:rotateY(60deg);
	transform:rotateY(60deg)
}
80% {
	-webkit-transform:rotateY(30deg);
	transform:rotateY(30deg)
}
100% {
	-webkit-transform:rotateY(0deg);
	transform:rotateY(0deg)
}
}@-webkit-keyframes y-spin {
	0% {
	-webkit-transform:rotateY(0deg)
}
20% {
	-webkit-transform:rotateY(30deg)
}
40% {
	-webkit-transform:rotateY(60deg)
}
50% {
	-webkit-transform:rotateY(90deg)
}
60% {
	-webkit-transform:rotateY(60deg)
}
80% {
	-webkit-transform:rotateY(30deg)
}
100% {
	-webkit-transform:rotateY(0deg)
}
}#T041_Image_Canvas {
	position:absolute;
	z-index:0;
	-webkit-perspective:1000;
	perspective:1000
}
.T041_Image_Wrapper {
	position:absolute;
	overflow:hidden;
	animation-fill-mode:forwards;
	-webkit-animation-fill-mode:forwards
}
.T041_Image_Frame {
	position:absolute;
	overflow:hidden;
	border:6px solid #fff;
	animation-fill-mode:forwards;
	-webkit-animation-fill-mode:forwards
}
.T041_Image,.T041_Image_Border {
	position:absolute
}
@keyframes kf_T041_rotateInY {
	0% {
	opacity:0;
	-webkit-transform:translate3d(0,600px,-700px) rotateX(-80deg);
	transform:translate3d(0,600px,-700px) rotateX(-80deg)
}
100% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateX(0deg);
	transform:translate3d(0,0,50px) rotateX(0deg)
}
}@-webkit-keyframes kf_T041_rotateInY {
	0% {
	opacity:0;
	-webkit-transform:translate3d(0,600px,-700px) rotateX(-80deg)
}
100% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateX(0deg)
}
}@keyframes kf_T041_rotateOutY {
	0% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateX(0deg);
	transform:translate3d(0,0,50px) rotateX(0deg)
}
100% {
	opacity:0;
	-webkit-transform:translate3d(0,-600px,-700px) rotateX(80deg);
	transform:translate3d(0,-600px,-700px) rotateX(80deg)
}
}@-webkit-keyframes kf_T041_rotateOutY {
	0% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateX(0deg)
}
100% {
	opacity:0;
	-webkit-transform:translate3d(0,-600px,-700px) rotateX(80deg)
}
}@keyframes kf_T041_rotateInX {
	0% {
	opacity:0;
	-webkit-transform:translate3d(-200px,0,700px) rotateY(80deg);
	transform:translate3d(-200px,0,700px) rotateY(80deg)
}
100% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateY(0deg);
	transform:translate3d(0,0,50px) rotateY(0deg)
}
}@-webkit-keyframes kf_T041_rotateInX {
	0% {
	opacity:0;
	-webkit-transform:translate3d(-200px,0,700px) rotateY(80deg)
}
100% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateY(0deg)
}
}@keyframes kf_T041_rotateOutX {
	0% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateY(0deg);
	transform:translate3d(0,0,50px) rotateY(0deg)
}
100% {
	opacity:0;
	-webkit-transform:translate3d(200px,0,700px) rotateY(-80deg);
	transform:translate3d(200px,0,700px) rotateY(-80deg)
}
}@-webkit-keyframes kf_T041_rotateOutX {
	0% {
	opacity:1;
	-webkit-transform:translate3d(0,0,50px) rotateY(0deg)
}
100% {
	opacity:0;
	-webkit-transform:translate3d(200px,0,700px) rotateY(-80deg)
}
}
#container {
	width:100%;
	height:100%;
	overflow:hidden;
	position:absolute;
	left:0px;
	top:0px;
}

.cloud.small {
	z-index: 1;
	top: 5%;
	left: 15%;
	background: url(<?=$p?>images/TB2l5yVppXXXXXAXXXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	height: 30px;
	width: 39px;
    background-size:100% 100%;
	-webkit-animation: cloudSmall 83s infinite;
	-moz-animation: cloudSmall 83s infinite;
	-o-animation: cloudSmall 83s infinite;
	animation: cloudSmall 83s infinite;
}
.cloud.medium {
	z-index: 3;
	top: 25%;
	left: 30%;
	background: url(<?=$p?>images/TB2OCObaXXXXXcgXpXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	height: 52px;
	width: 89px;
    background-size:100% 100%;
	-webkit-animation: cloudMedium 50s infinite;
	-moz-animation: cloudMedium 50s infinite;
	-o-animation: cloudMedium 50s infinite;
	animation: cloudMedium 50s infinite;
}
.cloud.large {
	z-index: 2;
	top: 5%;
	right: 15%;
	background: url(<?=$p?>images/TB2TtDoaFXXXXcDXpXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	height: 89px;
	width: 150px;
    background-size:100% 100%;
	-webkit-animation: cloudLarge 52s infinite;
	-moz-animation: cloudLarge 52s infinite;
	-o-animation: cloudLarge 52s infinite;
	animation: cloudLarge 52s infinite;
}
 @-webkit-keyframes cloudSmall {
 0% {
 left: -8%;
}
 100% {
 left: 108%;
}
}
 @-moz-keyframes cloudSmall {
 0% {
 left: -5%;
}
 100% {
 left: 105%;
}
}
 @-o-keyframes cloudSmall {
 0% {
 left: -5%;
}
 100% {
 left: 105%;
}
}
 @keyframes cloudSmall {
 0% {
 left: -5%;
}
 100% {
 left: 105%;
}
}
 @-webkit-keyframes cloudMedium {
 0% {
 left: -8%;
}
 100% {
 left: 108%;
}
}
 @-moz-keyframes cloudMedium {
 0% {
 left: -8%;
}
 100% {
 left: 108%;
}
}
 @-o-keyframes cloudMedium {
 0% {
 left: -8%;
}
 100% {
 left: 108%;
}
}
 @keyframes cloudMedium {
 0% {
 left: -8%;
}
 100% {
 left: 108%;
}
}
 @-webkit-keyframes cloudLarge {
 0% {
 right: -18%;
}
 100% {
 right: 118%;
}
}
 @-moz-keyframes cloudLarge {
 0% {
 right: -18%;
}
 100% {
 right: 118%;
}
}
 @-o-keyframes cloudLarge {
 0% {
 right: -18%;
}
 100% {
 right: 118%;
}
}
 @keyframes cloudLarge {
 0% {
 right: -18%;
}
 100% {
 right: 118%;
}
}

.balloon {
	position: absolute;
	z-index: 3;
	top: 5%;
	right: 20%;
	background: url(<?=$p?>images/TB2mouMppXXXXaqXXXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	height: 100px;
	width: 70px;
	background-size: 100% 100%;
	-webkit-animation: balloon 15s infinite cubic-bezier(0.91, 0.01, 1, 0.89);
	-moz-animation: balloon 15s infinite cubic-bezier(0.1, 0.1, 0.95, 0.5);
	-o-animation: balloon 15s infinite cubic-bezier(0.1, 0.1, 0.95, 0.5);
	animation: balloon 15s infinite cubic-bezier(0.1, 0.1, 0.95, 0.5);
}
 @-webkit-keyframes balloon {
 0% {
 right: -20%;
 -webkit-transform: rotate(0deg);
}
 5% {
 right: -20%;
 -webkit-transform: rotate(0deg);
}
 25% {
 -webkit-transform: rotate(0deg);
}
 100% {
 right: 120%;
 -webkit-transform: rotate(-30deg);
}
}
 @-moz-keyframes balloon {
 0% {
 right: -20%;
 -moz-transform: rotate(0deg);
}
 5% {
 right: -20%;
 -moz-transform: rotate(0deg);
}
 25% {
 -moz-transform: rotate(0deg);
}
 100% {
 right: 120%;
 -moz-transform: rotate(-30deg);
}
}
 @-o-keyframes balloon {
 0% {
 right: -20%;
 -o-transform: rotate(0deg);
}
 5% {
 right: -20%;
 -o-transform: rotate(0deg);
}
 25% {
 -o-transform: rotate(0deg);
}
 100% {
 right: 120%;
 -o-transform: rotate(-30deg);
}
}
 @keyframes balloon {
 0% {
 right: -20%;
 transform: rotate(0deg);
}
 5% {
 right: -20%;
 transform: rotate(0deg);
}
 25% {
 transform: rotate(0deg);
}
 100% {
 right: 120%;
 transform: rotate(-30deg);
}
}
/*-------- ������� --------*/
.skyline {
	position: absolute;
	z-index: 5;
	width: 100%;
	bottom: 16%;
	background: url(<?=$p?>images/TB2A1vKapXXXXXGXFXXXXXXXXXX_!!1016194477.png) repeat no-repeat;
	height: 100px;
}

.ground {
	position: absolute;
	width: 100%;
	bottom: 0;
}
.ground.front {
	z-index: 30;
	background: url(<?=$p?>images/TB2IPGYppXXXXXnXXXXXXXXXXXX_!!1016194477.png) no-repeat center;
	background-size: auto 100%;
	height: 101px;
}
.ground.mid {
	z-index: 20;
	background: url(<?=$p?>images/TB2BdKvppXXXXbBXpXXXXXXXXXX_!!1016194477.png) no-repeat;
	background-size: auto 100%;
	height: 26%;
}
.ground.back {
	z-index: 10;
	background: url(<?=$p?>images/TB29gCDppXXXXarXpXXXXXXXXXX_!!1016194477.png) no-repeat center;
	background-size: auto 100%;
	height: 26%;
}
.dowEventCenter {
	position: absolute;
	z-index: 12;
	bottom: 20%;
	left: 5%;
	background: url(<?=$p?>images/TB2AaOsppXXXXXmXFXXXXXXXXXX_!!1016194477.png) no-repeat center;
	background-size: auto 100%;
	height: 10%;
	width: 100%;
}
.planetarium {
	position: absolute;
	z-index: 12;
	bottom: 18%;
	right: 10%;
	background: url(<?=$p?>images/TB2zjyrppXXXXa1XpXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	background-size: auto 100%;
	height: 10%;
	width: 80%;
}
.friendshipShell {
	position: absolute;
	z-index: 21;
	bottom: 28%;
	left: 20%;
	background: url(<?=$p?>images/TB2aJCsppXXXXXWXpXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	background-size: auto 100%;
	height: 10%;
	width: 60%;
}
.glockenspiel {
	position: absolute;
	z-index: 11;
	bottom: 22%;
	right: 50%;
	background: url(<?=$p?>images/TB29m5tppXXXXa9XpXXXXXXXXXX_!!1016194477.png) no-repeat no-repeat center;
	height: 23%;
	width: 47px;
    background-size:100% 100%;
}

#duolaameng{position:absolute;z-index:1;}
#duolaameng_elements{position:absolute;z-index:1;}
    </style>
<div class="stage">
    <div class="nightOverlay"></div>
    <div class="skyline"></div>
    <div class="beans"></div>
    <div class="ground back"></div>
    <div class="ground mid"></div>
    <div class="ground front"></div>
    <div class="cloud large"></div>
    <div class="cloud small"></div>
    <div class="cloud medium"></div>
    <div class="balloon"></div>
    <div class="dowEventCenter"></div>
    <div class="planetarium"></div>
    <div class="friendshipShell"></div>
    <div class="glockenspiel"></div>
    <div class="rotation">
      <div class="sun"></div>
      <div class="moon"></div>
    </div>
  </div>
 <div id="duolaameng">
    <div id="duolaameng_elements" style="width: 375px; height: 627px; left: 0px; top: 0px;overflow:hidden;"></div>
    <div id="T041_Image_Canvas" style="width: 375px; height: 627px; left: 0px; top: 0px;">
    </div>
        </div>
    <script type="text/javascript">
        ZMTXIFENBAO_Animation_Mgr = new
function () {
    function t(t) {
        var r = $(this).data(e),
        c = o(this),
        l = c > -1 ? a[c] : null,
        d = l && l.param[n],
        p = l && l.param[i];
        if (d && d(this, t.animationName), t.animationName == r) p && p(this);
        else {
            var f = l.param.animates[++a[c].frame];
            s(this, f)
        }
    }
    var e = "animation-mgr-last-animation-name",
    n = "progress",
    i = "end",
    a = [],
    o = function (t) {
        for (var e = 0; e < a.length; ++e) if (a[e].obj == t) return e;
        return -1
    };
    this.animate = function (n, i) {
        if (i && i.animates && !(i.animates.length < 1)) {
            $(n).data(e, i.animates[i.animates.length - 1].name);
            var r = o(n);
            r > -1 ? (a[r].param = i, a[r].frame = 0) : a.push({
                obj: n,
                frame: 0,
                param: i
            }),
            s(n, i.animates[0]),
            $listen(n, "webkitAnimationEnd", t),
            $listen(n, "animationEnd", t)
        }
    };
    var s = function (t, e) {
        e && t && window.setTimeout(function () {
            var n = e.name,
            i = e.duration + "s",
            a = e.timing,
            o = e.count;
            jQuery(t).css("animation-name", n).css("-webkit-animation-name", n).css("animation-duration", i).css("-webkit-animation-duration", i).css("animation-delay", "").css("-webkit-animation-delay", "0").css("animation-timing-function", a).css("-webkit-animation-timing-function", a).css("animation-iteration-count", o).css("-webkit-animation-iteration-count", o)
        },
        1e3 * ("undefined" != e.delay ? e.delay : 0))
    }
};
        ZMTXIFENBAO_Image = new
function () {
    this.loadImage = function (t) {
        var e = new Image;
        e.onload = function () {
            t && t.success && t.success(this)
        },
        e.onerror = function () {
            t && t.fail && t.fail(this)
        },
        e.src = t.url
    },
    this.clip = function (t, e, n, i, a) {
        var o, s, r, c;
        i / a > e / n ? (s = n, o = Math.floor(n * (i / a))) : (o = e, s = Math.floor(e * (a / i))),
        r = (o - e) / 2,
        c = (s - n) / 2,
        $(t).width(o).height(s).css("left", 0 - r + "px").css("top", 0 - c + "px"),
        t.style.clip = "rect(" + c + "px " + Math.floor(r + e) + "px " + Math.floor(c + n) + "px " + r + "px)"
    }
};
        ZMTXIFENBAO = new
function () {
    var e = "",
    t = 5,
    i = !1;
    this.init = function () {
        n()
    };
    var n = function () {
        $("#playendtips_replay").click(function () {
            $("#wrapper_play_end").hide(),
            ZMTXIFENBAO_DA.click("album.view.play.endtip.replay")
        }),
        $("#playendtips_stop").click(function () {
            ZMTXIFENBAO_DA.click("album.view.play.endtip.stop"),
            window.close(),
            wx.closeWindow()
        })
    }
    this.isPlayEndTipsShowed = function () {
        return i
    },
    this.setBizId = function (t) {
        e = t
    },
    this.getBizId = function () {
        return e
    },
    this.imgMgr = new
    function () {
        var e = "ZMTXIFENBAO_ImageMgr",
        t = [],
        i = 0;
        this.append = function (i) {
            i && i.url && i.url.length > 0 && t.push({
                url: i.url
            })
        },
        this.getNextImg = function () {
            return t.length < 1 ? null : t[i++ % t.length]
        },
        this.isAllImgDisplayed = function () {
            return i > t.length
        },
        this.getImgCount = function () {
            return t.length
        },
        this.getDisplayedImgCount = function () {
            return i
        },
        this.getImg = function (e) {
            return e < t.length ? t[e] : null
        }
    }
};
        function calcImgClip(t, e, n, i) {
            var a, o, s, r;
            return n / i > t / e ? (o = e, a = Math.floor(e * (n / i))) : (a = t, o = Math.floor(t * (i / n))),
            s = (a - t) / 2,
            r = (o - e) / 2,
            {
                objWidth: a,
                objHeight: o,
                clipLeft: s,
                clipTop: r
            }
        }
        function $listen(t, e, n) {
            t.addEventListener ? t.addEventListener(e, n, !1) : t.attachEvent && t.attachEvent(e, n)
        }
        function $unlisten(t, e, n) {
            t.removeEventListener ? t.removeEventListener(e, n) : t.detachEvent && t.detachEvent(e, n)
        }
       $(function(){
           var params = { max_width: $(window).width(), max_height: $(window).height(), canvas_id: "T041_Image_Canvas" };
            ZMTXIFENBAO_Album_Template(params);
       })
        
        function ZMTXIFENBAO_Album_Template(t) {
            function i() {
                o()
            }
            var e = t.max_width,
            a = t.max_height,
            n = !1,
            s = $("#" + t.canvas_id);
            this.div_root = document.createElement("div"),
            this.div_root.id = "EVA_ALBUM_T041",
            this.div_root.style.width = e + "px",
            this.div_root.style.height = a + "px";
            for (var i = 0; i < slider_images_url.length; i++) {
                ZMTXIFENBAO.imgMgr.append({
                    url: slider_images_url[i]
                })
            }
            var o = function () {
                return 1 == n ? void console.log("warning,re init") : (n = !0, $("#duolaameng").width(e).height(a).css("left", "0").css("top", "0"), $("#duolaameng_elements").width(e).height(a).css("left", "0").css("top", "0"), $(".T041_butterfly").width(50 * e / 375).height(50 * e / 375).css("right", .16 * e + "px").css("bottom", .134 * a + "px"), $(".T041_butterfly").find("img").width(50 * e / 375).height(50 * e / 375), void $("#T041_Image_Canvas").width(e).height(a).css("left", "0").css("top", "0"))
            };
            o();
            this.div_root.style.display = "block";
            var t = window.setInterval(function () {
                if (n === !0) {
                    window.clearInterval(t);
                    for (var i, s, o = 0; 30 > o; o++) i = parseInt(Math.random() * e),
                    s = parseInt(-50 * Math.random());
                    var d = $("#T041_Image_Canvas");
                    T041Scene.init(d[0], e, a,
                    function () {
                        r()
                    }),
                    r()
                }
            },
            20)
            var r = function () {
                T041Scene.show(ZMTXIFENBAO.imgMgr.getNextImg().url)
            }
        }
        var T041Scene = new
        function () {
            var t, i, e, a, n = 208,
            s = 320,
            o = ["kf_T041_rotateInX", "kf_T041_rotateInY"],
            r = ["kf_T041_rotateOutX", "kf_T041_rotateOutY"],
            d = 0;
            this.init = function (n, s, o, r) {
                a = n,
                t = s,
                i = o,
                e = r
            },
            this.show = function (l) {
                ZMTXIFENBAO_Image.loadImage({
                    url: l,
                    success: function (m) {
                        m.width / m.height > 1 && m.width / m.height < 2 ? (n = .85 * t, s = 320 * m.height / m.width) : m.width / m.height <= 1 && m.width / m.height > .5 ? (s = 320, n = 320 * m.width / m.height) : m.width / m.height >= 2 ? (n = 240, s = 320) : (n = 240, s = 320);
                        var h = document.createElement("div");
                        if (h.className = "T041_Image_Wrapper", h.innerHTML = '<div class="T041_Image_Frame"><img class="T041_Image"></div>', $(h).width(n + 12).height(s + 12).css({
                            left: (t - n - 12) / 2 + "px",
                            top: (i - s - 12) / 2 + "px",
                            opacity: 0
                        }), $(a).append(h), $(".T041_Image_Frame").width(n).height(s).css({
                            left: "0px",
                            top: "0px"
                        }), $(".T041_Image").attr("src", l), ZMTXIFENBAO_Image.clip($(".T041_Image")[0], n, s, m.width, m.height), 5 > d) var c = o[0],
                        g = r[0];
                        else if (9 > d && d >= 5) var c = o[1],
                        g = r[1];
                        d++,
                        d >= 9 && (d = 0),
                        ZMTXIFENBAO_Animation_Mgr.animate($(".T041_Image_Wrapper")[0], {
                            animates: [{
                                name: c,
                                duration: 2.5,
                                delay: 0,
                                times: "liner"
                            }],
                            progress: function (t, i) {
                            },
                            end: function (t) {
                                window.setTimeout(function () {
                                    ZMTXIFENBAO_Animation_Mgr.animate($(".T041_Image_Wrapper")[0], {
                                        animates: [{
                                            name: g,
                                            duration: 2.5,
                                            delay: 0,
                                            times: "liner"
                                        }],
                                        progress: function (t, i) {
                                        },
                                        end: function (t) {
                                            $(".T041_Image_Wrapper").remove(),
                                            e()
                                        }
                                    })
                                },
                                500)
                            }
                        })
                    },
                    fail: function (t) {
                        e()
                    }
                })
            }
        };
        jQuery(document).ready(function (jQuery) {
            jQuery('#recommend-close').click(function (e) {
                jQuery('#recommend').fadeOut();
                e.preventDefault();
            });
        });

    </script>
