<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * AlbumApp model
 *
 * @property integer $id
 * @property string $name 名称
 * @property string $sort 排序
 */
class AlbumTemplateClassModel extends ActiveRecord
{
    const SORT = 0;//排序
    
    /**
     * @wangwei
     */
    public static function tableName()
    {
        return '{{%album_template_class}}';
    }
  
    /**
     * @wangwei
     * 默认值
     */
    public function rules()
    {
        return [
            ['sort', 'default', 'value' => self::SORT],
        ];
    }
     /**
     * @wangwei
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'sort' => "排序",
        ];
    }
    /**
     * Finds albumApp by id
     * @wangwei
     * @param int $id id
     */
    public static function findOneById($id)
    {
        return static::findOne(['id' => $id]);
    }
     /**
     * Finds templates by classid
     * @wangwei
     * @param int $id id
     * @param int $status 公开程度（0私有，1好友可见，2公开）
     */
    public function getItems()
    {
        // 第一个参数为要关联的子表模型类名，
        // 第二个参数指定 通过子表的app_id，关联主表的id字段
        return $this->hasMany(AlbumTemplateModel::className(), ['class_id' => 'id']);
    }  
    /**
     * Finds albumApp by name
     *
     * @param int $name 名称
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::findOne(['name' => $name]);
    }
     
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }  
 
}
