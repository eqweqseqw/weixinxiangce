<?php

namespace app\modules\album\controllers;

use yii\web\Controller;

/**
 * 网站基本页面控制器
 */
class IndexController extends Controller
{
    /**
     * 相册首页页面
     * @return string
     */
    public function actionIndex()
    {
        $this->getView()->title = "首页";
        return $this->render('index');
    }
    /**
     * 相册帮助页面
     * @return string
     */
    public function actionHelp()
    {
        $this->getView()->title = "相册常见问题";
        return $this->render('help');
    }
}
