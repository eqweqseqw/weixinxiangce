<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class JqueryAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/dist';
    public $baseUrl = '@web'; 
    public $js = [
         'js/jquery.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
