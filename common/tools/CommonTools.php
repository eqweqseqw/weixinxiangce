<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\tools;

/**
 * Description of CommonTools
 *
 * @author weiwangw
 */
class CommonTools {
    /**
     * 是否微信
     * @return boolean
     */
    public static function is_wechat(){
        return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') === false ?false:true;    
    }
    /**
     * 是否ios
     * @return boolean
     */
    public static function is_ios(){
        return self::get_device_type()==="ios" ?true:false;        
    }
    /**
     * 是否android
     * @return boolean
     */
    public static function is_android(){
        return self::get_device_type()==="android" ?true:false; 
    }
    /**
     * 安装or苹果
     * @return type
     */
    private static function get_device_type(){
        //全部变成小写字母
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $type = 'other';
        //分别进行判断
        if(strpos($agent, 'iphone') || strpos($agent, 'ipad')){
            $type = 'ios';
        } 

        if(strpos($agent, 'android')){
            $type = 'android';
        }
        return $type;
    }
}
