<?php

namespace common\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AlbumAppModel;

/**
 * 
 * @author wangwei
 * @since 1.0
 */
class AlbumSearch extends AlbumAppModel
{
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'title','des'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Searching menu
     * @param  array $params
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->from(self::tableName());    
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $sort = $dataProvider->getSort();
        
        $sort->defaultOrder = ['created_at' => SORT_DESC];
        if (!($this->load($params) && $this->validate())) {       
            return $dataProvider;
        }
        
        $query->andFilterWhere(['=', 'lower(uid)', strtolower($this->uid)])
            ->andFilterWhere(['like', 'lower(title)', $this->title])
            ->andFilterWhere(['like', 'lower(des)', $this->des]);
        return $dataProvider;
    }
}
