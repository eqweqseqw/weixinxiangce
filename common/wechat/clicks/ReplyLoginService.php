<?php
namespace common\wechat\clicks;

use Yii;
use common\wechat\interfaces\Ireply; 
use common\tools\YWeChatPrint;

class ReplyLoginService implements Ireply
{
    protected $_postObject;
    protected $_fromUserName;
    protected $_toUserName;
    protected $_flag;
    
    public function __construct($postObj) {
        // 获取参数   
        $this->_postObject = $postObj;     
        $this->_fromUserName = $this->_postObject->FromUserName;      
        $this->_toUserName =  $this->_postObject->ToUserName;  
    }
    
    public function createReMsg(){
        $site = \Yii::$app->params["weixinConf"]["domainName"];
        $item = [
                    "Title"=>"点我登录",
                    "Description"=>"点我登录",
                    "PicUrl"=>"{$site}/image/bg.jpg",
                    "Url"=>"{$site}/site/loginbyopenid/?openid=".$this->_fromUserName,
                ];  
        return YWeChatPrint::print_news($this->_fromUserName, $this->_toUserName, array($item));                              
    }  
}
